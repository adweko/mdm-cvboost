import groovy.json.JsonOutput
import groovy.json.JsonSlurperClassic

//---------------------------------------
// Read business partner in Spectrum MDM
//---------------------------------------

logger.debug("Start writeStateMessages")
try {
    def contentType
    Integer loopcounter
    def status

    def service = "getPredatetBusinessPartnerVersionsValidFromDate"
    def serviceJson = resources.get("deployment:/json/service/${service}.json")
    def serviceMap = serviceJson.load("json")

// Fill service map from object map
//serviceMap.I.Row[0].keyDate = "04.04.2020"
    Date date = new Date()
    serviceMap.I.Row[0].keyDate = date.format("dd.MM.yyyy")

    logger.debug("ServiceMap getPredatedVersions: " + serviceMap)

    logger.debug("Call Spectrum REST service ${service}")

    contentType = ""
    loopcounter = 0
    while (contentType != "application/json") {
        try {
            spectrum.request(service)
                    .object("json", serviceMap)
                    .acceptJSON()
                    .execute({ info, inStream ->
                        status = info.status
                        serviceResultText = inStream.getText(info.contentEncoding)
                        contentType = info.getContentType()
                    })
        }
        catch (Exception ex) {
            logger.error("Error calling Spectrum service (" + service + "): " + ex.toString())
            loopcounter++
            if (loopcounter == 5) {
                throw new Exception("Error calling Spectrum service (" + service + "): Retry Limit of 5 reached")
            }
            continue
        }
        if (contentType == "application/json") {
            // Service returned a json, so return a Map object
            serviceResults = new JsonSlurperClassic().parseText(serviceResultText)

        } else {
            logger.error("Error calling Spectrum service (" + service + "): " + status + ", " + serviceResultText)
            throw new Exception("Error calling Spectrum service (" + service + "): " + status + ", " + serviceResultText)
        }
    }

    if (serviceResults in Map) {
        def serviceResultsPrettyNP = JsonOutput.prettyPrint(JsonOutput.toJson(serviceResults).toString())
        logger.debug( "serviceResults: " + serviceResultsPrettyNP)
    }

    def service2 = "readFullPartnerAllVersionsFromDate"

// read service definition
    def serviceJson2 = resources.get("deployment:/json/service/${service2}.json")
    def serviceMap2 = serviceJson2.load("json")


    serviceResults.O.each { message ->

        serviceMap2.I.Row[0].keyDate = message.keyDateOut
        serviceMap2.I.Row[0].CRUD = "Update"
        serviceMap2.I.Row[0].TECHMDMPartnerID = message.TECHMDMPartnerID

        logger.debug("ServiceMap readFullPartnerAllVersionsFromDate: " + serviceMap2)
        // call spectrum getPredatetBusinessPartnerVersionsValidFromDate REST service POST method
        contentType = ""
        loopcounter = 0
        contentType = ""
        while (contentType != "application/json") {
            try {
                logger.debug("Call Spectrum REST service ${service2}")
                spectrum.request(service2)
                        .object("json", serviceMap2)
                        .acceptJSON()
                        .execute({ info, inStream ->
                            status = info.status
                            serviceResultText = inStream.getText(info.contentEncoding)
                            contentType = info.getContentType()
                        })
            }
            catch (Exception ex) {
                logger.error("Error calling Spectrum service (" + service2 + "): " + ex.toString())
                loopcounter++
                if (loopcounter == 5) {
                    throw new Exception("Error calling Spectrum service (" + service2 + "): Retry Limit of 5 reached")
                }
                continue
            }
            if (contentType == "application/json") {
                // Service returned a json, so return a Map object
                serviceResults2 = new JsonSlurperClassic().parseText(serviceResultText)

            } else {
                logger.error("Error calling Spectrum service (" + service2 + "): " + status + ", " + serviceResultText)
                throw new Exception("Error calling Spectrum service (" + service2 + "): " + status + ", " + serviceResultText)
            }
        }

        if (serviceResults2 in Map) {
            def serviceResultsPrettyNP = JsonOutput.prettyPrint(JsonOutput.toJson(serviceResults2).toString())
            logger.debug("serviceResults: " + serviceResultsPrettyNP)
            resources.get("/kafka_out_state.json").save("json", serviceResults2)
            // extract kafka_key
            Map isBusinessPartner = serviceResults2.get("BusinessPartnerID").first().get("Versions").first().get("isBusinessPartner").first()
            isBusinessPartner.remove("BusinessPartner")
            isBusinessPartner.remove("TECHInternalPID")
            isBusinessPartner.put("TECHMDMPartnerID", serviceResults2.get("BusinessPartnerID").first().get("TECHMDMPartnerID").toString())
            def isBusinessPartnerPretty = JsonOutput.prettyPrint(JsonOutput.toJson(isBusinessPartner).toString())
            task.setVariable("kafka_key", isBusinessPartnerPretty.toString())
            logger.debug("kafka_key: " + isBusinessPartnerPretty)
            // write kafka message
            messaging.message("ch-mdm.state.avro")
            		.nodeId("AGENT_MESSAGING_KAFKA")
                    .source("/kafka_out_state.json")
                    .property("key", isBusinessPartnerPretty)
                    .property("converter", "Json2Avro")
                    .property("converter.subject", "ch-mdm.state.all")
                    .property("converter.version", task.getVariable("avro.schema.state.version"))
                    .send()

            logger.debug("Kafka Message written to ch-mdm.state.avro")
        }
    }
}
catch (Exception ex) {
    logger.error(ex.toString())
    task.setVariable("error", true)
    task.setVariable("error_message", ex.toString())
}
logger.debug("End writeStateMessages")

