import groovy.transform.Field

logger.debug("Start initializeWorkflow")
// Use Kafka true/false
task.setVariable("useKafka", true)
logger.debug("useKafka: " + task.getVariable("useKafka"))

task.setVariable("error", false)

// Read the file "avro_schema.versions" located in the home of cvboost
// and set task variables to point to the versions read from there
    def homeDir = application.getProperty("app.home")
    def avroSchemaFilePath = resources.get("system:" + homeDir + "/avro/schema.properties")
    Properties properties = new Properties()
    avroSchemaFilePath.load(
    	{
    		inputStream -> properties.load(inputStream)
    	}
    	)
    //properties.load(new FileInputStream(avroSchemaFilePath))

    def stateSchemaVersion = properties.get("ch-mdm.state.all")
    if (stateSchemaVersion != null) {
        task.setVariable("avro.schema.state.version", stateSchemaVersion)
    } else {
        throw new RuntimeException("The property ch-mdm.state.all is not set in " + avroSchemaFilePath)
    }

    def changeSchemaVersion = properties.get("ch-mdm.change.all")
    if (changeSchemaVersion != null) {
        task.setVariable("avro.schema.change.version", changeSchemaVersion)
    } else {
        throw new RuntimeException("The property ch-mdm.state.all is not set in " + avroSchemaFilePath)
    }



logger.debug("End initializeWorkflow")