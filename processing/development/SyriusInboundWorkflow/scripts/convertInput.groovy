import groovy.json.JsonOutput
import groovy.transform.Field
import groovy.json.JsonBuilder
import groovy.json.JsonSlurperClassic

def parseMap(map) {

    returnparseMap = new String()
    returnparseMap = returnparseMap + "{"

    map.each {
        if (it.value instanceof Map) {
            returnparseMap = returnparseMap + "\"" + it.key + "\":"

            parseMap2(it.value)
        } else {
            if (it.value instanceof List) {
                if (it.key == "Output") {
                    returnparseMap = returnparseMap + "\"I\":{\"Row\":"
                } else {
                    returnparseMap = returnparseMap + "\"" + it.key + "\":"
                }
                returnparseMap = returnparseMap + "["
                parseArray(it.value)
                returnparseMap = returnparseMap + "]"

                if (it.key == "Output") {
                    returnparseMap = returnparseMap + "}"
                }
            } else {

                if (it.value == null) {
                    returnparseMap = returnparseMap + "\"" + it.key + "\":" + it.value + ","
                } else {
                    returnparseMap = returnparseMap + "\"" + it.key + "\":\"" + it.value + "\","
                }
            }
        }
    }

    if (returnparseMap.substring(returnparseMap.length() - 1) == ",") {
        returnparseMap = returnparseMap.substring(0, (returnparseMap.length() - 1))
    }
    returnparseMap = returnparseMap + "}"
    return returnparseMap
}

def parseMap2(map) {

    if (map.keySet().iterator().next() != "string" && map.keySet().iterator().next() != "long" && map.keySet().iterator().next() != "boolean" && map.keySet().iterator().next() != "int" && map.keySet().iterator().next() != "date" && map.keySet().iterator().next() != "array" && !map.keySet().iterator().next().contains("com.adcubum")) {
        returnparseMap = returnparseMap + "{"
    }

    map.each {
        if (it.value instanceof Map) {
            if (it.key != "array" && !it.key.contains("com.adcubum")) {
                returnparseMap = returnparseMap + "\"" + it.key + "\":"
            }
            parseMap2(it.value)
        } else if (it.value instanceof List) {
            if (it.key != "array" && !it.key.contains("com.adcubum")) {
                returnparseMap = returnparseMap + "\"" + it.key + "\":"
            }
            returnparseMap = returnparseMap + "["
            parseArray(it.value)

            if (returnparseMap.substring(returnparseMap.length() - 1) == ",") {
                returnparseMap = returnparseMap.substring(0, (returnparseMap.length() - 1))
            }
            returnparseMap = returnparseMap + "],"
        } else {
            if (it.key != "string" && it.key != "long" && it.key != "boolean" && it.key != "int" && it.key != "date") {
                if (it.value == null) {
                    returnparseMap = returnparseMap + "\"" + it.key + "\":" + it.value + ","
                } else {
                    returnparseMap = returnparseMap + "\"" + it.key + "\":\"" + it.value + "\","
                }
            } else {
                if (it.value == null) {
                    returnparseMap = returnparseMap + it.value + ","
                } else {
                    returnparseMap = returnparseMap + "\"" + it.value + "\","
                }
            }
        }
    }

    if (map.keySet().iterator().next() != "string" && map.keySet().iterator().next() != "long" && map.keySet().iterator().next() != "boolean" && map.keySet().iterator().next() != "int" && map.keySet().iterator().next() != "date" && map.keySet().iterator().next() != "array" && !map.keySet().iterator().next().contains("com.adcubum")) {
        if (returnparseMap.substring(returnparseMap.length() - 1) == ",") {
            returnparseMap = returnparseMap.substring(0, (returnparseMap.length() - 1))
        }
        returnparseMap = returnparseMap + "},"
    }
}

def parseArray(array) {
    array.each {
        if (it instanceof Map) {
            parseMap2(it)
        } else if (it instanceof List) {
            parseArray(it)
        }
    }
}

logger.debug("Start convert_input")

String partnerId

try {
    def contentType
    Integer loopcounter
    def status
    def headers

    def useTestfile = task.getVariable("useTestfile")

    // load transaction file
    def transactionRead
    if (useTestfile) {
        transactionRead = resources.get("deployment:/test/message.json").load("json")
    } else {
        transactionRead = resources.get("workorder:/message.json").load("json")
    }

    // Individual ArrayList object for each Syrius table
    ArrayList natPerson = []

    //logger.debug("transaction: " + transactionRead)

    def parsedTransaction = parseMap(transactionRead)

    //logger.debug("parsed Map: " + parsedTransaction)

    def transaction = new JsonSlurperClassic().parseText(parsedTransaction)
    // service components
    Map inboundTrafoSyriusFull = [:]
    Map I = [:]
    ArrayList RowList = []

    Map natPersonMap = transaction.get("natPerson")
    Map jurPersonMap = transaction.get("jurPerson")
    String notifikationsart = transaction.get("notifikationsart")
    Map domiziladresseMap = transaction.get("domiziladresse")
    Map zusatzadresseMap = transaction.get("zusatzadresse")
    Map emailMap = transaction.get("email")
    Map telefonMap = transaction.get("telefon")
    Map webMap = transaction.get("web")
    Map partnerrolleMap = transaction.get("partnerrolle")
    Map dubletteMap = transaction.get("dublette")

    String aenderungPer = transaction.get("aenderungPer")

    logger.debug("aenderungPer: " + aenderungPer)

    String aenderungQuelle = transaction.get("aenderungQuelle")

    logger.debug("aenderungQuelle: " + aenderungQuelle)


    // fill list per table and add table list as service row
    Map Row = [:]

    Row.put("partnerId", partnerId)
    Row.put("notifikationsart", notifikationsart)
    String message_flag = ""

    if (natPersonMap != null) {
        if (!natPersonMap.isEmpty()) {
            partnerId = natPersonMap.get("id")
            Row.put("natPerson", natPersonMap)
            Row.put("aenderungPer", aenderungPer)
            Row.put("aenderungQuelle", aenderungQuelle)
            message_flag = "X"
            task.setVariable("spectrumDataFlow", "inboundTrafoSyriusMFNaturalPerson")
        }
    }

    if (jurPersonMap != null) {
        if (!jurPersonMap.isEmpty()) {
            partnerId = jurPersonMap.get("id")
            Row.put("jurPerson", jurPersonMap)
            Row.put("aenderungPer", aenderungPer)
            Row.put("aenderungQuelle", aenderungQuelle)
            message_flag = "X"
            task.setVariable("spectrumDataFlow", "inboundTrafoSyriusMFLegalPerson")

        }
    }

    if (domiziladresseMap != null) {
        if (!domiziladresseMap.isEmpty()) {
            partnerId = domiziladresseMap.get("partnerId")
            Row.put("domiziladresse", domiziladresseMap)
            Row.put("aenderungPer", aenderungPer)
            Row.put("aenderungQuelle", aenderungQuelle)
            message_flag = "X"
            task.setVariable("spectrumDataFlow", "inboundTrafoSyriusMFLocation")

        }
    }

    if (zusatzadresseMap != null) {
        if (!zusatzadresseMap.isEmpty()) {
            partnerId = zusatzadresseMap.get("partnerId")
            Row.put("zusatzadresse", zusatzadresseMap)
            Row.put("aenderungPer", aenderungPer)
            Row.put("aenderungQuelle", aenderungQuelle)
            message_flag = "X"
            task.setVariable("spectrumDataFlow", "inboundTrafoSyriusMFLocation")
        }
    }

    if (emailMap != null) {
        if (!emailMap.isEmpty()) {
            partnerId = emailMap.get("partnerId")
            Row.put("email", emailMap)
            Row.put("aenderungPer", aenderungPer)
            Row.put("aenderungQuelle", aenderungQuelle)
            message_flag = "X"
            task.setVariable("spectrumDataFlow", "inboundTrafoSyriusMFChannel")
        }
    }

    if (telefonMap != null) {
        if (!telefonMap.isEmpty()) {
            partnerId = telefonMap.get("partnerId")
            Row.put("telefon", telefonMap)
            Row.put("aenderungPer", aenderungPer)
            Row.put("aenderungQuelle", aenderungQuelle)
            message_flag = "X"
            task.setVariable("spectrumDataFlow", "inboundTrafoSyriusMFChannel")
        }
    }

    if (webMap != null) {
        if (!webMap.isEmpty()) {
            partnerId = webMap.get("partnerId")
            Row.put("web", webMap)
            Row.put("aenderungPer", aenderungPer)
            Row.put("aenderungQuelle", aenderungQuelle)
            message_flag = "X"
            task.setVariable("spectrumDataFlow", "inboundTrafoSyriusMFChannel")
        }
    }

    if (partnerrolleMap != null) {
        if (!partnerrolleMap.isEmpty()) {
            partnerId = partnerrolleMap.get("partnerId")
            Row.put("partnerrolle", partnerrolleMap)
            Row.put("aenderungPer", aenderungPer)
            Row.put("aenderungQuelle", aenderungQuelle)
            message_flag = "X"
            task.setVariable("spectrumDataFlow", "inboundTrafoSyriusMFPartnerrolle")
        }
    }

    if (dubletteMap != null) {
        // ignorieren für Etappe 1
        /*
        if (!dubletteMap.isEmpty()) {
            partnerId = dubletteMap.get("partnerId")
            Row.put("dublette", dubletteMap)
            Row.put("aenderungPer", aenderungPer)
            Row.put("aenderungQuelle", aenderungQuelle)
            message_flag = "X"
            task.setVariable("spectrumDataFlow", "inboundTrafoSyriusMF")
        }
         */
    }

    if (message_flag == "X") {
        RowList.add(Row)
        // add all rows to "Row"
        I.put("Row", RowList)

        // add all rows to "I" to complete service Map
        inboundTrafoSyriusFull.put("I", I)

        def mapAsJson = JsonOutput.toJson(inboundTrafoSyriusFull)
        //logger.debug("jsonMap: " + JsonOutput.prettyPrint(mapAsJson.toString()))

        // store service Map in workorder variable
        task.setVariable("jsonMap", inboundTrafoSyriusFull)
        task.setVariable("error", false)
    } else {
        task.setVariable("error", true)
    }

    // Update MDM Transaction Status, update IDNumber and IDType
    functions.MDMTransactionStatus.updateIDNumber(
            task.getVariable("system.workorder.id"),
            workOrder.owner,
            "SYRIUS",
            partnerId,
            task.getVariable("transactionSource"),
            task.getVariable("transactionSourceProcessID"),
            task.getVariable("transactionType")
    )

    task.setVariable("KUPE_LAUFNUMMER", partnerId)

    // try to read MDMPartnerID
    def serviceResultsReadMDMPartnerID
    String serviceResultText
    String serviceReadMDMPartnerID = "readMDMPartnerIDfromIdentificationNumber"
    Map serviceMapReadMDMPartnerID = [:]
    Map ReadMDMPartnerID = [:]
    ArrayList RowListReadMDMPartnerID = []
    
    if(partnerId != null){
    	partnerId = partnerId.toLowerCase()
    }
    
    ReadMDMPartnerID.put("IDNumber", partnerId)
    ReadMDMPartnerID.put("IDType", "2")
    RowListReadMDMPartnerID.add(ReadMDMPartnerID)
    // add all rows to "Row"
    Map IReadMDMPartnerID = [:]
    IReadMDMPartnerID.put("Row", RowListReadMDMPartnerID)
    // add all rows to "I" to complete service Map
    serviceMapReadMDMPartnerID.put("I", IReadMDMPartnerID)
// Call spectrum
    contentType = ""
    loopcounter = 0
    while (contentType != "application/json") {
        try {
            spectrum.request(serviceReadMDMPartnerID)
                    .object("json", serviceMapReadMDMPartnerID)
                    .acceptJSON()
                    .execute({ info, inStream ->
                        status = info.status
                        serviceResultText = inStream.getText(info.contentEncoding)
                        contentType = info.getContentType()
                    })
        }
        catch (Exception ex) {
            logger.error("Error calling Spectrum service (" + serviceReadMDMPartnerID + "): " + ex.toString())
            loopcounter++
            if (loopcounter == 5) {
                throw new Exception("Error calling Spectrum service (" + serviceReadMDMPartnerID + "): Retry Limit of 5 reached")
            }
            continue
        }
        if (contentType == "application/json") {
            // Service returned a json, so return a Map object
            serviceResultsReadMDMPartnerID = new JsonSlurperClassic().parseText(serviceResultText)

        } else {
            logger.error("Error calling Spectrum service (" + serviceReadMDMPartnerID + "): " + status + ", " + serviceResultText)
            throw new Exception("Error calling Spectrum service (" + serviceReadMDMPartnerID + "): " + status + ", " + serviceResultText)
        }
    }

    def MDMPartnerID = serviceResultsReadMDMPartnerID.O.first().get("MDMPartnerID")
    logger.debug("MDM PartnerID: " + MDMPartnerID)
    task.setVariable("MDMPartnerID", MDMPartnerID)
    if(MDMPartnerID != null) {
        // Step 4: Update MDM Status, update MDMPartnerID Status
        functions.MDMTransactionStatus.updateMDMPartnerID(task.getVariable("system.workorder.id"), workOrder.owner, MDMPartnerID)
    }

}
catch (Exception ex) {
    logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": " + ex.toString())
    task.setVariable("error", true)
    task.setVariable("update_flag", false)
    task.setVariable("error_message", ex.toString())
}

logger.debug("End convert_input")