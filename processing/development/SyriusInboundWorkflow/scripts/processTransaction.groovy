import groovy.json.JsonOutput
import groovy.json.JsonSlurperClassic

import java.sql.Timestamp
import java.time.Instant

logger.debug("Start processTransaction")
try {

    Instant BITEMPMDMTimestamp = task.getVariable("BITEMPMDMTimestamp")
    String MDMPartnerID = ""
    Boolean MDMStatus

    def contentType
    Integer loopcounter
    def serviceResultText
    def status
    def headers
    def service = task.getVariable("spectrumDataFlow")

    // get task variables
    def serviceMap = task.getVariable("jsonMap")
    def useSpectrum = task.getVariable("useSpectrum")

    // call spectrum syrius outbound trafo REST service POST method
    logger.debug("Call Spectrum REST service ${service}")

    // set udate flag to false as initial value
    boolean update_flag = false
    task.setVariable("update_flag", false)
    task.setVariable("error", true)

    def mapAsJson = JsonOutput.toJson(serviceMap)
    logger.debug("serviceMap as Json: " + JsonOutput.prettyPrint(mapAsJson.toString()))

    if (!useSpectrum) {
        logger.debug("Service ${service} will not be be called, useSpectrum == false")
        return
    }

    // Call spectrum
    if (service != "inboundTrafoSyriusMFPartnerrolle") {
	    contentType = ""
	    loopcounter = 0
	    while (contentType != "application/json") {
	        try {
	            spectrum.request(service)
	                    .object("json", serviceMap)
	                    .acceptJSON()
	                    .execute({ info, inStream ->
	                        status = info.status
	                        serviceResultText = inStream.getText(info.contentEncoding)
	                        contentType = info.getContentType()
	                    })
	        }
	        catch (Exception ex) {
                logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": Error calling Spectrum service (" + service + "): " + ex.toString())
	            loopcounter++
	            if (loopcounter == 5) {
	                throw new Exception("Error calling Spectrum service (" + service + "): Retry Limit of 5 reached")
	            }
	            continue
	        }
	        if (contentType == "application/json") {
	            // Service returned a json, so return a Map object
	            serviceResults = new JsonSlurperClassic().parseText(serviceResultText)
	
	        } else {
                logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": Error calling Spectrum service (" + service + "): " + status + ", " + serviceResultText)
	            throw new Exception("Error calling Spectrum service (" + service + "): " + status + ", " + serviceResultText)
	        }
	    }
    

	    def serviceResultsPretty = JsonOutput.prettyPrint(JsonOutput.toJson(serviceResults).toString())
	    logger.debug("serviceResults: " + serviceResultsPretty)
	
	    // return webservice response to workflow
	    update_flag = serviceResults.BusinessPartnerID[0].hasUpdate
    } 
    
    logger.debug("hasUpdate: " + update_flag)
    task.setVariable("update_flag", update_flag)

    if (update_flag) {
        MDMPartnerID = serviceResults.get("BusinessPartnerID").first().get("TECHMDMPartnerID")
    } else {
        MDMPartnerID = task.getVariable("KUPE_LAUFNUMMER").toUpperCase()
    }

    // Step 4: Update MDM Status, update TransactionCommitted Status
    functions.MDMTransactionStatus.updateTransactionCommittedStatus(task.getVariable("system.workorder.id"), workOrder.owner, true, update_flag)

    // Step 4: Update MDM Status, update MDMPartnerID Status
    functions.MDMTransactionStatus.updateMDMPartnerID(task.getVariable("system.workorder.id"), workOrder.owner, MDMPartnerID)

    //create json for ReadFullPartnerAllVersionsFromDate
    if (update_flag == true) {

        // write kafka message
        resources.get("/kafka_out_change.json").save("json", serviceResults)

        // Step 5: Update MDM Status, update ChangeOutboundGenerated Status
        functions.MDMTransactionStatus.updateTransactionStatus(task.getVariable("system.workorder.id"), workOrder.owner, 2, true, MDMPartnerID, Timestamp.from(BITEMPMDMTimestamp), serviceResults, [resources: resources])

        // extract kafka_key
        Map isBusinessPartner = serviceResults.get("BusinessPartnerID").first().get("isBusinessPartner").first()
        isBusinessPartner.remove("BusinessPartner")
        isBusinessPartner.remove("TECHInternalPID")
        String kafka_key = JsonOutput.toJson(isBusinessPartner).toString()
        def isBusinessPartnerPretty = JsonOutput.prettyPrint(JsonOutput.toJson(isBusinessPartner).toString())
        task.setVariable("kafka_key", kafka_key)
        logger.debug("kafka_key: " + isBusinessPartnerPretty)

        task.setVariable("error", false)

        // load service definition for ReadFullPartnerAllVersionsFromDate
        String service1 = "readFullPartnerAllVersionsFromDate"
        def serviceMap1 = resources.get("deployment:/json/service/${service1}.json").load("json")

        // Fill service map from object map
        try {
            serviceMap1.I.Row[0].TECHMDMPartnerID = serviceResults.BusinessPartnerID[0].TECHMDMPartnerID
            serviceMap1.I.Row[0].CRUD = serviceResults.BusinessPartnerID[0].CRUD
            serviceMap1.I.Row[0].TECHInternalPIDIn = serviceResults.BusinessPartnerID[0].TECHInternalPID
            serviceMap1.I.Row[0].TECHSourceSystemIn = serviceResults.BusinessPartnerID[0].TECHSourceSystem
            serviceMap1.I.Row[0].keyDate = new Date().format("dd.MM.yyyy")
        }
        catch (Exception ex) {
        }

        logger.debug("serviceMap1: " + serviceMap1)
        // Call spectrum
        contentType = ""
        loopcounter = 0
        while (contentType != "application/json") {
            try {
                spectrum.request(service1)
                        .object("json", serviceMap1)
                        .acceptJSON()
                        .execute({ info, inStream ->
                            status = info.status
                            serviceResultText = inStream.getText(info.contentEncoding)
                            contentType = info.getContentType()
                        })
            }
            catch (Exception ex) {
                logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": Error calling Spectrum service (" + service1 + "): " + ex.toString())
                loopcounter++
                if (loopcounter == 5) {
                    throw new Exception("Error calling Spectrum service (" + service1 + "): Retry Limit of 5 reached")
                }
                continue
            }
            if (contentType == "application/json") {
                // Service returned a json, so return a Map object
                serviceResults1 = new JsonSlurperClassic().parseText(serviceResultText)

            } else {
                logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": Error calling Spectrum service (" + service1 + "): " + status + ", " + serviceResultText)
                throw new Exception("Error calling Spectrum service (" + service1 + "): " + status + ", " + serviceResultText)
            }
        }

        if (serviceResults in Map) {

            def serviceResultsPretty1 = JsonOutput.prettyPrint(JsonOutput.toJson(serviceResults1).toString())
            logger.debug("serviceResults1: " + serviceResultsPretty1)

            // write kafka message
            resources.get("/kafka_out_state.json").save("json", serviceResults1)
            task.setVariable("state_kafka", true)

            // Step 6: Update MDM Status, update StateOutboundGenerated Status
            functions.MDMTransactionStatus.updateTransactionStatus(task.getVariable("system.workorder.id"), workOrder.owner, 3, true, MDMPartnerID, Timestamp.from(BITEMPMDMTimestamp), serviceResults1, [resources: resources])

            // call kafka producer for change topic
            logger.debug("Call Kafka producer for MDM Change Topic (" + application.getProperty("mdm.outboundChangeKafkaTopic") + ")")
            logger.debug("Kafka key: " + kafka_key)

            messaging.message(application.getProperty("mdm.outboundChangeKafkaTopic"))
                    .nodeId("AGENT_MESSAGING_KAFKA")
                    .source("/kafka_out_change.json")
                    .property("key", kafka_key)
                    .property("converter", "Json2Avro")
                    .property("converter.subject", "ch-mdm.change.all")
                    .property("converter.version", task.getVariable("avro.schema.change.version"))
                    .send()


            // Step 7: Update MDM Status, update ChangeOutboundCommitted Status
            functions.MDMTransactionStatus.updateTransactionStatus(task.getVariable("system.workorder.id"), workOrder.owner, 4, true)

            // call kafka producer for state topic
            logger.debug("Call Kafka producer for MDM State Topic (" + application.getProperty("mdm.outboundStateKafkaTopic") + "), key: " + kafka_key)

            messaging.message(application.getProperty("mdm.outboundStateKafkaTopic"))
                    .nodeId("AGENT_MESSAGING_KAFKA")
                    .source("/kafka_out_state.json")
                    .property("key", kafka_key)
                    .property("converter", "Json2Avro")
                    .property("converter.subject", "ch-mdm.state.all")
                    .property("converter.version", task.getVariable("avro.schema.state.version"))
                    .send()

            // Step 8: Update MDM Status, update StateOutboundCommitted Status
            functions.MDMTransactionStatus.updateTransactionStatus(task.getVariable("system.workorder.id"), workOrder.owner, 5, true)
        }
    } else {
        // As there is no Delta, no output is generated
        List statusObject = []
        // Step 5: Update MDM Status, update ChangeOutboundGenerated Status
        functions.MDMTransactionStatus.updateTransactionStatus(task.getVariable("system.workorder.id"), workOrder.owner, 2, true)
        // Step 6: Update MDM Status, update StateOutboundGenerated Status
        functions.MDMTransactionStatus.updateTransactionStatus(task.getVariable("system.workorder.id"), workOrder.owner, 3, true)
        // Step 7: Update MDM Status, update ChangeOutboundCommitted Status
        functions.MDMTransactionStatus.updateTransactionStatus(task.getVariable("system.workorder.id"), workOrder.owner, 4, true)
        // Step 8: Update MDM Status, update StateOutboundCommitted Status
        functions.MDMTransactionStatus.updateTransactionStatus(task.getVariable("system.workorder.id"), workOrder.owner, 5, true)

        task.setVariable("error", false)
    }
}
catch (Exception ex) {
    logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": " + ex.toString())
    task.setVariable("error", true)
    task.setVariable("update_flag", false)
    task.setVariable("error_message", ex.toString())
}

logger.debug("End processTransaction")