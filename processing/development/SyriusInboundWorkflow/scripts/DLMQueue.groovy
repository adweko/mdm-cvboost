import groovy.transform.Field
import java.sql.Timestamp
import java.time.Instant

logger.debug("Start DLMQueue")
try {

    def useTestfile = task.getVariable("useTestfile")

    // load transaction file
    def transaction
    if (useTestfile) {
        transaction = resources.get("deployment:/test/message.json").load "json"
    } else {
        transaction = resources.get("workorder:/message.json").load("json")
    }

    // Get BusinessPartnerID
    def businessPartnerID = task.getVariable("KUPE_LAUFNUMMER")

    // Get ProcessPID = WorkorderID
    def processID = task.getVariable("system.workorder.id")

    // Get DLMTestMode
    def DLMTestMode = task.getVariable("DLMTestMode")

    // DLMQueue
    Instant BITEMPMDMTimestamp =  functions.DLMQueueManagement.queue(businessPartnerID, processID, DLMTestMode)
    if (BITEMPMDMTimestamp == null) {
        throw new Exception("DLMQueueManagement: queue Error")
    }
    task.setVariable("BITEMPMDMTimestamp", BITEMPMDMTimestamp)

    def MDMPartnerID = task.getVariable("MDMPartnerID")
    if (MDMPartnerID != null){
        // Transaction is an update to an existing partner, so also queue this
        Instant BITEMPMDMTimestampUpdate = functions.DLMQueueManagement.queue(MDMPartnerID, processID, DLMTestMode)
        if (BITEMPMDMTimestampUpdate == null) {
            throw new Exception("DLMQueueManagement: queue Error")
        }
        task.setVariable("BITEMPMDMTimestampUpdate", BITEMPMDMTimestampUpdate)
    }

    String IDNumber = task.getVariable("KUPE_LAUFNUMMER")

    // Update MDM Status, update TransactionQueued Status
    functions.MDMTransactionStatus.updateTransactionQueuedStatus(task.getVariable("system.workorder.id"), workOrder.owner, true, Timestamp.from(BITEMPMDMTimestamp), IDNumber, transaction, [resources: resources])
    
}
catch (Exception ex) {
    logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": " + ex.toString())
    task.setVariable("error", true)
    task.setVariable("update_flag", false)
    task.setVariable("error_message", ex.toString())
}

logger.debug("End DLMQueue")