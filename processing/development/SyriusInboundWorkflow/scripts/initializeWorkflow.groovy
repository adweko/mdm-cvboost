import java.util.Properties

logger.debug("Start initializeWorkflow")

task.setVariable("error", false)

try {
// Use Kafka true/false
    task.setVariable("useKafka", true)
    logger.debug("useKafka: " + task.getVariable("useKafka"))

// Use Spectrum true/false
    task.setVariable("useSpectrum", true)
    logger.debug("useSpectrum: " + task.getVariable("useSpectrum"))

// use testfile
    task.setVariable("useTestfile", false)
    logger.debug("useTestfile: " + task.getVariable("useTestfile"))

// set Distributed Lock Manager DLM test mode usage to true or false
    task.setVariable("DLMTestMode", false)
    logger.debug("DLMTestMode: " + task.getVariable("DLMTestMode"))

// Create MDM Transaction Status
    functions.MDMTransactionStatus.createTransactionStatus(task.getVariable("system.workorder.id"), workOrder.creator, workOrder.owner)

// Read the file "avro_schema.versions" located in the home of cvboost
// and set task variables to point to the versions read from there
    def homeDir = application.getProperty("app.home")
    def avroSchemaFilePath = resources.get("system:" + homeDir + "/avro/schema.properties")
    Properties properties = new Properties()
    avroSchemaFilePath.load(
    	{
    		inputStream -> properties.load(inputStream)
    	}
    	)
    //properties.load(new FileInputStream(avroSchemaFilePath))

    def stateSchemaVersion = properties.get("ch-mdm.state.all")
    if (stateSchemaVersion != null) {
        task.setVariable("avro.schema.state.version", stateSchemaVersion)
    } else {
        throw new RuntimeException("The property ch-mdm.state.all is not set in " + avroSchemaFilePath)
    }

    def changeSchemaVersion = properties.get("ch-mdm.change.all")
    if (changeSchemaVersion != null) {
        task.setVariable("avro.schema.change.version", changeSchemaVersion)
    } else {
        throw new RuntimeException("The property ch-mdm.state.all is not set in " + avroSchemaFilePath)
    }

    if (task.getVariable("transactionSource") == null){
        // Variable was not transfered by caller, so create default value 2 = extern
        task.setVariable("transactionSource", 2)
    }

    if (task.getVariable("transactionSourceProcessID") == null){
        // Variable was not transfered by caller, so create default value unknown
        task.setVariable("transactionSourceProcessID", "unknown")
    }

    if (task.getVariable("transactionType") == null){
        // Variable was not transfered by caller, so create default value 1 = change
        task.setVariable("transactionType", 1)
    }

}
catch (Exception ex) {
    logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": " + ex.toString())
    task.setVariable("error", true)
    task.setVariable("update_flag", false)
    task.setVariable("error_message", ex.toString())
}

logger.debug("End initializeWorkflow")