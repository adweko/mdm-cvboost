logger.debug("Start processFormMDMDeltaLoadTable")

// get form output
formAction = task.getVariable("formAction")
task.setVariable("formAction", "refresh")

formOutput = task.getVariable("MDMDeltaLoadTableOutput")

try {

    switch (formAction) {
        case "exit":
            // exit workorder
            task.setVariable("formAction", "exit")
            break

        case "refresh":
            break

        case "deltaLoadStart":
            task.setVariable("formAction", "deltaLoadStart")
            break

        case "unlock":
            task.setVariable("formAction", "unlock")
            try {
                functions.DLMQueueManagement.unlockReprocessing()
            }
            catch (Exception ex) {
                task.setVariable("formAction", "refresh")
            }
            break
    }
}
catch (Exception ex) {
    logger.error(ex.toString())
}
finally {

}
logger.debug("End processFormMDMDeltaLoadTable")