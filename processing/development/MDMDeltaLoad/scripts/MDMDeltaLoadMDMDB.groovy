import groovy.json.JsonOutput
import groovy.json.JsonSlurperClassic

import java.sql.Timestamp
import java.time.Instant

def workorderID = task.getVariable("system.workorder.id")
Integer deltaLoadNumberOfTransactions = 0
def workorderOwner = workorder.owner
def workorderCreator = workorder.creator

try {
    logger.debug("Start deltaLoadMDMDB")

    Boolean DLMTestMode = false
    def contentType
    Integer loopcounter
    def status

    task.setVariable("error", false)
    task.setVariable("error_message", "")

    Instant workorderStartTime = task.getVariable("workorderStartTime")
    String mdmPartnerID = task.getVariable("TECHMDMPartnerID")
    Timestamp queryTimestamp = Timestamp.from(workorderStartTime)

    // lock reprocessing
    boolean lockStatus = functions.DLMQueueManagement.lockReprocessing()

    if (!lockStatus) {
        throw new Exception("MDMDeltaLoad: Reprocessing already locked")
    }
    logger.debug("Reprocessing locked")

    com.compoverso.dmp.extension.core.connector.service.jdbc.builder.SelectBuilderImpl select

    if (mdmPartnerID == "%") {
        select = jdbc.select() // select for internal database
                .sql("SELECT * FROM MDM_TRANSACTION_STATUS WHERE WORKORDER_START_TIME_ >= :queryTimestamp ORDER BY WORKORDER_START_TIME_ ASC")
                .parameter("queryTimestamp", queryTimestamp)
    } else {
        select = jdbc.select() // select for internal database
                .sql("SELECT * FROM MDM_TRANSACTION_STATUS WHERE WORKORDER_START_TIME_ >= :queryTimestamp AND MDM_PARTNERID_ LIKE :mdmPartnerID ORDER BY WORKORDER_START_TIME_ ASC")
                .parameter("queryTimestamp", queryTimestamp)
                .parameter("mdmPartnerID", mdmPartnerID)
    }

    // log Delta Load Start in table MDM_DELTA_LOADS
    functions.MDMDeltaLoad.logStartDeltaLoad(workorderID, workorderCreator, workorderOwner, 2, queryTimestamp, mdmPartnerID, task.getVariable("comment"))

    select.query { row ->

        // Read businesspartner data per workorderID from MDM DB
        // =============================================

        logger.debug("row: " + row.toString())

        def datapathInbound = row.get("TRANSACTION_DATAPATH_")
        def idType = row.get("TRANSACTION_IDTYPE_")
        def idNumber = row.get("TRANSACTION_IDNUMBER_")
        def MDMPartnerID = row.get("MDM_PARTNERID_")
        def readWorkorderID = row.get("WORKORDER_ID_")
        Instant BITEMPMDMTimestamp
        Instant BITEMPMDMTimestampUpdate

        // Create MDM Transaction Status
        functions.MDMTransactionStatus.createTransactionStatus(workorderID, workorderCreator, workorderOwner)

        // Update MDM Transaction Status, update IDNumber and IDType
        functions.MDMTransactionStatus.updateIDNumber(workorderID, workorderOwner, idType, idNumber)

        // Update MDM Transaction Status, update MDMPartnerID
        if (MDMPartnerID != null) {
            functions.MDMTransactionStatus.updateMDMPartnerID(workorderID, workorderOwner, MDMPartnerID)
        }

        // Queue external PartnerID and MDMPartnerID
        BITEMPMDMTimestamp = functions.DLMQueueManagement.queue(idNumber, workorderID, DLMTestMode)
        if (BITEMPMDMTimestamp == null) {
            throw new Exception("DLMQueueManagement: queue Error")
        }
        if (MDMPartnerID != null) {
            // Transaction is an update to an existing partner, so also queue this
            BITEMPMDMTimestampUpdate = functions.DLMQueueManagement.queue(MDMPartnerID, workorderID, DLMTestMode)
            if (BITEMPMDMTimestampUpdate == null) {
                throw new Exception("DLMQueueManagement: queue Error")
            }
        }

        // Update MDM Status, update TransactionQueued Status
        functions.MDMTransactionStatus.updateTransactionQueuedStatus(workorderID, workorderOwner, true, Timestamp.from(BITEMPMDMTimestamp), idNumber)

        // Check until we are allowed to proceed
        boolean DLMCheck = false
        while (DLMCheck = false) {
            DLMCheck = functions.DLMQueueManagement.check(idNumber, workorderID, DLMTestMode)
            if (MDMPartnerID != null) {
                boolean DLMCheckUpdate = functions.DLMQueueManagement.check(MDMPartnerID, workorderID, DLMTestMode)
                if (!DLMCheckUpdate) {
                    DLMCheck = false
                }
            }
            wait(3000)
        }

        // Call Spectrum read workorderID data from MDM DB
        //===============================================

        // Parameters for dataflow
        // readWorkorderID aka TECHExternalPID
        // BITEMPMDMTimestamp


        boolean update_flag = false
        def serviceResults
        String serviceResultText
        String serviceReadTransaction = "readTransaction"
        Map serviceMapReadTransaction = [:]
        Map readTransaction = [:]

        readTransaction.put("BITEMPMDMTimestamp", Timestamp.from(BITEMPMDMTimestamp).format("yyyyMMddHHmmssSSS", TimeZone.getTimeZone("UTC")))
        readTransaction.put("TECHExternalPID", readWorkorderID)
        readTransaction.put("TECHExternalPIDNew", workorderID)
        logger.debug("workorderID: " + workorderID)

        // add all rows to "Row"
        Map IReadTransaction = [:]
        IReadTransaction.put("Row", [readTransaction])
        // add all rows to "I" to complete service Map
        serviceMapReadTransaction.put("I", IReadTransaction)
        logger.debug("serviceMapReadTransaction: " + serviceMapReadTransaction)
        // call spectrum

        contentType = ""
        loopcounter = 0
        while (contentType != "application/json") {
            try {
                logger.debug("starting readTransaction")
                spectrum.request(serviceReadTransaction)
                        .object("json", serviceMapReadTransaction)
                        .acceptJSON()
                        .execute({ info, inStream ->
                            status = info.status
                            serviceResultText = inStream.getText(info.contentEncoding)
                            contentType = info.getContentType()
                        })
                logger.debug("finished readTransaction")
            }
            catch (Exception ex) {
                logger.error("Error calling Spectrum service (" + serviceReadTransaction + "): " + ex.toString())
                loopcounter++
                if (loopcounter == 5) {
                    throw new Exception("Error calling Spectrum service (" + serviceReadTransaction + "): Retry Limit of 5 reached")
                }
                continue
            }
            if (contentType == "application/json") {
                // Service returned a json, so return a Map object
                serviceResults = new JsonSlurperClassic().parseText(serviceResultText)

            } else {
                logger.error("Error calling Spectrum service (" + serviceReadTransaction + "): " + status + ", " + serviceResultText)
                throw new Exception("Error calling Spectrum service (" + serviceReadTransaction + "): " + status + ", " + serviceResultText)
            }
        }

        def serviceResultsPretty = JsonOutput.prettyPrint(JsonOutput.toJson(serviceResults).toString())
        logger.debug("serviceResults: " + serviceResultsPretty)

        MDMPartnerID = serviceResults.get("BusinessPartnerID").first().get("TECHMDMPartnerID")

        // return webservice response to workflow
        // update_flag = serviceResults.BusinessPartnerID[0].hasUpdate
        update_flag = true //TODO muss doch immer true sein, oder nicht?

        // Update MDM Status, update TransactionCommitted Status
        functions.MDMTransactionStatus.updateTransactionCommittedStatus(workorderID, workorderOwner, true, update_flag)

        // Update MDM Status, update MDMPartnerID Status
        functions.MDMTransactionStatus.updateMDMPartnerID(workorderID, workorderOwner, MDMPartnerID)

        // create json for ReadFullPartnerAllVersionsFromDate
        if (update_flag == true) {

            // write kafka message
            resources.get("/kafka_out_change.json").save("json", serviceResults)

            // Update MDM Status, update ChangeOutboundGenerated Status
            functions.MDMTransactionStatus.updateTransactionStatus(workorderID, workorderOwner, 2, true, MDMPartnerID, Timestamp.from(BITEMPMDMTimestamp), serviceResults, [resources: resources])

            // extract kafka_key
            Map isBusinessPartner = serviceResults.get("BusinessPartnerID").first().get("isBusinessPartner").first()
            isBusinessPartner.remove("BusinessPartner")
            isBusinessPartner.remove("TECHInternalPID")
            def isBusinessPartnerPretty = JsonOutput.toJson(isBusinessPartner).toString()
            logger.debug("kafka_key: " + isBusinessPartnerPretty)

            // load service definition for ReadFullPartnerAllVersionsFromDate
            String service1 = "readFullPartnerAllVersionsFromDate"
            def serviceMap1 = resources.get("deployment:/json/service/${service1}.json").load("json")

            // Fill service map from object map
            serviceMap1.I.Row[0].TECHMDMPartnerID = serviceResults.BusinessPartnerID[0].TECHMDMPartnerID
            serviceMap1.I.Row[0].CRUD = serviceResults.BusinessPartnerID[0].CRUD
            serviceMap1.I.Row[0].TECHInternalPIDIn = serviceResults.BusinessPartnerID[0].TECHInternalPID
            serviceMap1.I.Row[0].TECHSourceSystemIn = serviceResults.BusinessPartnerID[0].TECHSourceSystem
            serviceMap1.I.Row[0].keyDate = new Date().format("dd.MM.yyyy")

            logger.debug("serviceMap1: " + serviceMap1)

            contentType = ""
            loopcounter = 0
            while (contentType != "application/json") {
                try {
                    spectrum.request(service1)
                            .object("json", serviceMap1)
                            .acceptJSON()
                            .execute({ info, inStream ->
                                status = info.status
                                serviceResultText = inStream.getText(info.contentEncoding)
                                contentType = info.getContentType()
                            })
                }
                catch (Exception ex) {
                    logger.error("Error calling Spectrum service (" + service1 + "): " + ex.toString())
                    loopcounter++
                    if (loopcounter == 5) {
                        throw new Exception("Error calling Spectrum service (" + service1 + "): Retry Limit of 5 reached")
                    }
                    continue
                }
                if (contentType == "application/json") {
                    // Service returned a json, so return a Map object
                    serviceResults1 = new JsonSlurperClassic().parseText(serviceResultText)

                } else {
                    logger.error("Error calling Spectrum service (" + service1 + "): " + status + ", " + serviceResultText)
                    throw new Exception("Error calling Spectrum service (" + service1 + "): " + status + ", " + serviceResultText)
                }
            }

            if (serviceResults1 in Map) {

                def serviceResultsPretty1 = JsonOutput.prettyPrint(JsonOutput.toJson(serviceResults1).toString())
                logger.debug("serviceResults1: " + serviceResultsPretty1)

                // write kafka message
                resources.get("/kafka_out_state.json").save("json", serviceResults1)

                // Update MDM Status, update StateOutboundGenerated Status
                functions.MDMTransactionStatus.updateTransactionStatus(workorderID, workorderOwner, 3, true, MDMPartnerID, Timestamp.from(BITEMPMDMTimestamp), serviceResults1, [resources: resources])

                // call kafka producer for change topic
                logger.debug("Call Kafka producer for MDM Change Topic (" + application.getProperty("mdm.outboundChangeKafkaTopic"))
                logger.debug("Kafka key: " + isBusinessPartnerPretty.toString())
                messaging.message(application.getProperty("mdm.outboundChangeKafkaTopic"))
                        .nodeId("AGENT_MESSAGING_KAFKA")
                        .source("/kafka_out_change.json")
                        .property("key", isBusinessPartnerPretty)
                        .property("converter", "Json2Avro")
                        .property("converter.subject", "ch-mdm.change.all")
                        .property("converter.version", task.getVariable("avro.schema.change.version"))
                        .send()

                // Update MDM Status, update ChangeOutboundCommitted Status
                functions.MDMTransactionStatus.updateTransactionStatus(workorderID, workorderOwner, 4, true)

                // call kafka producer for state topic
                logger.debug("Call Kafka producer for MDM State Topic (" + application.getProperty("mdm.outboundStateKafkaTopic") + "), key: " + isBusinessPartnerPretty.toString())
                messaging.message(application.getProperty("mdm.outboundStateKafkaTopic"))
                        .nodeId("AGENT_MESSAGING_KAFKA")
                        .source("/kafka_out_state.json")
                        .property("key", isBusinessPartnerPretty)
                        .property("converter", "Json2Avro")
                        .property("converter.subject", "ch-mdm.state.all")
                        .property("converter.version", task.getVariable("avro.schema.state.version"))
                        .send()

                // Step 8: Update MDM Status, update StateOutboundCommitted Status
                functions.MDMTransactionStatus.updateTransactionStatus(workorderID, workorderOwner, 5, true)
            }
        } else {
            // As there is no Delta, no output is generated
            List statusObject = []
            // Step 5: Update MDM Status, update ChangeOutboundGenerated Status
            functions.MDMTransactionStatus.updateTransactionStatus(workorderID, workorderOwner, 2, true)
            // Step 6: Update MDM Status, update StateOutboundGenerated Status
            functions.MDMTransactionStatus.updateTransactionStatus(workorderID, workorderOwner, 3, true)
            // Step 7: Update MDM Status, update ChangeOutboundCommitted Status
            functions.MDMTransactionStatus.updateTransactionStatus(workorderID, workorderOwner, 4, true)
            // Step 8: Update MDM Status, update StateOutboundCommitted Status
            functions.MDMTransactionStatus.updateTransactionStatus(workorderID, workorderOwner, 5, true)
        }

        // DLMDequeue
        boolean DLMDequeue = functions.DLMQueueManagement.dequeue(idNumber, workorderID, DLMTestMode)

        if (!DLMDequeue) {
            throw new Exception("functions.DLMQueueManagement: dequeue Error")
        }

        if (MDMPartnerID != null) {
            boolean DLMDequeueUpdate = functions.DLMQueueManagement.dequeue(MDMPartnerID, workorderID, DLMTestMode)
            if (!DLMDequeueUpdate) {
                throw new Exception("DLMQueueManagement: dequeue Error")
            }
        }

        deltaLoadNumberOfTransactions = deltaLoadNumberOfTransactions++
    } // end of closure row ->


} catch (Exception ex) {
    logger.error(ex.toString())
    task.setVariable("error", true)
    task.setVariable("error_message", ex.toString())
}
finally {
    // log Delta Load Start in table MDM_DELTA_LOADS
    functions.MDMDeltaLoad.logEndDeltaLoad(workorderID, deltaLoadNumberOfTransactions, workorderOwner)

    // unlock reprocessing
    functions.DLMQueueManagement.unlockReprocessing()
    logger.debug("Reprocessing unlocked")
}
logger.debug("End deltaLoadMDMDB")
