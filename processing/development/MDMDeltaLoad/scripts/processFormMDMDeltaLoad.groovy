import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter

logger.debug("Start processFormMDMDeltaLoad")

// get form output
formAction = task.getVariable("formAction")
task.setVariable("formAction", "refresh")

formOutput = task.getVariable("MDMDeltaLoadOutput")

try {

    switch (formAction) {
        case "exit":
            // exit workorder
            task.setVariable("formAction", "exit")
            break

        case "refresh":
            // refresh display
            task.setVariable("formAction", "refresh")
            Instant workorderStartTime = LocalDateTime
                    .parse(formOutput.workorderStartTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"))
                    .atZone(ZoneId.of("UTC"))
                    .toInstant()

            task.setVariable("workorderStartTime", workorderStartTime)
            task.setVariable("comment", formOutput.comment)

            break

        case {it == "deltaLoadInboundStage" || it == "deltaLoadMDMDB"  || it == "deltaLoadOutboundStage" || it == "deltaLoadRepublishPartner"}:
            if (functions.DLMQueueManagement.reprocessingLockStatus() == false) {
                // Reprocessing not locked, we can start
                task.setVariable("formAction", "deltaLoad")
                task.setVariable("reprocessingMode", formAction)
                task.setVariable("TECHMDMPartnerID", formOutput.TECHMDMPartnerID)
                task.setVariable("comment", formOutput.comment)
                try {
                    Instant workorderStartTime = LocalDateTime
                            .parse(formOutput.workorderStartTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"))
                            .atZone(ZoneId.of("UTC"))
                            .toInstant()
                    task.setVariable("workorderStartTime", workorderStartTime)
                }
                catch (Exception ex) {
                    task.setVariable("formAction", "refresh")
                    logger.error(ex.toString())
                }
            } else {
                // Reprocessing already locked, we cannot start
                task.setVariable("formAction", "refresh")
            }
            break

    }
}
catch (Exception ex) {
    logger.error(ex.toString())
}
finally {

}
logger.debug("End processFormMDMDeltaLoad")




