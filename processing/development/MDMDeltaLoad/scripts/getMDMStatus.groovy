import java.sql.Timestamp
import java.time.Instant

logger.debug("Start getMDMStatus")

try {

    Map MDMDeltaLoadTableInput = [:]

    // set form field values - moved
    //Map MDMDeltaLoadInput = [:]
    //Instant workorderStartTime = task.getVariable("workorderStartTime")
    //MDMDeltaLoadInput.put("workorderStartTime", Timestamp.from(workorderStartTime).format("yyyyMMddHHmmssSSS", TimeZone.getTimeZone("UTC")))
    //MDMDeltaLoadInput.put("TECHMDMPartnerID", task.getVariable("TECHMDMPartnerID"))
    //task.setVariable("MDMDeltaLoadInput", MDMDeltaLoadInput)

    boolean reprocessingLockStatus
    reprocessingLockStatus = functions.DLMQueueManagement.reprocessingLockStatus()
    if (reprocessingLockStatus == true) {
        MDMDeltaLoadTableInput.put("reprocessingLockStatus", "locked")
    } else {
        MDMDeltaLoadTableInput.put("reprocessingLockStatus", "unlocked")
    }

    // read DeltaLoad runs from DB
    List localDeltaLoadsList = functions.MDMDeltaLoad.getDeltaLoadsList()
    logger.debug("localDeltaLoadsList: " + localDeltaLoadsList.toString())

    Map MDMDeltaLoadsTable = [:]
    MDMDeltaLoadsTable.put("MDMDeltaLoads", localDeltaLoadsList)
    MDMDeltaLoadTableInput.put("MDMDeltaLoadsTable", [MDMDeltaLoadsTable])

    task.setVariable("MDMDeltaLoadTableInput", MDMDeltaLoadTableInput)

}
catch (Exception ex) {
    logger.error(ex.toString())
    task.setVariable("error", true)
    task.setVariable("error_message", ex.toString())
}
finally {

}
logger.debug("End getMDMStatus")