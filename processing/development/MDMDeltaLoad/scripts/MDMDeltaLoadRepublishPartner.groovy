import groovy.json.JsonOutput
import groovy.json.JsonSlurperClassic

import java.sql.Timestamp
import java.time.Instant

def workorderID = task.getVariable("system.workorder.id")
def workorderOwner = workorder.owner
def workorderCreator = workorder.creator

try {
    logger.debug("Start deltaLoadRepublishPartner")

    Boolean DLMTestMode = false
    def contentType
    Integer loopcounter
    def status

    task.setVariable("error", false)
    task.setVariable("error_message", "")

    Instant workorderStartTime = task.getVariable("workorderStartTime")
    String MDMPartnerID = task.getVariable("TECHMDMPartnerID")
    Timestamp queryTimestamp = Timestamp.from(workorderStartTime)

    // lock reprocessing
    boolean lockStatus = functions.DLMQueueManagement.lockReprocessing()

    if (!lockStatus) {
        throw new Exception("MDMDeltaLoad: Reprocessing already locked")
    }
    logger.debug("Reprocessing locked")

    // log Delta Load Start in table MDM_DELTA_LOADS
    functions.MDMDeltaLoad.logStartDeltaLoad(workorderID, workorderCreator, workorderOwner, 4, queryTimestamp, MDMPartnerID, task.getVariable("comment"))

    Instant BITEMPMDMTimestamp
    Instant BITEMPMDMTimestampUpdate

    // Create MDM Transaction Status
    functions.MDMTransactionStatus.createTransactionStatus(workorderID, workorderCreator, workorderOwner)

    // Update MDM Transaction Status, update IDNumber and IDType
    functions.MDMTransactionStatus.updateIDNumber(workorderID, workorderOwner, null, null, 6, workorderID, 3)

    // Update MDM Transaction Status, update MDMPartnerID
    if (MDMPartnerID != null) {
        functions.MDMTransactionStatus.updateMDMPartnerID(workorderID, workorderOwner, MDMPartnerID)
    }

    // Queue MDMPartnerID
    BITEMPMDMTimestamp = functions.DLMQueueManagement.queue(MDMPartnerID, workorderID, DLMTestMode)
    if (BITEMPMDMTimestamp == null) {
        throw new Exception("DLMQueueManagement: queue Error")
    }

    // Update MDM Status, update TransactionQueued Status
    functions.MDMTransactionStatus.updateTransactionQueuedStatus(workorderID, workorderOwner, true, Timestamp.from(BITEMPMDMTimestamp), null)

    // Check until we are allowed to proceed
    boolean DLMCheck = false
    while (DLMCheck = false) {
        DLMCheck = functions.DLMQueueManagement.check(MDMPartnerID, workorderID, DLMTestMode)
        if (!DLMCheck) {
            wait(3000)
        }
    }

// load service definition for ReadFullPartnerAllVersionsFromDate
    String service1 = "readFullPartnerAllVersionsFromDate"
    def serviceMap1 = resources.get("deployment:/json/service/${service1}.json").load("json")

// Fill service map
    serviceMap1.I.Row[0].TECHMDMPartnerID = MDMPartnerID
    serviceMap1.I.Row[0].keyDate = "01.01.1900" // read complete partner with all versions

    logger.debug("serviceMap1: " + serviceMap1)

    contentType = ""
    loopcounter = 0
    while (contentType != "application/json") {
        try {
            spectrum.request(service1)
                    .object("json", serviceMap1)
                    .acceptJSON()
                    .execute({ info, inStream ->
                        status = info.status
                        serviceResultText = inStream.getText(info.contentEncoding)
                        contentType = info.getContentType()
                    })
        }
        catch (Exception ex) {
            logger.error("Error calling Spectrum service (" + service1 + "): " + ex.toString())
            loopcounter++
            if (loopcounter == 5) {
                throw new Exception("Error calling Spectrum service (" + service1 + "): Retry Limit of 5 reached")
            }
            continue
        }
        if (contentType == "application/json") {
            // Service returned a json, so return a Map object
            serviceResults1 = new JsonSlurperClassic().parseText(serviceResultText)

        } else {
            logger.error("Error calling Spectrum service (" + service1 + "): " + status + ", " + serviceResultText)
            throw new Exception("Error calling Spectrum service (" + service1 + "): " + status + ", " + serviceResultText)
        }
    }

    if (serviceResults1 in Map) {

        def serviceResultsPretty1 = JsonOutput.prettyPrint(JsonOutput.toJson(serviceResults1).toString())
        logger.debug("serviceResults1: " + serviceResultsPretty1)

        // create kafkaKey
        Map kafkaKey = [:]
        kafkaKey.put("TECHMDMPartnerID", MDMPartnerID)
        kafkaKey.put("TECHExternalPID", workorderID.toLowerCase())
        kafkaKey.put("TECHSourceSystem", "MDM")
        kafkaKey.put("BITEMPMDMTimestamp", Timestamp.from(BITEMPMDMTimestamp).format("yyyyMMddHHmmssSSS").toString())

        def kafkaKeyPretty = JsonOutput.toJson(kafkaKey).toString()
        logger.debug("kafka Key: " + kafkaKeyPretty)

        // write kafka state message
        resources.get("/kafka_out_state.json").save("json", serviceResults1)

        // Update MDM Status, update StateOutboundGenerated Status
        functions.MDMTransactionStatus.updateTransactionStatus(workorderID, workorderOwner, 3, true, MDMPartnerID, Timestamp.from(BITEMPMDMTimestamp), serviceResults1, [resources: resources])

        // create unlock Partner message
        Map unlockPartner = [:]
        unlockPartner.put("TECHMDMPartnerID", MDMPartnerID)
                unlockPartner.put("RepublishedAt", Timestamp.from(BITEMPMDMTimestamp).format("yyyyMMddHHmmssSSS", TimeZone.getTimeZone("UTC")))

        // write partnerUnlock message
        resources.get("/partner_unlock.json").save("json", unlockPartner)
        logger.debug("unlockPartner Message: " + JsonOutput.prettyPrint(JsonOutput.toJson(unlockPartner).toString()))

        // call kafka producer for unlockPartner topic
        logger.debug("Call Kafka producer for MDM unlockPartner Topic (" + application.getProperty("mdm.unlockPartnerKafkaTopic"))
        messaging.message(application.getProperty("mdm.unlockPartnerKafkaTopic"))
                .nodeId("AGENT_MESSAGING_KAFKA")
                .source("/partner_unlock.json")
                .property("key", kafkaKeyPretty)
                .send()

        // call kafka producer for change topic for each version
        logger.debug("Call Kafka producer for MDM Change Topic (" + application.getProperty("mdm.outboundChangeKafkaTopic") + " for each version")

        serviceResults1."BusinessPartnerID".first()."Versions".each{ row ->
            

            // write kafka state message
            row.remove("Version")
            row.put("hasUpdate", true)
            row.put("user_fields", [])
            row.put("TECHUser", workorder.owner)
            Map rowPartner = ["BusinessPartnerID": [row]]
            resources.get("/kafka_out_version.json").save("json", rowPartner)

            logger.debug("rowPartner: " + JsonOutput.prettyPrint(JsonOutput.toJson(rowPartner).toString()))

			// extract kafka_key
            Map kafkaRowKey = row.get("isBusinessPartner").first()
            kafkaRowKey.remove("BusinessPartner")
            kafkaRowKey.remove("TECHInternalPID")
            def kafkaRowKeyPretty = JsonOutput.toJson(kafkaRowKey).toString()

            logger.debug("Kafka key: " + kafkaRowKeyPretty.toString())
            // extract message

            // call producer
            messaging.message(application.getProperty("mdm.outboundChangeKafkaTopic"))
                    .nodeId("AGENT_MESSAGING_KAFKA")
                    .source("/kafka_out_version.json")
                    .property("key", kafkaRowKeyPretty)
                    .property("converter", "Json2Avro")
                    .property("converter.subject", "ch-mdm.change.all")
                    .property("converter.version", task.getVariable("avro.schema.change.version"))
                    .send()
        }

        // Update MDM Status, update ChangeOutboundGenerated Status
        functions.MDMTransactionStatus.updateTransactionStatus(workorderID, workorderOwner, 2, true, MDMPartnerID, Timestamp.from(BITEMPMDMTimestamp), serviceResults1, [resources: resources])

        // Update MDM Status, update ChangeOutboundCommitted Status
        functions.MDMTransactionStatus.updateTransactionStatus(workorderID, workorderOwner, 4, true)

        // call kafka producer for state topic
        logger.debug("Call Kafka producer for MDM State Topic (" + application.getProperty("mdm.outboundStateKafkaTopic"))
        messaging.message(application.getProperty("mdm.outboundStateKafkaTopic"))
                .nodeId("AGENT_MESSAGING_KAFKA")
                .source("/kafka_out_state.json")
                .property("key", kafkaKeyPretty)
                .property("converter", "Json2Avro")
                .property("converter.subject", "ch-mdm.state.all")
                .property("converter.version", task.getVariable("avro.schema.state.version"))
                .send()

        // Step 8: Update MDM Status, update StateOutboundCommitted Status
        functions.MDMTransactionStatus.updateTransactionStatus(workorderID, workorderOwner, 5, true)
    }

// DLMDequeue
    boolean DLMDequeue = functions.DLMQueueManagement.dequeue(MDMPartnerID, workorderID, DLMTestMode)
    if (!DLMDequeue) {
        throw new Exception("DLMQueueManagement: dequeue Error")
    }
}
catch (Exception ex) {
    logger.error(ex.toString())
    task.setVariable("error", true)
    task.setVariable("error_message", ex.toString())
}
finally {
    // log Delta Load Start in table MDM_DELTA_LOADS
    functions.MDMDeltaLoad.logEndDeltaLoad(workorderID, 1, workorderOwner)

    // unlock reprocessing
    functions.DLMQueueManagement.unlockReprocessing()
    logger.debug("Reprocessing unlocked")
}
logger.debug("End deltaLoadRepublishPartner")