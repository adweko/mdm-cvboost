import groovy.json.JsonOutput

import java.sql.Timestamp

logger.info("------------------writeDelta START---------------")

// lock reprocessing
try {
    functions.DLMQueueManagement.lockReprocessing()
    logger.info("locked Reprocessing")
} catch(ex) {
    logger.error(ex.toString())
}

Date queryTimestamp = task.getVariable("queryTimestamp")

com.compoverso.dmp.extension.core.connector.service.jdbc.builder.SelectBuilderImpl select = jdbc.select() // select for internal database
        .sql("SELECT * FROM MDM_TRANSACTION_STATUS WHERE WORKORDER_START_TIME_ >= :queryTimestamp ORDER BY WORKORDER_START_TIME_ ASC")
        .parameter("queryTimestamp", queryTimestamp)

List cleanedInboundFiles = []
List cleanedStateFiles = []
List cleanedChangeFiles = []

select.query { row ->
    datapathInbound = row.get("TRANSACTION_DATAPATH_")
    datapathOutboundChange = row.get("CHANGE_OUTBOUND_DATAPATH_")
    datapathOutboundState = row.get("STATE_OUTBOUND_DATAPATH_")

    if (datapathOutboundChange) {
        cleanedChangeFiles.add(datapathOutboundChange)
    }

    if (datapathOutboundState) {
        cleanedStateFiles.add(datapathOutboundState)
    }

    if (datapathInbound) {
        cleanedInboundFiles.add(resources.get(datapathInbound).load("json"))
    }
}
logger.info("Found " + cleanedChangeFiles.size().toString() + " Change files and " + cleanedStateFiles.size().toString() + " State files.")

def extractKafkaKey(String filepath, String topicType) {
    def fileJSON = resources.get(filepath).load("json")

    Map isBusinessPartner

    if ( topicType == "state" ) {
        isBusinessPartner = fileJSON["BusinessPartnerID"][0]["Versions"][0]["isBusinessPartner"][0]
    } else {
        isBusinessPartner = fileJSON["BusinessPartnerID"][0]["isBusinessPartner"][0]
    }

    isBusinessPartner.remove("BusinessPartner")
    isBusinessPartner.remove("TECHInternalPID")

    task.setVariable("TECHMDMPartnerID", fileJSON["BusinessPartnerID"][0]["TECHMDMPartnerID"].toString())
    isBusinessPartner.put( "TECHMDMPartnerID", fileJSON["BusinessPartnerID"][0]["TECHMDMPartnerID"].toString() )

    def isBusinessPartnerPretty = JsonOutput.prettyPrint(JsonOutput.toJson(isBusinessPartner).toString())

    return isBusinessPartnerPretty
}

for (file in cleanedStateFiles) {
    String kafkaKey = extractKafkaKey(file.toString(), "state")

    // queue transaction
    def businessPartnerID = task.getVariable("TECHMDMPartnerID")
    def processID = file.toString()[-41..-6]
    def DLMTestMode = task.getVariable("DLMTestMode")
    functions.DLMQueueManagement.queue(businessPartnerID, processID, DLMTestMode)

    // check transaction
    boolean DLMCheck = functions.DLMQueueManagement.check(businessPartnerID, processID, "zepasCalls", DLMTestMode)
    while ( DLMCheck = false ) {
        wait(3000)
        DLMCheck = functions.DLMQueueManagement.check(businessPartnerID, processID, "zepasCalls", DLMTestMode)
    }

    // write message
    messaging.message("ch-mdm.state.all")
            .source(file.toString())
            .property("key", kafkaKey)
            .send()

    // update commited state status
    try {
    	functions.MDMTransactionStatus.updateTransactionStatus(processID, workOrder.owner, 5, true)
    } catch (ex) {
    	logger.error(ex.toString())
    } finally {
        // dequeue transaction
        boolean DLMDequeue = functions.DLMQueueManagement.dequeue(businessPartnerID, processID, "zepasCalls", DLMTestMode)
        if (!DLMDequeue) {
            throw new Exception("DLMQueueManagement: dequeue Error")
        }
    }
}

logger.info("------------------files written to State Kafka---------------")

for (file in cleanedChangeFiles) {
    String kafkaKey = extractKafkaKey(file.toString(), "change")

    // queue transaction
    def businessPartnerID = task.getVariable("TECHMDMPartnerID")
    def processID = file.toString()[-41..-6]
    def DLMTestMode = task.getVariable("DLMTestMode")
    functions.DLMQueueManagement.queue(businessPartnerID, processID, DLMTestMode)

	// check transaction
    boolean DLMCheck = functions.DLMQueueManagement.check(businessPartnerID, processID, "zepasCalls", DLMTestMode)
    while ( DLMCheck = false ) {
        wait(3000)
        DLMCheck = functions.DLMQueueManagement.check(businessPartnerID, processID, "zepasCalls", DLMTestMode)
    }

	//write kafka
    messaging.message("ch-mdm.change.all")
            .source(file.toString())
            .property("key", kafkaKey)
            .send()

    // update commited change status
    try {
	    functions.MDMTransactionStatus.updateTransactionStatus(processID, workOrder.owner, 4, true)
    } catch (ex) {
    	logger.error(ex.toString())
    } finally {
    	// dequeue transaction
        boolean DLMDequeue = functions.DLMQueueManagement.dequeue(businessPartnerID, processID, "zepasCalls", DLMTestMode)
        if (!DLMDequeue) {
            throw new Exception("DLMQueueManagement: dequeue Error")
        }
    }
}
logger.info("------------------files written to Change Kafka---------------")

// unlock reprocessing
try {
    functions.DLMQueueManagement.unlockReprocessing()
    logger.info("unlocked Reprocessing")
} catch (ex) {
    logger.error(ex.toString())
}
