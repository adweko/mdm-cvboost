import java.time.Instant
import java.time.temporal.ChronoUnit

logger.debug("Start initializeWorkflow")

// Set initial timestamp
Instant workorderStartTime = Instant.now().minus(7, ChronoUnit.DAYS)
task.setVariable("workorderStartTime", workorderStartTime)
task.setVariable("TECHMDMPartnerID", "%")
task.setVariable("comment", "")

task.setVariable("error", false)

// Read the file "avro_schema.versions" located in the home of cvboost
// and set task variables to point to the versions read from there
def homeDir = application.getProperty("app.home")
def avroSchemaFilePath = resources.get("system:" + homeDir + "/avro/schema.properties")
Properties properties = new Properties()
avroSchemaFilePath.load(
        {
            inputStream -> properties.load(inputStream)
        }
)

def stateSchemaVersion = properties.get("ch-mdm.state.all")
if (stateSchemaVersion != null) {
    task.setVariable("avro.schema.state.version", stateSchemaVersion)
} else {
    throw new RuntimeException("The property ch-mdm.state.all is not set in " + avroSchemaFilePath)
}

def changeSchemaVersion = properties.get("ch-mdm.change.all")
if (changeSchemaVersion != null) {
    task.setVariable("avro.schema.change.version", changeSchemaVersion)
} else {
    throw new RuntimeException("The property ch-mdm.state.all is not set in " + avroSchemaFilePath)
}

logger.debug("End initializeWorkflow")