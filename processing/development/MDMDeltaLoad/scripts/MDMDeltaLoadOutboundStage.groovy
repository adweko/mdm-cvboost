import groovy.json.JsonOutput
import java.sql.Timestamp
import java.time.Instant

def extractKafkaKey(def fileJSON, String topicType) {

    Map isBusinessPartner

    if (topicType == "state") {
        isBusinessPartner = fileJSON["BusinessPartnerID"][0]["Versions"][0]["isBusinessPartner"][0]
    } else {
        isBusinessPartner = fileJSON["BusinessPartnerID"][0]["isBusinessPartner"][0]
    }

    isBusinessPartner.remove("BusinessPartner")
    isBusinessPartner.remove("TECHInternalPID")

    isBusinessPartner.put("TECHMDMPartnerID", fileJSON["BusinessPartnerID"][0]["TECHMDMPartnerID"].toString())

    def isBusinessPartnerPretty = JsonOutput.toJson(isBusinessPartner).toString()

    return isBusinessPartnerPretty
}

logger.debug("Start deltaLoadOutboundStage")

Instant workorderStartTime = task.getVariable("workorderStartTime")
String mdmPartnerID = task.getVariable("TECHMDMPartnerID")

Timestamp queryTimestamp = Timestamp.from(workorderStartTime)

Integer deltaLoadNumberOfTransactions = 0

if(mdmPartnerID == "%") {
    com.compoverso.dmp.extension.core.connector.service.jdbc.builder.SelectBuilderImpl select = jdbc.select() // select for internal database
            .sql("SELECT * FROM MDM_TRANSACTION_STATUS WHERE WORKORDER_START_TIME_ >= :queryTimestamp ORDER BY WORKORDER_START_TIME_ ASC")
            .parameter("queryTimestamp", queryTimestamp)
}
else{
    com.compoverso.dmp.extension.core.connector.service.jdbc.builder.SelectBuilderImpl select = jdbc.select() // select for internal database
            .sql("SELECT * FROM MDM_TRANSACTION_STATUS WHERE WORKORDER_START_TIME_ >= :queryTimestamp AND MDM_PARTNERID_ LIKE :mdmPartnerID ORDER BY WORKORDER_START_TIME_ ASC")
            .parameter("queryTimestamp", queryTimestamp)
            .parameter("mdmPartnerID", mdmPartnerID)
}

List cleanedInboundFiles = []
List cleanedStateFiles = []
List cleanedChangeFiles = []

// log Delta Load Start in table MDM_DELTA_LOADS
functions.MDMDeltaLoad.logStartDeltaLoad(workorderID, workorderCreator, workorderOwner, 3, queryTimestamp, mdmPartnerID, task.getVariable("comment"))

select.query { row ->
    datapathInbound = row.get("TRANSACTION_DATAPATH_")
    datapathOutboundChange = row.get("CHANGE_OUTBOUND_DATAPATH_")
    datapathOutboundState = row.get("STATE_OUTBOUND_DATAPATH_")

    if (datapathOutboundChange) {
        cleanedChangeFiles.add(datapathOutboundChange)
    }

    if (datapathOutboundState) {
        cleanedStateFiles.add(datapathOutboundState)
    }

    if (datapathInbound) {
        cleanedInboundFiles.add(resources.get(datapathInbound).load("json"))
    }
    deltaLoadNumberOfTransactions = deltaLoadNumberOfTransactions++
}
logger.debug("Found " + cleanedChangeFiles.size().toString() + " Change files and " + cleanedStateFiles.size().toString() + " State files.")


for (file in cleanedStateFiles) {
    def fileJSON = resources.get(file.toString()).load("json")
    String TECHMDMPartnerID = fileJSON["BusinessPartnerID"][0]["TECHMDMPartnerID"].toString()

    String kafkaKey = extractKafkaKey(fileJSON, "state")

    // queue transaction
    def businessPartnerID = TECHMDMPartnerID
    def processID = file.toString()[-41..-6]
    def DLMTestMode = true
    functions.DLMQueueManagement.queue(businessPartnerID, processID, DLMTestMode)

    // check transaction
    boolean DLMCheck = functions.DLMQueueManagement.check(businessPartnerID, processID, "zepasCalls", DLMTestMode)
    while (DLMCheck = false) {
        wait(3000)
        DLMCheck = functions.DLMQueueManagement.check(businessPartnerID, processID, "zepasCalls", DLMTestMode)
    }

    // write message
    messaging.message(application.getProperty("mdm.outboundStateKafkaTopic"))
            .nodeId("AGENT_MESSAGING_KAFKA")
            .source(file.toString())
            .property("key", kafkaKey)
            .property("converter", "Json2Avro")
            .property("converter.subject", "ch-mdm.state.all")
            .property("converter.version", task.getVariable("avro.schema.state.version"))
            .send()

    // update committed state status
    try {
        MDMTransactionStatus.updateTransactionStatus(processID, workOrder.owner, 5, true)
    } catch (ex) {
        logger.error(ex.toString())
    } finally {
        // dequeue transaction
        boolean DLMDequeue = functions.DLMQueueManagement.dequeue(businessPartnerID, processID, "zepasCalls", DLMTestMode)
        if (!DLMDequeue) {
            throw new Exception("DLMQueueManagement: dequeue Error")
        }
    }
}

logger.debug("------------------files written to State Kafka---------------")

for (file in cleanedChangeFiles) {
    def fileJSON = resources.get(file.toString()).load("json")
    String TECHMDMPartnerID = fileJSON["BusinessPartnerID"][0]["TECHMDMPartnerID"].toString()
    String kafkaKey = extractKafkaKey(fileJSON, "change")

    // queue transaction
    def businessPartnerID = TECHMDMPartnerID
    def processID = file.toString()[-41..-6]
    def DLMTestMode = true
    functions.DLMQueueManagement.queue(businessPartnerID, processID, DLMTestMode)

    // check transaction
    boolean DLMCheck = functions.DLMQueueManagement.check(businessPartnerID, processID, "zepasCalls", DLMTestMode)
    while (DLMCheck = false) {
        wait(3000)
        DLMCheck = functions.DLMQueueManagement.check(businessPartnerID, processID, "zepasCalls", DLMTestMode)
    }

    //write kafka
    messaging.message(application.getProperty("mdm.outboundChangeKafkaTopic"))
            .nodeId("AGENT_MESSAGING_KAFKA")
            .source(file.toString())
            .property("key", kafkaKey)
            .property("converter", "Json2Avro")
            .property("converter.subject", "ch-mdm.change.all")
            .property("converter.version", task.getVariable("avro.schema.change.version"))
            .send()

    // update commited change status
    try {
        MDMTransactionStatus.updateTransactionStatus(processID, workOrder.owner, 4, true)
    } catch (ex) {
        logger.error(ex.toString())
    } finally {
        // dequeue transaction
        boolean DLMDequeue = functions.DLMQueueManagement.dequeue(businessPartnerID, processID, "zepasCalls", DLMTestMode)
        if (!DLMDequeue) {
            throw new Exception("DLMQueueManagement: dequeue Error")
        }
    }
}
logger.debug("------------------files written to Change Kafka---------------")

// log Delta Load Start in table MDM_DELTA_LOADS
functions.MDMDeltaLoad.logEndDeltaLoad(workorderID, deltaLoadNumberOfTransactions, workorderOwner)

logger.debug("End deltaLoadOutboundStage")
