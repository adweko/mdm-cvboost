import groovy.json.JsonOutput
import java.sql.Timestamp
import java.time.Instant

logger.info("Start reprocessDelta")
try {
    task.setVariable("error", false)
    task.setVariable("error_message", "")

    Instant workorderStartTime = task.getVariable("workorderStartTime")
    Timestamp queryTimestamp = Timestamp.from(workorderStartTime)

    // lock reprocessing
    boolean lockStatus = functions.DLMQueueManagement.lockReprocessing()

    if (!lockStatus) {
        throw new Exception("MDMDeltaLoad: Reprocessing already locked")
    }
    logger.info("Reprocessing locked")

    def reprocessingMode = task.getVariable("reprocessingMode")

    task.setVariable("queryTimestamp", queryTimestamp)

    switch(reprocessingMode){
        case "deltaLoadInboundStage":
            MDMDeltaLoadInboundStage.deltaLoadInboundStage(queryTimestamp, [resources: resources], task.getVariable("system.workorder.id"), workorder.owner, workorder.creator, logger, functions, jdbc, spectrum)
            break
        case "deltaLoadMDMDB":
            functions.MDMDeltaLoadMDMDB.deltaLoadMDMDB(queryTimestamp)
            break
        case "deltaLoadOutboundStage":
            functions.MDMDeltaLoadOutboundStage.deltaLoadOutboundStage(queryTimestamp, [resources: resources])
            break
    }

} catch (ex) {
    logger.error(ex.toString())
    task.setVariable("error", true)
    task.setVariable("error_message", ex.toString())
}
finally {
    // unlock reprocessing
    functions.DLMQueueManagement.unlockReprocessing()
    logger.info("Reprocessing unlocked")
}
logger.info("End reprocessDelta")