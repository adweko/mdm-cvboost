import groovy.transform.Field
// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "INFO"
task.setVariable("consoleLogLevel", consoleLogLevel)
// console logging

def consoleLog(String level, String message) {
    Boolean doPrint
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO", "Start initializeWorkflow")

// Use Kafka true/false
task.setVariable("useKafka", true)
consoleLog("INFO",  "useKafka: " + task.getVariable("useKafka"))

// Use Spectrum true/false
task.setVariable("useSpectrum", true)
consoleLog("INFO",  "useSpectrum: " + task.getVariable("useSpectrum"))

// use testfile
task.setVariable("useTestfile", false)
consoleLog("INFO",  "useTestfile: " + task.getVariable("useTestfile"))

consoleLog("INFO", "End initializeWorkflow")