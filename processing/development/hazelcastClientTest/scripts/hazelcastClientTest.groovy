import groovy.json.JsonOutput
import groovy.transform.Field
import com.hazelcast.client.config.ClientConfig
import com.hazelcast.client.config.ClientNetworkConfig
import com.hazelcast.client.HazelcastClient
import com.hazelcast.core.HazelcastInstance
import com.hazelcast.core.IMap


// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    Boolean doPrint
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO", "Start hazelcastClientTest")

consoleLog("INFO", "Create Hazelcast client")
ClientConfig clientConfig = new ClientConfig()
ClientNetworkConfig clientNetConfig = new ClientNetworkConfig()
clientNetConfig.setAddresses(["dc00-smdm001d.helvetia.ch:5701","dc00-smdm002d.helvetia.ch:5701"])
clientConfig.setNetworkConfig(clientNetConfig)
clientConfig.getGroupConfig().setName("SpectrumCluster").setPassword("p1tn3yb0w3s")
HazelcastInstance client = HazelcastClient.newHazelcastClient(clientConfig)




consoleLog("INFO", "Get customers map")
IMap map = client.getMap("customers")

consoleLog("INFO", "Map Size:" + map.size())
task.setVariable("customers", map.toMapString())

consoleLog("INFO", "End hazelcastClientTest")
