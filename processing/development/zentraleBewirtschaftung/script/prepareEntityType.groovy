import groovy.json.JsonOutput
import groovy.transform.Field

//
// MDM prepare entityType data for form
//

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO", "Start prepareEntityType")

try {

    // get selection list
    def entityListOutput = task.getVariable("partnerViewsOutput")
    // set entityListOutput for prepareEntityDetailFormData script
    task.setVariable("entityListOutput", entityListOutput)

    // get form action
    def formAction = task.getVariable("formAction")

    String entityListKey
    String entity

    switch (formAction) {
        case "role":
            entityListKey = "RoleID"
            entity = "role"
            break
        case "channel":
            entityListKey = "ChannelID"
            entity = "channel"
            break
        case "shippingInstructions":
            entityListKey = "ShippingInstructionID"
            entity = "shippingInstructions"
            break
        case "relation":
            entityListKey = "RelationID"
            entity = "relation"
            break
    }
    task.setVariable("entityListKey", entityListKey)
    task.setVariable("entity", entity)


    // get selected object
    Map entityDetailInput = [:]
    Map selectedEntity = [:]

    ArrayList entityList
    try {
        entityList = entityListOutput.get(entityListKey)
        entityList.each { singleEntity ->
            if (singleEntity.selector == true) {
                selectedEntity = singleEntity
            }
        }
    }
    catch (Exception ex) {

    }

    if (selectedEntity.isEmpty()) {
        // no location selected, return
        task.setVariable("formAction", "noSelection")
    } else {
        // Preload entityDetailInput with values from selected line from list
        task.setVariable("entityDetailInput", selectedEntity)
        def dataAsJson = JsonOutput.toJson(selectedEntity)
        consoleLog("INFO", "entityDetailInput: " + JsonOutput.prettyPrint(dataAsJson.toString()))

        // clear entityDetailOutput
        Map entityDetailOutput = [:]
        task.setVariable("entityDetailOutput", entityDetailOutput)

        // prepare entity detail form template variables
        task.setVariable("formTemplate", "${entity}Detail")
        task.setVariable("entityDetailDefinition", resources.get("deployment:/json/form/${entity}Detail.json").load("json"))

        task.setVariable("formAction", "detail")
    }
}
catch (Exception ex) {
    consoleLog("ERROR", ex.toString())
}

consoleLog("INFO", "End prepareEntityType")
