import groovy.json.JsonOutput
import groovy.transform.Field
import org.codehaus.groovy.runtime.NullObject

//
// MDM prepare locationDetail data for form
//

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO", "Start prepareLocationDetailFormData")

try {
    // get full partner data
    def fullPartnerInput = task.getVariable("fullPartnerInput")
    // get selection
    def locationListOutput = task.getVariable("locationListOutput")
    // get selected location
    Map locationDetailInput = [:]
    Map selectedLocation = [:]

    ArrayList locationList
    try {
        locationList = locationListOutput.get("hasLocationID")
        locationList.each { location ->
            if (location.selector == true) {
                selectedLocation = location
            }
        }
    }
    catch(Exception ex){

    }

    if ((selectedLocation.isEmpty()) && (locationListOutput.get("formAction") == "detail")) {
        // no location selected, return
        task.setVariable("formAction", "noSelection")
    } else {
        // Select location from fullPartner data
        if (fullPartnerInput != null) {
            fullPartnerInput.get("BusinessPartnerID").first().each { entity ->
                if (entity.key == "hasLocationID") {
                    locationDetailInput.put(entity.key, entity.value)
                    task.setVariable("locationDetailInput", locationDetailInput)
                    def dataAsJson = JsonOutput.toJson(locationDetailInput)
                    consoleLog("INFO", "locationDetailInput: " + JsonOutput.prettyPrint(dataAsJson.toString()))
                }
            }
        }

        // Preload with values from list if a new location was added
        task.setVariable("locationDetailOutput", locationListOutput.get("hasLocationID").first())
        def dataAsJson = JsonOutput.toJson(locationListOutput.get("hasLocationID").first())
        consoleLog("INFO", "locationDetailOutput: " + JsonOutput.prettyPrint(dataAsJson.toString()))

        task.setVariable("formTemplate", "locationDetail")
        task.setVariable("locationDetailDefinition", resources.get("deployment:/json/form/locationDetail.json").load("json"))
    }
}
catch (Exception ex) {
    consoleLog("ERROR", ex.toString())
}

consoleLog("INFO", "End prepareLocationDetailFormData")
