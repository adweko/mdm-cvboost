import groovy.json.JsonOutput
import groovy.transform.Field

//
// MDM prepare entityDetail data for form
//

String entityListKey = task.getVariable("entityListKey")
String entity = task.getVariable("entity")

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO", "Start prepareEntityDetailFormData. entity: " + entity + " entityListKey: " + entityListKey)

try {

    // get full partner data
    def fullPartnerInput = task.getVariable("fullPartnerInput")

    // get selection list
    def entityListOutput = task.getVariable("entityListOutput")

    // remove entity from fullPartnerInput as it will be delivered via entityListOutput
    if (fullPartnerInput != null) {
        fullPartnerInput.get("BusinessPartnerID").first().remove( entityListKey )
    }
    task.setVariable("fullPartnerInput", fullPartnerInput)

    def formAction

    try {
        formAction = entityListOutput.get("formAction")
    }
    catch(Exception ex){

    }

    if(formAction == "back"){
        // push entityListOutput to ${entity}ListOutput so it can be processed further
        task.setVariable("${entity}ListOutput", entityListOutput)
        def dataAsJson = JsonOutput.toJson(entityListOutput)
        consoleLog("INFO", "${entity}ListOutput: " + JsonOutput.prettyPrint(dataAsJson.toString()))

    }
    else if(formAction == "detail") {

        // get selected object
        Map entityDetailInput = [:]
        Map selectedEntity = [:]

        ArrayList entityList
        try {
            entityList = entityListOutput.get(entityListKey)
            entityList.each { singleEntity ->
                if (singleEntity.selector == true) {
                    selectedEntity = singleEntity
                }
            }
        }
        catch (Exception ex) {

        }

        if (selectedEntity.isEmpty()) {
            // no location selected, return
            task.setVariable("formAction", "noSelection")
        } else {
            // Preload entityDetailInput with values from selected line from list
            task.setVariable("entityDetailInput", selectedEntity)
            def dataAsJson = JsonOutput.toJson(entityDetailInput)
            consoleLog("INFO", "entityDetailInput: " + JsonOutput.prettyPrint(dataAsJson.toString()))

            // clear entityDetailOutput
            Map entityDetailOutput = [:]
            task.setVariable("entityDetailOutput", entityDetailOutput)

            // prepare entity detail form template variables
            task.setVariable("formTemplate", "${entity}Detail")
            task.setVariable("entityDetailDefinition", resources.get("deployment:/json/form/${entity}Detail.json").load("json"))
        }
    }
}
catch (Exception ex) {
    consoleLog("ERROR", ex.toString())
}

consoleLog("INFO", "End prepareEntityDetailFormData. entity: " + entity + " entityListKey: " + entityListKey)
