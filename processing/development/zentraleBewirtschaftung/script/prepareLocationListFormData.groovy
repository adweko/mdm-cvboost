import groovy.json.JsonOutput
import groovy.transform.Field
import org.codehaus.groovy.runtime.NullObject

//
// MDM prepare locationList data for form
//

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO", "Start prepareLocationListFormData")

// get full partner data
try {

    // Prepare form template
    task.setVariable("formTemplate", "locationList")
    task.setVariable("locationListDefinition", resources.get("deployment:/json/form/locationList.json").load("json"))
    task.setVariable("locationListButton", resources.get("deployment:/json/action/locationList.json").load("json"))


    def fullPartnerInput = task.getVariable("fullPartnerInput")
// set variable for form input
    def locationListInput = [:]
    try {
        locationListInput.put("hasLocationID", fullPartnerInput.get("BusinessPartnerID").first().get("hasLocationID"))
    }
    catch(Exception ex){
        // no location ID exist
    }
    task.setVariable("locationListInput", locationListInput)

    def locationListOuput = [:]
    locationListOutput = task.getVariable("locationListOuput")

    if (locationListOutput in Map) {
        locationListInput.put("hasLocationID", locationListOuput)
    }

    def dataAsJson = JsonOutput.toJson(locationListInput)
    consoleLog("INFO", "locationListInput: " + JsonOutput.prettyPrint(dataAsJson.toString()))

}
catch (Exception ex) {
    consoleLog("ERROR", +ex.toString())
}

consoleLog("INFO", "End prepareLocationListFormData")
