import groovy.transform.Field

//
// MDM clearVariables
//

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO",  "Start clearVariables")

task.setVariable("readonly", false)
consoleLog("INFO",  "readonly: false")

task.setVariable("businessPartnerType", null)
consoleLog("INFO",  "businessPartnerType: null")

task.removeVariable("fullPartnerOutput")
consoleLog("INFO",  "removeVariable fullPartnerOutput")

task.removeVariable("fullPartnerInput")
consoleLog("INFO",  "removeVariable fullPartnerInput")

task.removeVariable("identificationNumberInput")
consoleLog("INFO",  "removeVariable identificationNumberInput")

task.removeVariable("identificationNumberOutput")
consoleLog("INFO",  "removeVariable identificationNumberOutput")

task.removeVariable("displayServiceResultInput")
consoleLog("INFO",  "removeVariable displayServiceResultInput")

task.removeVariable("partnerViewsInput")
consoleLog("INFO",  "removeVariable partnerViewsInput")

task.removeVariable("partnerViewsOutput")
consoleLog("INFO",  "removeVariable partnerViewsOutput")

task.removeVariable("formAction")
consoleLog("INFO",  "removeVariable formAction")

task.removeVariable("entityDetailDefinition")
consoleLog("INFO",  "removeVariable entityDetailDefinition")

task.removeVariable("entityDetailInput")
consoleLog("INFO",  "removeVariable entityDetailInput")

task.removeVariable("entityDetailOutput")
consoleLog("INFO",  "removeVariable entityDetailOutput")

task.removeVariable("entityListInput")
consoleLog("INFO",  "removeVariable entityListInput")

task.removeVariable("entityListOutput")
consoleLog("INFO",  "removeVariable entityListOutput")

task.removeVariable("locationListInput")
consoleLog("INFO",  "removeVariable locationListInput")

task.removeVariable("locationListOutput")
consoleLog("INFO",  "removeVariable locationListOutput")

task.removeVariable("locationInput")
consoleLog("INFO",  "removeVariable locationInput")

task.removeVariable("locationOutput")
consoleLog("INFO",  "removeVariable locationOutput")

consoleLog("INFO",  "End clearVariables")