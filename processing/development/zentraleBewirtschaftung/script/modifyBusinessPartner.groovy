/**
 * Modify business partner in Spectrum MDM
 *
 * Using Spectrum service modifyBusinessPartner
 */
import groovy.json.JsonBuilder
import groovy.json.JsonOutput
import groovy.json.JsonSlurperClassic
import groovy.transform.Field
import org.apache.logging.log4j.core.tools.picocli.CommandLine.IExceptionHandler
import org.apache.tools.ant.types.selectors.SelectSelector

import java.sql.Timestamp
import com.compoverso.dmp.extension.core.workflow.usertask.form.Form
import com.compoverso.dmp.extension.core.workflow.context.UserTaskContextImpl
import com.compoverso.dmp.extension.core.resource.context.MultiFileResourceContext
import com.compoverso.dmp.extension.spectrum.services.SpectrumRequestBuilderImpl

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

def writeSpectrum = task.getVariable("writeSpectrum")
if (writeSpectrum == null) {
    writeSpectrum = true
}

consoleLog("INFO", "Start modifyBusinessPartner")


/*
* Variables
 */
def jsonSlurper = new JsonSlurperClassic()
def jsonBuilder
def serviceJson
Map serviceMap
Map serviceMapSingleObject
// Map with user input from fullPartner form
Map fullPartnerOutput

Exception exception

String serviceResultMessage


/*
* Script global variables
 */
@Field def BITEMPValidFrom
@Field def BITEMPValidTo
@Field def BITEMPSourceTimestamp
@Field def BITEMPMDMTimestamp
@Field def BITEMPMDMTimestamp2
@Field def TECHSourceSystem
@Field def TECHExternalPID
@Field ArrayList businessPartnerServices
@Field String partnerMode
@Field Locale currentLocale
@Field String lang
@Field def fieldDefinitions
def serviceResult


Boolean state // State of script: true = Everything OK, false = shit happened

state = true

/*
* Methods
*/

def initialization(Map initialDataMap) {
/* Define master keys for business partner
* BITEMPValidFrom
* BITEMPValidTo
* BITEMPSourceTimestamp
* BITEMPMDMTimestamp
* TECHSourceSystem
* TECHExternalPID
*/
    BITEMPValidFrom = initialDataMap.BITEMPValidFrom
    if ((initialDataMap.BITEMPValidFrom == null) || (initialDataMap.BITEMPValidFrom == "")) {
        BITEMPValidFrom = new Date().parse("dd.MM.yyyy", "31.12.9999").format("dd.MM.yyyy")
    } else {
        try {
            BITEMPValidFrom = initialDataMap.BITEMPValidFrom.format("dd.MM.yyyy")
        }
        catch (Exception ex) {
        }
    }

    BITEMPValidTo = initialDataMap.BITEMPValidTo
    if ((initialDataMap.BITEMPValidTo == null) || (initialDataMap.BITEMPValidTo == "")) {
        BITEMPValidTo = new Date().parse("dd.MM.yyyy", "31.12.9999").format("dd.MM.yyyy")
    } else {
        try {
            BITEMPValidTo = initialDataMap.BITEMPValidTo.format("dd.MM.yyyy")
        }
        catch (Exception ex) {
        }
    }
    Date date = new Date();
    BITEMPSourceTimestamp = new Timestamp(date.getTime()).format("yyyyMMddHHmmssSSS", TimeZone.getTimeZone('UTC')).toString()
    BITEMPMDMTimestamp = BITEMPSourceTimestamp
    TECHSourceSystem = "MDM"
    TECHExternalPID = task.getVariable("system.workorder.id")
    TECHMDMPartnerID = task.getVariable("TECHMDMPartnerID")

// fill business partner services list in call order
    switch (partnerMode) {
        case "showPartner":
            businessPartnerServices = []
            break
        case "createPartner":
            businessPartnerServices = [[service: "createBusinessPartner", serviceMap: "fullPartnerOutput", serviceMapType: "string", serviceResultRelationType: null],
                                       [service: "createLocation", serviceMap: "locationListOutput", serviceMapType: "list", serviceResultRelationType: "hasLocationID"]
            ]
            break
        case "updatePartner":
            businessPartnerServices = [[service: "updateBusinessPartner", serviceMap: "fullPartnerOutput", serviceMapType: "string", serviceResultRelationType: null],
                                       [service: "modifyLocation", serviceMap: "locationListOutput", serviceMapType: "list", serviceResultRelationType: "hasLocationID"]
            ]
            break
    }
}

/*
* Fill service map from object map
 */

def fillServiceMap(Map serviceMapSingleObject, Map objectMap, String objectKey) {

    String type = ""
    def serviceMapObjects = []
    Map objectMapSingleObject = [:]
    objectMapSingleObject.putAll(serviceMapSingleObject)

    objectMap.each { key, value ->

        if (key == "formAction") {
            return // skip formAction field with button pressed information
        }


        def result
        try {
            type = fieldDefinitions.get(key).type
        }
        catch (Exception ex) {
            // defaulting to string type
        }

        switch (type) {
            case "date":
                if (serviceMapSingleObject.containsKey(key)) {
                    if (value != null) {
                        objectMapSingleObject.put(key, value.format("dd.MM.yyyy"))
                    }
                }
                break

            case "table":
                ArrayList subList = []
                String subKey = "data" + key
                def subObject
                try {
                    subObject = serviceMapSingleObject.get(key).first()

                }
                catch (Exception ex) {

                }
                if (subObject != null) {
                    value.each { it ->

                        def subServiceMap = fillServiceMap(subObject, it, key)
                        subList.add(subServiceMap.first())
                    }
                    objectMapSingleObject.put(key, subList)
                }
                break

            default:
                if (serviceMapSingleObject != null) {
                    if (serviceMapSingleObject.containsKey(key)) {
                        // delete Flag
                        if (key == "deleteFlag") {
                            if (value == true) {
                                objectMapSingleObject.put(key, "X")
                            }
                        } else {
                            objectMapSingleObject.put(key, value)
                        }
                    }
                }

        }

    }


    if (objectKey == null) {
        try {
            // We are on Top Level => add Org Unit data for service from partnerViewsOutput
            Map partnerViewsOutput = task.getVariable("partnerViewsOutput")
            objectMapSingleObject.put("OrgUnit", partnerViewsOutput.get("OrgUnit"))
        }
        catch (Exception ex) {

        }
    }

    // Create additional technical fields depending on (sub)entity type
    boolean partnerIDField = false
    boolean techFields = false
    boolean tempFields = false
    boolean partnerTypeField = false

    switch (objectKey) {
        case (null):
            partnerIDField = true
            techFields = true
            tempFields = true
            partnerTypeField = true
            break
        case ("IdentificationNumber"):
            partnerIDField = false
            techFields = false
            tempFields = false
            partnerTypeField = false
            break
        default:
            partnerIDField = true
            techFields = false
            tempFields = false
            partnerTypeField = false
    }

    if (partnerIDField) {
        // Set TECHMDMPartnerID if available
        try {
            objectMapSingleObject.TECHMDMPartnerID = TECHMDMPartnerID
        }
        catch (Exception ex) {
        }
    }

    if (techFields) {
        // General Tech fields
        // Set TECHExternalPID
        objectMapSingleObject.TECHExternalPID = TECHExternalPID
        // Set TECHSourceSystem
        objectMapSingleObject.TECHSourceSystem = TECHSourceSystem
    }

    if (tempFields) {
        // Bitemporal fields
        // Set BITEMPSourceTimestamp
        objectMapSingleObject.BITEMPSourceTimestamp = BITEMPSourceTimestamp
        // Set BITEMPMDMTimestamp
        objectMapSingleObject.BITEMPMDMTimestamp = BITEMPMDMTimestamp
        // Set BITEMPValidTo
        objectMapSingleObject.BITEMPValidTo = BITEMPValidTo
        // Set BITEMPValidFrom
        objectMapSingleObject.BITEMPValidFrom = BITEMPValidFrom
    }

    if (partnerTypeField) {
        // Set businessPartnerType
        if (objectMapSingleObject.containsKey("businessPartnerType")) {
            try {
                objectMapSingleObject.businessPartnerType = task.getVariable("businessPartnerType")
            }
            catch (Exception ex) {
            }
        }
    }

    // End: Create additional technical fields depending on (sub)entity type

    //
    if ("N" == task.getVariable("businessPartnerType")) {
        // no OrgUnit for Natural person
        objectMapSingleObject.remove("OrgUnit")
    }

    // add object
    serviceMapObjects.add(objectMapSingleObject)

    return serviceMapObjects
}


/*
* call spectrum REST service POST method
*/

def callSpectrumService(Map serviceMap, String service) {
    consoleLog("INFO", "Call Spectrum REST service ${service}")
    def mapAsJson = JsonOutput.toJson(serviceMap)
    def serviceResult = [:]

    consoleLog("INFO", "serviceMap as Json: " + JsonOutput.prettyPrint(mapAsJson.toString()))


    if (!writeSpectrum) {
        consoleLog("INFO", "Service ${service} will not be called, writeSpectrum == false")
        return serviceResult
    }

    try {
        spectrum.request(service)
                .object("json", serviceMap)
                .acceptJSON()
                .execute({ info, inStream ->
                    serviceResultText = inStream.getText(info.contentEncoding)
                    contentType = info.getContentType()
                })
        if (contentType == "application/json") {
            // Service returned a json, worked fine
            serviceResult = new groovy.json.JsonSlurperClassic().parseText(serviceResultText)

        } else {
            serviceResult = serviceResultText
        }
    }
    catch (Exception ex) {
        serviceResult = ex
    }

    return serviceResult
}


/*
* Main
*/
consoleLog("INFO", "Start Main")
// initialize variables
// Read field definitions
fieldDefinitions = resources.get("system:${application.getProperty("mdm.metadata")}").load("json")
// get ID of partner in processing
TECHMDMPartnerID = task.getVariable("TECHMDMPartnerID")
// get language of user
currentLocale = localization.getLocale()
lang = currentLocale.getLanguage()
// get partner mode
partnerMode = task.getVariable("partnerMode")
// get modify data for businessPartner
fullPartnerOutput = task.getVariable("fullPartnerOutput")
// fill bitemporal fields and technical fields with data from fullPartnerOutput form
initialization(fullPartnerOutput)


// set status to OK at first
task.setVariable("messageLabel", "OK")
task.setVariable("messageText", fieldDefinitions.get("serviceResult").language.get(lang).shorttxt)
task.setVariable("messageLevel", "Info")

serviceResultsComplete = []

// process partner data from forms
businessPartnerServices.each { it ->

    // get service Name and objectMap name
    String serviceObjectMapName
    String serviceName
    String serviceMapType
    def serviceResultRelationType

    // Map can have multiple entries
    serviceObjectMapName = it.get("serviceMap")
    serviceName = it.get("service")
    serviceMapType = it.get("serviceMapType")
    serviceResultRelationType = it.get("serviceResultRelationType")

    // read service definition
    serviceJson = resources.get("deployment:/json/service/${serviceName}.json")
    serviceMap = serviceJson.load("json")
    serviceMapSingleObject = serviceMap.I.Row[0]

    // service result
    serviceResult = [:]

    try {
        // read object map
        def objectData = task.getVariable(serviceObjectMapName)

        // special treatment for identificationNumber as we use businessPartner service to modify it
        if (serviceObjectMapName == "fullPartnerOutput") {
            try {
                objectData.remove("IdentificationNumber")
            }
            finally {
                // if its not there it does not need to be removed
            }
            // add data from identificationNumberOutput form to fullPartnerOutput as it is processed in the same service
            Map identificationNumberOutput = task.getVariable("identificationNumberOutput")
            if (identificationNumberOutput != null) {
                if (identificationNumberOutput in Map) {
                    objectData.put("IdentificationNumber", identificationNumberOutput.get("IdentificationNumber"))
                }
            }
        }

        if (objectData == null) {
            objectData = [:]
        }

        if (!objectData.isEmpty()) {
            switch (serviceMapType) {
                case "string":
                    // fill serviceMap
                    def serviceMapObjects = fillServiceMap(serviceMapSingleObject, objectData, serviceResultRelationType)
                    serviceMap.I.put("Row", serviceMapObjects)

                    // call Spectrum + Kafka push
                    serviceResult = callSpectrumService(serviceMap, serviceName)
                    // if serviceResult is json try to convert to Map
                    if (serviceResult in LinkedHashMap) {
                        // service call successful
                        //read TECHMDMPartnerID from createBusinessPartner result and set variable for following service calls
                        if (serviceName == "createBusinessPartner") {
                            TECHMDMPartnerID = serviceResult.BusinessPartnerID.first().get("TECHMDMPartnerID")
                        }

                        def dataAsJson = JsonOutput.prettyPrint(JsonOutput.toJson(serviceResult).toString())
                        consoleLog("INFO", "serviceResult: " + dataAsJson)
                        String messageText = "TECHMDMPartnerID: " + serviceMapObjects.TECHMDMPartnerID +
                                " BITEMPValidFrom: " + serviceMapObjects.BITEMPValidFrom +
                                " BITEMPValidTo: " + serviceMapObjects.BITEMPValidTo +
                                " BITEMPSourceTimestamp: " + serviceMapObjects.BITEMPSourceTimestamp +
                                " BITEMPMDMTimestamp: " + serviceMapObjects.BITEMPMDMTimestamp
                        task.setVariable("messageLabel", "Serviceaufruf: " + serviceName + " erfolgreich")
                        task.setVariable("messageText", messageText)
                        task.setVariable("messageLevel", "Info")

                    } else if ((serviceResult in Exception) || (serviceResult in String)) {
                        consoleLog("ERROR", "Exception: serviceResult: " + serviceResult.toString())
                        state = false

                        task.setVariable("messageLabel", "Exception")
                        task.setVariable("messageText", "serviceResult: " + serviceResult.toString())
                        task.setVariable("messageLevel", "Error")

                    }

                    if (serviceResultRelationType == null) {
                        // head of result
                        serviceResultsCompleteClosure = serviceResult
                    } else {

                        serviceResultsCompleteClosure.get("BusinessPartnerID").first().put(serviceResultRelationType, serviceResult.get("BusinessPartnerID").first().get(serviceResultRelationType))
                    }

                    break

                case "list":
                    objectData.get(serviceResultRelationType).each { objectDataLine ->
                        // fill serviceMap

                        def serviceMapObjects = fillServiceMap(serviceMapSingleObject, objectDataLine, serviceResultRelationType)
                        serviceMap.I.put("Row", serviceMapObjects)

                        // call Spectrum + Kafka push
                        serviceResult = callSpectrumService(serviceMap, serviceName)
                        // if serviceResult is json try to convert to Map
                        if (serviceResult in LinkedHashMap) {
                            // service call successful
                            def dataAsJson = JsonOutput.prettyPrint(JsonOutput.toJson(serviceResult).toString())
                            consoleLog("INFO", "serviceResult: " + dataAsJson)
                            String messageText = "TECHMDMPartnerID: " + serviceMapObjects.TECHMDMPartnerID +
                                    " BITEMPValidFrom: " + serviceMapObjects.BITEMPValidFrom +
                                    " BITEMPValidTo: " + serviceMapObjects.BITEMPValidTo +
                                    " BITEMPSourceTimestamp: " + serviceMapObjects.BITEMPSourceTimestamp +
                                    " BITEMPMDMTimestamp: " + serviceMapObjects.BITEMPMDMTimestamp
                            task.setVariable("messageLabel", "Serviceaufruf: " + serviceName + " erfolgreich")
                            task.setVariable("messageText", messageText)
                            task.setVariable("messageLevel", "Info")

                        } else if (serviceResult in Exception) {
                            consoleLog("ERROR", "Exception: serviceResult: " + serviceResult.toString())
                            state = false

                            task.setVariable("messageLabel", "Exception")
                            task.setVariable("messageText", "serviceResult: " + serviceResult.toString())
                            task.setVariable("messageLevel", "Error")

                        }

                        if (serviceResultRelationType == null) {
                            // head of result
                            serviceResultsCompleteClosure = serviceResult
                        } else {
                            if (!serviceResultsCompleteClosure.isEmpty()) {
                                if (serviceResultsCompleteClosure.get("BusinessPartnerID").first().containsKey(serviceResultRelationType)) {
                                    serviceResultsCompleteClosure.get("BusinessPartnerID").first().get(serviceResultRelationType).add(serviceResult.get("BusinessPartnerID").first().get(serviceResultRelationType).first())
                                } else {
                                    serviceResultsCompleteClosure.get("BusinessPartnerID").first().put(serviceResultRelationType, serviceResult.get("BusinessPartnerID").first().get(serviceResultRelationType))
                                }
                            }
                        }

                    }

                    break
            }
        }

    }
    catch (Exception ex) {
        consoleLog("ERROR", "Exception: " + ex.toString())
        consoleLog("ERROR", "serviceResult: " + serviceResult)
        state = false

        task.setVariable("messageLabel", ex.toString())
        task.setVariable("messageText", "serviceResult: " + serviceResult.toString())
        task.setVariable("messageLevel", "Error")
    }

    serviceResultsComplete = serviceResultsCompleteClosure
    def dataAsJson = JsonOutput.prettyPrint(JsonOutput.toJson(serviceResultsComplete).toString())
    consoleLog("INFO", "serviceResultsComplete: " + dataAsJson)

} // businessPartnerServices.each { it ->

// Prepare message for MDM Outbound Kafka Topic
try {
    def useKafka = task.getVariable("useKafka")
    if (useKafka) {
        def kafkaTopic = task.getVariable("kafkaTopic")
        // prepare Kafka Message file
        resources.get("/modifyPartnerKafkaMessage.json").save("json", serviceResultsComplete)

        // extract kafka key
        Map isBusinessPartner = serviceResultsComplete.get("BusinessPartnerID").first().get("isBusinessPartner").first()
        isBusinessPartner.remove ("BusinessPartner")
        isBusinessPartner.remove ("TECHInternalPID")
        //def isBusinessPartnerPretty = JsonOutput.prettyPrint(JsonOutput.toJson(isBusinessPartner).toString())
        task.setVariable("kafka_key", JsonOutput.toJson(isBusinessPartner).toString())
        consoleLog("INFO", "kafka_key: " + JsonOutput.toJson(isBusinessPartner) )


        // save to Kafka
        consoleLog("INFO", "Calling Kafka Producer")
        messaging.message(kafkaTopic.toString())
                .source("/modifyPartnerKafkaMessage.json")
                .property("key", JsonOutput.toJson(isBusinessPartner))
                .send()
        consoleLog("INFO", "Kafka Message written")
    }
}
catch (Exception ex) {
    consoleLog("ERROR", "Exception: " + ex.toString())
    state = false

    task.setVariable("messageLabel", ex.toString())
    task.setVariable("messageText", "")
    task.setVariable("messageLevel", "Error")
}


// return result
Map displayServiceResult = [:]
if (state) {
    displayServiceResult = [serviceResult: fieldDefinitions.dataSaved.language.get(lang).shorttxt]
    //displayServiceResult = [serviceResult: "saveed"]
} else {
    displayServiceResult = [serviceResult: task.getVariable("messageText")]
}

task.setVariable("formInput", displayServiceResult)
task.removeVariable("formOutput")

consoleLog("INFO", "End modifyBusinessPartner")