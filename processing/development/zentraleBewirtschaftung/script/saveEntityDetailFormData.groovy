import groovy.json.JsonOutput
import groovy.transform.Field


//
// MDM save entityDetail data for form
//

String entityListKey = task.getVariable("entityListKey")
String entity = task.getVariable("entity")
boolean readonly = task.getVariable("readonly")

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO", "Start saveEntityDetailFormData. entity: " + entity + " entityListKey: " + entityListKey)

if (!readonly) {
    try {
        // get entityList
        def entityListOutput = task.getVariable("entityListOutput")
        // get entityDetail Output
        def entityDetailOutput = task.getVariable("entityDetailOutput")

        // get selected entity
        Map entityDetailInput = [:]
        Map selectedEntity = [:]

        ArrayList entityList = []
        ArrayList updatedEntityList = []
        ArrayList updatedEntities = []
        Map updatedEntityListOutput = [:]
        try {
            entityList = entityListOutput.get(entityListKey)
            entityList.each { singleEntity ->
                if (singleEntity.selector == true) {
                    // replace with data from entityDetailOutput
                    singleEntity.each{ k, v ->
                        if( entityDetailOutput.containsKey(k) ){
                           singleEntity.put( k, entityDetailOutput.get(k))
                        }
                    }
                    updatedEntities.add(singleEntity)
                } else {
                    // keep data from entityListOutput
                    updatedEntities.add(singleEntity)
                }
            }
            updatedEntityList.add(updatedEntities)
            updatedEntityListOutput.put(entityListKey, updatedEntities)
            task.setVariable("${entity}ListOutput", updatedEntityListOutput)
            def dataAsJson = JsonOutput.toJson(updatedEntityListOutput)
            consoleLog("INFO", "updated ${entity}ListOutput: " + JsonOutput.prettyPrint(dataAsJson.toString()))
        }
        catch (Exception ex) {
            consoleLog("ERROR", ex.toString())
        }
    }
    catch (Exception ex) {
        consoleLog("ERROR", ex.toString())
    }
}

consoleLog("INFO", "End saveEntityDetailFormData. entity: " + entity +  " entityListKey: " + entityListKey)
