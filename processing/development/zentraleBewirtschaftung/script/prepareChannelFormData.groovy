import groovy.transform.Field

//
// MDM prepare channel data for form
//

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO",  "Start prepareChannelFormData")

// get form data from showFullPartner form
fullPartnerOutput = task.getVariable("fullPartnerOutput")

// get full partner data
fullPartnerInput = task.getVariable("fullPartnerInput")

// fill channelInput
// get selection parameters
def channelInput
Map selectedChannel
try {
    ArrayList channelList = fullPartnerOutput.ChannelID
    channelList.each { channel ->
        if (channel.selector == true) {
            selectedChannel = channel
         }
    }
// go to correct channel in showFullPartnerInput
    fullPartnerInput.BusinessPartnerID.first().hasChannelID.each { channel ->
        if (channel.ChannelID.first().channelID == selectedChannel.channelID) {
            channelInput = channel
        }
    }
    task.setVariable("channelInput", channelInput)

}
catch(Exception ex){

}

consoleLog("INFO",  "End prepareChannelFormData")