import groovy.json.JsonOutput
import groovy.json.JsonSlurperClassic
import groovy.transform.Field

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

def useSpectrum = task.getVariable("useSpectrum")
if (useSpectrum == null){
    useSpectrum = true
}

//---------------------------------------
// Read business partner in Spectrum MDM
//---------------------------------------

def service = "readFullPartner"

consoleLog("INFO",   "Start ${service}")

if (!useSpectrum){
    consoleLog("INFO", "Service ${service} will not be be called, useSpectrum == false")
    return
}

def jsonSlurper = new JsonSlurperClassic()
def jsonBuilder



// read service definition
def serviceJson = resources.get("deployment:/json/service/${service}.json")
def serviceMap = serviceJson.load("json")

// service results
LinkedHashMap serviceResults
LinkedHashMap serviceResultsMap

// Fill service map from object map
try {
    serviceMap.I.Row[0].keyDate = task.getVariable("keyDate").format("dd.MM.yyyy")
    def BITEMPMDMTimestamp = Date.parse("yyyy-MM-dd HH:mm:ss.SSS", task.getVariable("BITEMPMDMTimestamp"))
    serviceMap.I.Row[0].BITEMPMDMTimestamp = BITEMPMDMTimestamp.format("yyyyMMddHHmmssSSS").toString()
    serviceMap.I.Row[0].TECHMDMPartnerID = task.getVariable("TECHMDMPartnerID")
}
catch (Exception ex) {
}

// call spectrum readBusinessPartner REST service POST method
consoleLog("INFO",   "Call Spectrum REST service ${service}")

try {

    def mapAsJson = JsonOutput.toJson(serviceMap)
    consoleLog("INFO", "Service json: " + JsonOutput.prettyPrint(mapAsJson.toString()))

    spectrum.request(service)
            .object("json", serviceMap)
            .acceptJSON()
            .execute({info, inStream ->
                serviceResultText = inStream.getText(info.contentEncoding)
                contentType = info.getContentType()
            })
    if (contentType == "application/json") {
        // Service returned a json, worked fine
        serviceResults = new groovy.json.JsonSlurperClassic().parseText(serviceResultText)

    }
    else{
        // error happened
        consoleLog("ERROR", "Error when calling service ${service}: " + serviceResultText)

        task.setVariable("errorLabel", "Error when calling service ${service}")
        task.setVariable("errorMessage", "serviceResults: " + serviceResultString)
        task.setVariable("readPartnerOK", false)

        return
    }

    dataAsJson = JsonOutput.toJson(serviceResults)
    consoleLog("INFO",   "serviceResultsJSON: " + JsonOutput.prettyPrint(dataAsJson.toString()))

    // return webservice response to workflow
    task.setVariable("fullPartnerInput", serviceResults)
    task.setVariable("readPartnerOK", true)

    // set partner type
    ArrayList naturalPerson
    naturalPerson = serviceResults.BusinessPartnerID[0].isNaturalPerson
    if (naturalPerson.isEmpty()) {
        task.setVariable("businessPartnerType", "L")
    } else {
        task.setVariable("businessPartnerType", "N")
    }

}
catch (Exception ex) {
    consoleLog("ERROR",   ex.toString())
    consoleLog("ERROR",    "serviceResults: " + serviceResults)

    task.setVariable("errorLabel", ex.toString())
    task.setVariable("errorMessage", "serviceResults: " + serviceResults.toString())
    task.setVariable("readPartnerOK", false)
}

consoleLog("INFO",   "End ${service}")

