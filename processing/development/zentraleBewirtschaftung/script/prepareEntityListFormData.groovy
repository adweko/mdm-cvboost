import groovy.json.JsonOutput
import groovy.transform.Field

//
// MDM prepare entityList data for form
//

String entityListKey = task.getVariable("entityListKey")
String entity = task.getVariable("entity")

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO", "Start prepareEntityListFormData. entity: " + entity + " entityListKey: " + entityListKey)

// get full partner data
try {

    // Prepare form template
    task.setVariable("formTemplate", "entityList")
    task.setVariable("entityListDefinition", resources.get("deployment:/json/form/${entity}List.json").load("json"))
    task.setVariable("entityListButton", resources.get("deployment:/json/action/entityList.json").load("json"))


    def fullPartnerInput = task.getVariable("fullPartnerInput")
// set variable for form input
    def entityListInput = [:]
    try {
        entityListInput.put(entityListKey, fullPartnerInput.get("BusinessPartnerID").first().get(entityListKey))
    }
    catch(Exception ex){
        // no location ID exist
    }
    task.setVariable("entityListInput", entityListInput)

// set variable for form output
    def entityListOutput = task.getVariable("${entity}ListOutput")
    if(entityListOutput == null){
        // do nothing
    }
    else {
        task.setVariable("entityListOutput", entityListOutput)
    }

    def dataAsJson = JsonOutput.toJson(entityListInput)
    consoleLog("INFO", "entityListInput: " + JsonOutput.prettyPrint(dataAsJson.toString()))
    dataAsJson = JsonOutput.toJson(entityListOutput)
    consoleLog("INFO", "entityListOutput: " + JsonOutput.prettyPrint(dataAsJson.toString()))

}
catch (Exception ex) {
    consoleLog("ERROR", +ex.toString())
}

consoleLog("INFO", "End prepareEntityListFormData. entity: " + entity + " entityListKey: " + entityListKey)
