import groovy.transform.Field

//
// MDM

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO", "Start processReadBusinessPartnerVersionsResultSelection")

// get form output
formOutput = task.getVariable("partnerVersionsResultOutput")

println "formOutput: " + formOutput

//
Map selectedEntry = [:]
Map readFullPartnerParameters = [:]
Date keyDate
String BITEMPMDMTimestamp
try {
// go to selection
    ArrayList list = formOutput.isBusinessPartner
    list.each { line ->
        if (line.selector == true) {
            keyDate = line.BITEMPValidFrom
            BITEMPMDMTimestamp = line.BITEMPMDMTimestamp
        }
    }


}
catch (Exception ex) {
}

// if nothing is selected formAction is always false

if (keyDate == null) {
    task.setVariable("formAction", "cancel")
} else {
    task.setVariable("keyDate", keyDate)
    task.setVariable("BITEMPMDMTimestamp",BITEMPMDMTimestamp.toString())
}

consoleLog("INFO", "Selected version: BITEMPMDMTimestamp: " + BITEMPMDMTimestamp + "BITEMPValidFrom: " + keyDate)
consoleLog("INFO", "End processReadBusinessPartnerVersionsResultSelection")