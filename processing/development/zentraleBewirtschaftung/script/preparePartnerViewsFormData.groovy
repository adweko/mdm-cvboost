import groovy.json.JsonOutput
import groovy.transform.Field
import org.codehaus.groovy.runtime.NullObject

//
// MDM prepare partnerViews data for form
//

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO", "Start preparePartnerViewsFormData")

// get full partner data
try {
    def fullPartnerInput = task.getVariable("fullPartnerInput")
// set variable for form input
    def partnerViewsInput = fullPartnerInput
    task.setVariable("partnerViewsInput", partnerViewsInput)

    def dataAsJson = JsonOutput.toJson(partnerViewsInput)
    consoleLog("INFO", "partnerViewsInput: " + JsonOutput.prettyPrint(dataAsJson.toString()))

}
catch (Exception ex) {
    consoleLog("ERROR", +ex.toString())
}

task.setVariable("formTemplate", "partnerViews")
task.setVariable("partnerViewsDefinition", resources.get("deployment:/json/form/partnerViews.json").load("json"))
task.setVariable("partnerViewsButton", resources.get("deployment:/json/action/partnerViews.json").load("json"))

consoleLog("INFO", "End preparePartnerViewsFormData")
