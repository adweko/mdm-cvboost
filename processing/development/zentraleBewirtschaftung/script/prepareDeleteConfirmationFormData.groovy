import groovy.transform.Field

//
// MDM prepare deleteConfirmation form data
//

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO", "Start prepare deleteConfirmation form data")

// get form output
formOutput = task.getVariable("searchPartnerResultOutput")

Map selectedEntry = [:]
Map readFullPartnerParameters = [:]
String TECHMDMPartnerID
try {
// go to selection
    ArrayList list = formOutput.BusinessPartnerID
    list.each { line ->
        if (line.selector == true) {
            TECHMDMPartnerID = line.TECHMDMPartnerID
        }
    }

}
catch (Exception ex) {
}

// if nothing is selected formAction is always false
// set MDM Partner ID
if (TECHMDMPartnerID == null) {
    task.setVariable("formAction", false)
    task.removeVariable("TECHMDMPartnerID")
} else {
    task.setVariable("TECHMDMPartnerID", TECHMDMPartnerID)
}

// prepare deleteConfirmationInput form
Map deleteConfirmationInput = [:]
deleteConfirmationInput = [TECHMDMPartnerID: TECHMDMPartnerID]
task.setVariable("deleteConfirmationInput", deleteConfirmationInput)

task.setVariable("deleteConfirmationDefinition", resources.get("deployment:/json/form/deleteConfirmation.json").load("json"))


consoleLog("INFO", "Selected partner to delete: " + TECHMDMPartnerID)

consoleLog("INFO", "End prepare deleteConfirmation form data")