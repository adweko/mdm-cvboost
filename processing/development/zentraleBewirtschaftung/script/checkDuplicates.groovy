import groovy.transform.Field

//
// MDM check duplicates
//

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}
consoleLog("INFO",  "Start checkDuplicates")

// prepare data for duplicate check

// call duplicate check

// if duplicates exist prepare data for showDuplicate form

// set action to show duplicates

// if no duplicates exist create Business partner
task.setVariable("duplicates", false)

consoleLog("INFO",  "End checkDuplicates")