import groovy.json.JsonSlurper
import groovy.transform.Field

//
// MDM prepare role data for form
//

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO",   "Start prepareRoleFormData")

// get form data from showFullPartner form
fullPartnerOutput = task.getVariable("fullPartnerOutput")

// get full partner data
fullPartnerInput = task.getVariable("fullPartnerInput")

// fill roleInput
// get selection parameters
Map roleInput = [:]
Map selectedRole
try {
    ArrayList roleList = fullPartnerOutput.RoleID
    roleList.each { role ->
        if (role.selector == true) {
            selectedRole = role
        }
    }

    roleInput.put("RoleID", [selectedRole])
    task.setVariable("roleInput", roleInput)

}
catch (Exception ex) {
}
consoleLog("INFO",  "End prepareRoleFormData")