import groovy.transform.Field

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)

@Field String consoleLogLevel
consoleLogLevel = "INFO"
task.setVariable("consoleLogLevel", consoleLogLevel)

// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
        case "DEBUG":
            logger.info(message)
            break
    }
}

consoleLog("INFO", "Start initializeWorkflow")

// Set readonly mode to false
task.setVariable("readonly", false)
consoleLog("INFO", "readonly: false")

// Read Spectrum true/false
task.setVariable("readSpectrum", true)
consoleLog("INFO",  "readSpectrum: " + task.getVariable("readSpectrum"))

// Write Spectrum true/false
task.setVariable("writeSpectrum", true)
consoleLog("INFO",  "writeSpectrum: " + task.getVariable("writeSpectrum"))

// Use Kafka true/false
task.setVariable("useKafka", true)
consoleLog("INFO",  "useKafka: " + task.getVariable("useKafka"))

// Set MDM Outbound Kafka Topic
task.setVariable("kafkaTopic", "${application.getProperty("mdm.outboundChangeKafkaTopic")}")
consoleLog("INFO", "kafkaTopic: ${application.getProperty("mdm.outboundChangeKafkaTopic")}")

consoleLog("INFO", "End initializeWorkflow")