import groovy.transform.Field

//
// MDM save role data in fullPartnerOutput
//

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO", "Start saveShippingInstructionFormData")

// get form data from fullPartner form
fullPartnerOutput = task.getVariable("fullPartnerOutput")
// get form data from role form
roleOutput = task.getVariable("shippingInstructionOutput")

// modify fullPartnerOutput from shippingInstructionOutput
Map modifiedShippingInstruction = [:]
Map shippingInstructions = [:]
try {
// go to correct ShippingInstruction in fullPartnerInput
    ArrayList shippingInstructionList = fullPartnerOutput.ShippingInstructionID
    shippingInstructionList.each { shippingInstruction ->
        if (shippingInstruction.selector == true) {
            shippingInstructionOutput.each { field, value ->
                // add values from role form
                modifiedShippingInstruction.put(field, value)
            }
            shippingInstructions.add(modifiedShippingInstruction)
        } else {
            shippingInstructions.add(role)
        }
    }

    fullPartnerOutput.put("ShippingInstructionID", shippingInstructions)
    task.setVariable("fullPartnerOutput", fullPartnerOutput)

}
catch (Exception ex) {
}

consoleLog("INFO", "End saveShippingInstructionFormData")