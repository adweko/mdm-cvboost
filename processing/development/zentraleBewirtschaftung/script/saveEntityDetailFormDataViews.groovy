import groovy.json.JsonOutput
import groovy.transform.Field


//
// MDM save entityDetail data for form
//

String entityListKey = task.getVariable("entityListKey")
String entity = task.getVariable("entity")

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO", "Start saveEntityDetailFormDataViews. entity: " + entity + " entityListKey: " + entityListKey)

try {
    // get entityList
    def entityListOutput = task.getVariable("partnerViewsOutput")
    // get entityDetail Output
    def entityDetailOutput = task.getVariable("entityDetailOutput")

    // get selected location
    Map entityDetailInput = [:]
    Map selectedEntity = [:]

    ArrayList entityList = []
    ArrayList updatedEntityList = []
    ArrayList updatedEntities = []
    Map updatedEntityListOutput = [:]
    try {
        entityList = entityListOutput.get(entityListKey)
        entityList.each { singleEntity ->
            if (singleEntity.selector == true) {
                // replace with data from entityDetailOutput
                updatedEntities.add(entityDetailOutput)
            } else {
                // keep data from entityListOutput
                updatedEntities.add(singleEntity)
            }
        }
        entityListOutput.put(entityListKey, updatedEntities)
        task.setVariable("partnerViewsOutput", entityListOutput)
        def dataAsJson = JsonOutput.toJson(entityListOutput)
        consoleLog("INFO", "partnerViewsOutput: " + JsonOutput.prettyPrint(dataAsJson.toString()))
    }
    catch (Exception ex) {
    }
}
catch (Exception ex) {
    consoleLog("ERROR", ex.toString())
}

consoleLog("INFO", "End saveEntityDetailFormDataViews. entity: " + entity +  " entityListKey: " + entityListKey)
