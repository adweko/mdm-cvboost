import groovy.transform.Field

//
// MDM prepare identificationNumber data for form
//

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO",   "Start prepareIdentificationNumberFormData")
// get full partner data
try {
    def fullPartnerInput = task.getVariable("fullPartnerInput")
// set variable for form input
    def relationInput = [:]

    relationInput.put("hasBusinessPartnerRelationFrom", fullPartnerInput.get("BusinessPartnerID").first().get("hasBusinessPartnerRelationFrom"))

    task.setVariable("relationInput", relationInput)
}
catch (Exception ex) {
    consoleLog("ERROR",   ex.toString())
}

consoleLog("INFO",   "End prepareIdentificationNumberFormData")
