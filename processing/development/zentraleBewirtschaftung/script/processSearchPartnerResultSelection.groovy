import groovy.transform.Field

//
// MDM save role data in fullPartnerOutput
//

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO", "Start processSearchPartnerResultSelection")

// get form output
formOutput = task.getVariable("searchPartnerResultOutput")

//
Map selectedEntry = [:]
Map readFullPartnerParameters = [:]
String TECHMDMPartnerID
try {
// go to selection
    ArrayList list = formOutput.BusinessPartnerID
    list.each { line ->
        if (line.selector == true) {
            TECHMDMPartnerID = line.TECHMDMPartnerID
        }
    }


}
catch (Exception ex) {
}

// if nothing is selected formAction is always false

if (TECHMDMPartnerID == null) {
    task.setVariable("formAction", "cancel")
} else {
    task.setVariable("TECHMDMPartnerID", TECHMDMPartnerID)
}

consoleLog("INFO", "Selected partner: " + TECHMDMPartnerID)
consoleLog("INFO", "End processSearchPartnerResultSelection")