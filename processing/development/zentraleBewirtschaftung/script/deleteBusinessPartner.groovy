import groovy.json.JsonBuilder
import groovy.json.JsonSlurperClassic
import groovy.json.JsonOutput
import groovy.transform.Field

//---------------------------------------
// Call a service in Spectrum MDM
//---------------------------------------

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

def writeSpectrum = task.getVariable("writeSpectrum")
if (writeSpectrum == null){
    writeSpectrum = true
}

// get field definitions
Map fieldDefinitions = resources.get("system:${application.getProperty("mdm.metadata")}").load("json")

// get language of user
currentLocale = localization.getLocale()
lang = currentLocale.getLanguage()

/**
 * Name of the service
 */
def service = "deleteBusinessPartner"

consoleLog("INFO", "Start call service ${service}")

if (!writeSpectrum){
    consoleLog("INFO", "Service ${service} will not be be called, writeSpectrum == false")
    return
}

/**
 * read service definition JSON
 */
def serviceJson = resources.get("deployment:/json/service/${service}.json")
def serviceMap = serviceJson.load("json")

/**
 * read data for the service call parameters
 */
def serviceParameterData = "actionBusinessPartnerOutput"
serviceParameter = task.getVariable("${serviceParameterData}")

/**
 * Fill serviceMap from serviceParameter
 */
if (serviceParameter.keyDate != null) {
    serviceMap.I.Row[0].BITEMPValidFrom = serviceParameter.keyDate.format("dd.MM.yyyy")
}
serviceMap.I.Row[0].TECHExternalPID = task.getVariable("system.workorder.id")
serviceMap.I.Row[0].TECHMDMPartnerID = task.getVariable("TECHMDMPartnerID")
serviceMap.I.Row[0].TECHSourceSystem = "MDM"


def mapAsJson = JsonOutput.toJson(serviceMap)
consoleLog("INFO", "Service parameters: " + JsonOutput.prettyPrint(mapAsJson.toString()))

/**
 * call spectrum deleteBusinessPartner REST service POST method
 */
consoleLog("INFO", "Calling Spectrum REST service ${service}")

def serviceResult
Boolean state = true

task.setVariable("messageLevel", "Info")

try {
    spectrum.request(service)
            .object("json", serviceMap)
            .acceptJSON()
            .execute({info, inStream ->
                serviceResultText = inStream.getText(info.contentEncoding)
                contentType = info.getContentType()
            })
    if (contentType == "application/json") {
        // Service returned a json, worked fine
        serviceResult = new groovy.json.JsonSlurperClassic().parseText(serviceResultText)

    }
    else{
        // error happened
        consoleLog("ERROR", "Error when calling service ${service}: " + serviceResultText)

        task.setVariable("errorLabel", "Error when calling service ${service}")
        task.setVariable("errorMessage", "serviceResults: " + serviceResultString)
        task.setVariable("readPartnerOK", false)

        state = false
    }
}
catch (Exception ex) {
    consoleLog("ERROR", "Exception: " + ex.toString())

    // set variables for error message
    task.setVariable("messageLabel", ex.toString())
    task.setVariable("messageText", "serviceResult: " + serviceResult.toString())
    task.setVariable("messageLevel", "Error")

    // set formInput for service result display form
    task.setVariable("displayServiceResultInput", [serviceResult: ex.toString()])

    state = false
}

if (state) {
    try {
        mapAsJson = JsonOutput.toJson(serviceResult)
        consoleLog("INFO", "Service results: " + JsonOutput.prettyPrint(mapAsJson.toString()))

        // Prepare message for MDM Outbound Kafka Topic
        resources.get("/deletePartnerKafkaMessage.json").save("json", serviceResult)

        // set variables for success message
        task.setVariable("messageLabel", "Serviceaufruf: " + service + " erfolgreich")
        String messageText = "TECHMDMPartnerID: " + serviceMap.I.Row[0].TECHMDMPartnerID +
                " BITEMPValidFrom: " + serviceMap.I.Row[0].BITEMPValidFrom
        task.setVariable("messageText", messageText)
        task.setVariable("messageLevel", "Info")

        // set formInput for service result display form
        def displayServiceResultInput = [serviceResult: fieldDefinitions.get("partnerDeleted").language.get(lang).shorttxt]
        task.setVariable("displayServiceResultInput", displayServiceResultInput)

    }
    catch (Exception ex) {
        consoleLog("ERROR", "Exception: " + ex.toString())

        // set variables for error message
        task.setVariable("messageLabel", ex.toString())
        task.setVariable("messageText", "serviceResult: " + serviceResult.toString())
        task.setVariable("messageLevel", "Error")

        // set formInput for service result display form
        task.setVariable("displayServiceResultInput", [serviceResult: ex.toString()])

    }
}

// set definitions for service result display form
task.setVariable("displayServiceResultDefinition", resources.get("deployment:/json/form/displayServiceResult.json").load("json"))
task.setVariable("displayServiceResultButton", resources.get("deployment:/json/action/displayServiceResult.json").load("json"))

consoleLog("INFO", "End call service ${service}")