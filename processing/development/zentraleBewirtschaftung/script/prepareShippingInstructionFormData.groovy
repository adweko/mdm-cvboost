import groovy.transform.Field

//
// MDM prepare shippingInstruction data for form
//

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO",   "Start prepareshippingInstructionData")

// get form data from showFullPartner form
fullPartnerOutput = task.getVariable("fullPartnerOutput")

// get full partner data
fullPartnerInput = task.getVariable("fullPartnerInput")

// fill roleInput
// get selection parameters
Map shippingInstructionInput = [:]
Map selectedShippingInstruction
try {
    ArrayList shippingInstructionList = fullPartnerOutput.ShippingInstructionID
    shippingInstructionList.each { shippingInstruction ->
        if (shippingInstruction.selector == true) {
            selectedShippingInstruction = shippingInstruction
        }
    }

    shippingInstructionInput.put("ShippingInstructionID", [selectedShippingInstruction])
    task.setVariable("shippingInstructionInput", shippingInstructionInput)

}
catch (Exception ex) {
}
consoleLog("INFO",   "End prepareShippingInstructionFormData")