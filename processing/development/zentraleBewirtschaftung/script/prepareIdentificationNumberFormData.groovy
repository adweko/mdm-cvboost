import groovy.json.JsonOutput
import groovy.transform.Field
import org.codehaus.groovy.runtime.NullObject

//
// MDM prepare identificationNumber data for form
//

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO", "Start prepareIdentificationNumberFormData")

// get full partner data
try {
    def fullPartnerInput = task.getVariable("fullPartnerInput")
// set variable for form input
    def identificationNumberInput = [:]
    try {
        identificationNumberInput.put("hasIdentificationNumber", fullPartnerInput.get("BusinessPartnerID").first().get("hasIdentificationNumber"))
    }
    catch(Exception ex){
        // no identification numbers exist
    }
    task.setVariable("identificationNumberInput", identificationNumberInput)

    def identificationNumberOutput = [:]
    identificationNumberOutput = task.getVariable("identificationNumberOuput")

    if (identificationNumberOutput in Map) {
        identificationNumberInput.put("hasIdentificationNumber", identificationNumberOutput)
    }

    def dataAsJson = JsonOutput.toJson(identificationNumberInput)
    consoleLog("INFO", "identificationNumberInput: " + JsonOutput.prettyPrint(dataAsJson.toString()))

}
catch (Exception ex) {
    consoleLog("ERROR", +ex.toString())
}

consoleLog("INFO", "End prepareIdentificationNumberFormData")
