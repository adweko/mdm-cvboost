import groovy.transform.Field

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO", "Start switchEditMode")

Boolean readonly
String partnerMode
try {
    readonly = task.getVariable("readonly")
    partnerMode = task.getVariable("partnerMode")
}
catch (Exception ex) {
    readonly = false
}

if (readonly == true) {
    readonly = false
} else {
    readonly = true
}

if ((partnerMode == "showPartner") && (readonly == false)) {
// if you show a partner and change to editable => mode changes to updatePartner once
    partnerMode = "updatePartner"
}

task.setVariable("readonly", readonly)
task.setVariable("partnerMode", partnerMode)

consoleLog("INFO", "partnerMode: " + partnerMode + " readonly: " + readonly)

consoleLog("INFO", "End switchEditMode")


