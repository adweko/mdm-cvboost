import groovy.transform.Field

//
// MDM save role data in fullPartnerOutput
//

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO", "Start saveRoleFormData")

// get form data from fullPartner form
fullPartnerOutput = task.getVariable("fullPartnerOutput")
// get form data from role form
roleOutput = task.getVariable("roleOutput")

// modify fullPartnerOutput from roleOutput
Map modifiedRole = [:]
Map roles = [:]
try {
// go to correct role in fullPartnerInput
    ArrayList roleList = fullPartnerOutput.RoleID
    roleList.each { role ->
        if (role.selector == true) {
            roleOutput.each { field, value ->
                // add values from role form
                modifiedRole.put(field, value)
            }
            roles.add(modifiedRole)
        } else {
            roles.add(role)
        }
    }

    fullPartnerOutput.put("RoleID", roles)
    task.setVariable("fullPartnerOutput", fullPartnerOutput)

}
catch (Exception ex) {
}

consoleLog("INFO", "End saveRoleFormData")