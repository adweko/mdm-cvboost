import groovy.transform.Field

//
// MDM save role data in fullPartnerOutput
//

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO",   "Start processActionBusinessPartner")

// get form output
try {

    formOutput = task.getVariable("actionBusinessPartnerOutput")

    task.setVariable("TECHMDMPartnerID", formOutput.TECHMDMPartnerID)
    task.setVariable("BITEMPMDMTimestamp", formOutput.BITEMPMDMTimestamp)
    task.setVariable("keyDate", formOutput.keyDate)
    task.setVariable("businessPartnerType", formOutput.businessPartnerType)

    formAction = task.getVariable("formAction")

    switch (formAction) {
        case "showPartner":
            task.setVariable("partnerMode", "showPartner")
            break

        case "updatePartner":
            task.setVariable("partnerMode", "updatePartner")
            break

        case "createPartner":
            task.setVariable("partnerMode", "createPartner")
            break

        case "deletePartner":
            task.setVariable("partnerMode", "deletePartner")
            break
    }

}
catch (Exception ex) {
}

consoleLog("INFO",   "End processActionBusinessPartner")