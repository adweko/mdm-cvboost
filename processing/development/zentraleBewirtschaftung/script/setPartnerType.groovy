import groovy.transform.Field
//
// MDM save role data in fullPartnerOutput
//

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

consoleLog("INFO", "Start setPartnerType")

// get result data from actionBusinessPartner form
def actionBusinessPartnerOutput = task.getVariable("actionBusinessPartnerOutput")

// get value of partnerType
try{
    if(actionBusinessPartnerOutput.businessPartnerType != null) {
        task.setVariable("businessPartnerType", actionBusinessPartnerOutput.businessPartnerType)
    }
    else{
        task.setVariable("businessPartnerType", "N")
    }
    consoleLog("INFO", "businessPartnerType = " + task.getVariable("businessPartnerType"))

    // prepare fullPartnerOutput
    Map fullPartnerOutput = [:]
    fullPartnerOutput.put("name", actionBusinessPartnerOutput.name)
    fullPartnerOutput.put("surname", actionBusinessPartnerOutput.surname)
    fullPartnerOutput.put("dateOfBirth", actionBusinessPartnerOutput.dateOfBirth)
    fullPartnerOutput.put("ZIPCode", actionBusinessPartnerOutput.ZIPCode)
    fullPartnerOutput.put("companyName", actionBusinessPartnerOutput.companyName)
    fullPartnerOutput.put("foundingDate", actionBusinessPartnerOutput.foundingDate)
    fullPartnerOutput.put("ZIPCodeLegalPerson", actionBusinessPartnerOutput.ZIPCodeLegalPerson)
    task.setVariable("fullPartnerOutput", fullPartnerOutput)

    task.setVariable("formTemplate", "fullPartner")
    task.setVariable("formDefinition", resources.get("deployment:/json/form/${formTemplate}.json").load("json"))
    task.setVariable("formButton", resources.get("deployment:/json/action/${formTemplate}.json").load("json"))
    task.removeVariable("fullPartnerInput")
    task.setVariable("partnerMode","createPartner")

}
catch (Exception ex) {
}

consoleLog("INFO", "End setPartnerType")