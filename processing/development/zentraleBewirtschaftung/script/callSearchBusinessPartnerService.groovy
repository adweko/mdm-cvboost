import groovy.json.JsonSlurperClassic
import groovy.json.JsonOutput
import groovy.transform.Field

import java.sql.Timestamp

//---------------------------------------
// Call a service in Spectrum MDM
//---------------------------------------

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "OFF"
try {
    consoleLogLevel = task.getVariable("consoleLogLevel")
}
catch (Exception ex) {
    consoleLogLevel = "OFF"
}
// console logging
def consoleLog(String level, String message) {
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}

def readSpectrum = task.getVariable("readSpectrum")
if (readSpectrum == null){
    readSpectrum = true
}

/**
 * Name of the service
 */
def service = "searchBusinessPartner"

consoleLog("INFO", "Start call service ${service}")

if (!readSpectrum){
    consoleLog("INFO", "Service ${service} will not be be called, readSpectrum == false")
    return
}


/**
 * read service definition JSON
 */
def serviceJson = resources.get("deployment:/json/service/${service}.json")
def serviceMap = serviceJson.load("json")

/**
 * read data for the service call parameters
 */
def serviceParameterData = "actionBusinessPartnerOutput"
serviceParameter = task.getVariable("${serviceParameterData}")

/**
 * Fill serviceMap from serviceParameter
 */
if (serviceParameter.keyDate != null) {
    serviceMap.I.Row[0].keyDate = serviceParameter.keyDate.format("dd.MM.yyyy")
}
if (serviceParameter.dateOfBirth != null) {
    serviceMap.I.Row[0].dateOfBirth = serviceParameter.dateOfBirth.format("dd.MM.yyyy").toString()
}
if(serviceParameter.businessPartnerType == "N"){
    // Natural Person
    serviceMap.I.Row[0].ZIPCode = serviceParameter.ZIPCode
}
else{
    // Legal Person
    serviceMap.I.Row[0].ZIPCode = serviceParameter.ZIPCodeLegalPerson
}
serviceMap.I.Row[0].name = serviceParameter.name
serviceMap.I.Row[0].surname = serviceParameter.surname
serviceMap.I.Row[0].companyName = serviceParameter.companyName
if (serviceParameter.foundingDate != null) {
    serviceMap.I.Row[0].foundingDate = serviceParameter.foundingDate.format("dd.MM.yyyy")
}

if(serviceParameter.BITEMPMDMTimestamp != null) {
    try {
        def BITEMPMDMTimestamp = Date.parse("yyyy-MM-dd HH:mm:ss.SSS", serviceParameter.BITEMPMDMTimestamp)
        serviceMap.I.Row[0].BITEMPMDMTimestamp = BITEMPMDMTimestamp.format("yyyyMMddHHmmssSSS").toString()
    }
    catch(Exception ex){
        consoleLog("ERROR", "Exception: " + ex.toString())
    }
}
serviceMap.I.Row[0].TECHMDMPartnerID = serviceParameter.TECHMDMPartnerID
serviceMap.I.Row[0].businessPartnerType = serviceParameter.businessPartnerType
serviceMap.I.Row[0].IDType = serviceParameter.IDType
serviceMap.I.Row[0].IDNumber = serviceParameter.IDNumber

def mapAsJson = JsonOutput.toJson(serviceMap)
consoleLog("INFO", "Service parameters: " + JsonOutput.prettyPrint(mapAsJson.toString()))

/**
 * call spectrum readBusinessPartner REST service POST method
 */
def serviceResult
def serviceResultString
def jsonSlurper = new JsonSlurperClassic()
Boolean state = true

task.setVariable("searchPartnerResultInput", null)

consoleLog("INFO", "Calling Spectrum REST service ${service}")

LinkedHashMap resultMap
ArrayList resultList


try {
    spectrum.request(service)
            .object("json", serviceMap)
            .acceptJSON()
            .execute({info, inStream ->
                serviceResultText = inStream.getText(info.contentEncoding)
                contentType = info.getContentType()
            })
    if (contentType == "application/json") {
        // Service returned a json, worked fine
        serviceResult = new groovy.json.JsonSlurperClassic().parseText(serviceResultText)
    }
    else{
        // error happened
        consoleLog("ERROR", "Error when calling service ${service}: " + serviceResultText)

        task.setVariable("errorLabel", "Error when calling service ${service}")
        task.setVariable("errorMessage", "serviceResults: " + serviceResultString)
        task.setVariable("readPartnerOK", false)

        state = false
    }

}
catch (Exception ex) {
    consoleLog("ERROR", "Exception: " + ex.toString())

    task.setVariable("errorLabel", ex.toString())
    task.setVariable("errorMessage", "serviceResults: " + serviceResultString)
    task.setVariable("readPartnerOK", false)

    state = false
}

if (state) {
    try {
        // prepare service results for display
        resultList = serviceResult.get("O")

        if (resultList.isEmpty()) {
            // nothing found
            task.setVariable("searchPartnerResultInput", null)
        } else {
            // return webservice response to workflow
            LinkedHashMap searchPartnerResultInput = ["SearchResult": resultList]
            task.setVariable("searchPartnerResultInput", searchPartnerResultInput)
        }
        mapAsJson = JsonOutput.toJson(resultList)
        consoleLog("INFO", "Service results: " + JsonOutput.prettyPrint(mapAsJson.toString()))
    }
    catch (Exception ex) {
        //serviceResult = serviceResultString
        task.setVariable("searchPartnerResultInput", null)
        consoleLog("ERROR", "Exception: " + ex.toString())

        task.setVariable("errorLabel", ex.toString())
        task.setVariable("errorMessage", "serviceResults: " + serviceResult.toString())
        task.setVariable("readPartnerOK", false)
        state = false

    }
}

consoleLog("INFO", "End call service ${service}")