import groovy.json.JsonOutput
import groovy.json.JsonSlurperClassic

import java.sql.Timestamp
import java.time.Instant

logger.debug("Start processTransaction")
if (!useSpectrum) {
    logger.debug("Service ${service} will not be be called, useSpectrum == false")
    return
}
try {

    def service = "inboundTrafoZepasFull"
    def serviceNaturalPerson = "inboundTrafoZepasNaturalPerson"
    def serviceLegalPerson = "inboundTrafoZepasLegalPerson"
    def serviceChannel = "inboundTrafoZepasChannel"
    def serviceLocation = "inboundTrafoZepasLocation"
    def serviceUnderwriting = "inboundTrafoZepasUnderwriting"
    def serviceDuplicates = "inboundTrafoZepasDuplicates"

    def contentType
    Integer loopcounter
    def serviceResultText
    def serviceStatus

    Instant BITEMPMDMTimestamp = task.getVariable("BITEMPMDMTimestamp")
    String MDMPartnerID = ""
    Boolean MDMStatus

    // get task variables
    def serviceMapNaturalPerson = task.getVariable("jsonMapNaturalPerson")
    def serviceMapLegalPerson = task.getVariable("jsonMapLegalPerson")
    def serviceMapChannel = task.getVariable("jsonMapChannel")
    def serviceMapLocation = task.getVariable("jsonMapLocation")
    def serviceMapUnderwriting = task.getVariable("jsonMapUnderwriting")
    def serviceMapDuplicates = task.getVariable("jsonMapDuplicates")
    def useSpectrum = task.getVariable("useSpectrum")

    // set udate flags to false as initial value
    boolean update_flag = false
    boolean has_error = false
    task.setVariable("update_flag", false)
    task.setVariable("update_flag_NP", false)
    task.setVariable("update_flag_LP", false)
    task.setVariable("update_flag_CH", false)
    task.setVariable("update_flag_LO", false)
    task.setVariable("update_flag_UW", false)

    task.setVariable("update_flag_DU", false)
    Map collectionMap = [:]
    Map status = [:]


    // call spectrum inboundtrafo REST services POST method
    logger.debug("Call Spectrum REST service ${service}")

    // Call Inbound Workflow for Natural Person
    if (serviceMapNaturalPerson && !serviceMapDuplicates) {
        // add BITEMPMDMTimestamp and TECHExternalPID
        serviceMapNaturalPerson.I.Row.first().put("BITEMPMDMTimestamp", Timestamp.from(BITEMPMDMTimestamp).format("yyyyMMddHHmmssSSS", TimeZone.getTimeZone("UTC")))
        serviceMapNaturalPerson.I.Row.first().put("TECHExternalPID", task.getVariable("system.workorder.id"))

        def mapAsJsonNaturalPerson = JsonOutput.toJson(serviceMapNaturalPerson)
        logger.debug("serviceMapNaturalPerson as Json: " + JsonOutput.prettyPrint(mapAsJsonNaturalPerson.toString()))

        // Call spectrum
        contentType = ""
        loopcounter = 0
        while (contentType != "application/json") {
            try {
                spectrum.request(serviceNaturalPerson)
                        .object("json", serviceMapNaturalPerson)
                        .acceptJSON()
                        .execute({ info, inStream ->
                            serviceStatus = info.status
                            serviceResultText = inStream.getText(info.contentEncoding)
                            contentType = info.getContentType()
                        })
            }
            catch (Exception ex) {
                logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": Error calling Spectrum service (" + serviceNaturalPerson + "): " + ex.toString())
                loopcounter++
                if (loopcounter == 5) {
                    throw new Exception("Error calling Spectrum service (" + serviceNaturalPerson + "): Retry Limit of 5 reached")
                }
                continue
            }
            if (contentType == "application/json") {
                // Service returned a json, so return a Map object
                serviceResultsNP = new JsonSlurperClassic().parseText(serviceResultText)

            } else {
                logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": Error calling Spectrum service (" + serviceNaturalPerson + "): " + serviceStatus + ", " + serviceResultText)
                throw new Exception("Error calling Spectrum service (" + serviceNaturalPerson + "): " + serviceStatus + ", " + serviceResultText)
            }
        }


        def serviceResultsPrettyNP = JsonOutput.prettyPrint(JsonOutput.toJson(serviceResultsNP).toString())
        logger.debug("serviceResultsNaturalPerson: " + serviceResultsPrettyNP)

        MDMPartnerID = serviceResultsNP.get("BusinessPartnerID").first().get("TECHMDMPartnerID")

        // return webservice response to workflow
        update_flag_NP = serviceResultsNP.BusinessPartnerID[0].hasUpdate
        logger.debug("hasUpdate_NP: " + update_flag_NP)

        //create json for ReadFullPartnerAllVersionsFromDate
        if (update_flag_NP == true) {
            // add to collection Map
            collectionMap.put("NaturalPerson", serviceResultsNP["BusinessPartnerID"][0])
            update_flag = true
            task.setVariable("update_flag", true)
        }

        if (serviceResultsNP.BusinessPartnerID[0].get("Status.Code") == "E") {
            throw new Exception(serviceResultsNP.BusinessPartnerID[0].get("Status.Description"))
        }
    }

    // Call Inbound Workflow for Legal Person
    if (serviceMapLegalPerson && !serviceMapDuplicates) {
        // add BITEMPMDMTimestamp and TECHExternalPID
        serviceMapLegalPerson.I.Row.first().put("BITEMPMDMTimestamp", Timestamp.from(BITEMPMDMTimestamp).format("yyyyMMddHHmmssSSS", TimeZone.getTimeZone("UTC")))
        serviceMapLegalPerson.I.Row.first().put("TECHExternalPID", task.getVariable("system.workorder.id"))

        def mapAsJsonLegalPerson = JsonOutput.toJson(serviceMapLegalPerson)
        logger.debug("serviceMapLegalPerson as Json: " + JsonOutput.prettyPrint(mapAsJsonLegalPerson.toString()))

        // Call spectrum
        contentType = ""
        loopcounter = 0
        while (contentType != "application/json") {
            try {
                spectrum.request(serviceLegalPerson)
                        .object("json", serviceMapLegalPerson)
                        .acceptJSON()
                        .execute({ info, inStream ->
                            serviceStatus = info.status
                            serviceResultText = inStream.getText(info.contentEncoding)
                            contentType = info.getContentType()
                        })
            }
            catch (Exception ex) {
                logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": Error calling Spectrum service (" + serviceLegalPerson + "): " + ex.toString())
                loopcounter++
                if (loopcounter == 5) {
                    throw new Exception("Error calling Spectrum service (" + serviceLegalPerson + "): Retry Limit of 5 reached")
                }
                continue
            }
            if (contentType == "application/json") {
                // Service returned a json, so return a Map object
                serviceResultsLP = new JsonSlurperClassic().parseText(serviceResultText)

            } else {
                logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": Error calling Spectrum service (" + serviceLegalPerson + "): " + serviceStatus + ", " + serviceResultText)
                throw new Exception("Error calling Spectrum service (" + serviceLegalPerson + "): " + serviceStatus + ", " + serviceResultText)
            }
        }

        def serviceResultsPrettyLP = JsonOutput.prettyPrint(JsonOutput.toJson(serviceResultsLP).toString())
        logger.debug("serviceResultsLegalPerson: " + serviceResultsPrettyLP)

        MDMPartnerID = serviceResultsLP.get("BusinessPartnerID").first().get("TECHMDMPartnerID")

        // return webservice response to workflow
        update_flag_LP = serviceResultsLP.BusinessPartnerID[0].hasUpdate
        logger.debug("hasUpdate_LP: " + update_flag_LP)

        //create json for ReadFullPartnerAllVersionsFromDate
        if (update_flag_LP == true) {
            // add to collection Map
            collectionMap.put("LegalPerson", serviceResultsLP["BusinessPartnerID"][0])
            update_flag = true
            task.setVariable("update_flag", true)
        }

        if (serviceResultsLP.BusinessPartnerID[0].get("Status.Code") == "E") {
            throw new Exception(serviceResultsLP.BusinessPartnerID[0].get("Status.Description"))
        }

    }

    // Call Inbound Workflow for Channel
    if (serviceMapChannel && !serviceMapDuplicates) {
        // add BITEMPMDMTimestamp
        serviceMapChannel.I.Row.first().put("BITEMPMDMTimestamp", Timestamp.from(BITEMPMDMTimestamp).format("yyyyMMddHHmmssSSS", TimeZone.getTimeZone("UTC")))
        serviceMapChannel.I.Row.first().put("TECHExternalPID", task.getVariable("system.workorder.id"))

        def mapAsJsonChannel = JsonOutput.toJson(serviceMapChannel)
        logger.debug("serviceMapChannel as Json: " + JsonOutput.prettyPrint(mapAsJsonChannel.toString()))

        // Call spectrum
        contentType = ""
        loopcounter = 0
        while (contentType != "application/json") {
            try {
                spectrum.request(serviceChannel)
                        .object("json", serviceMapChannel)
                        .acceptJSON()
                        .execute({ info, inStream ->
                            serviceStatus = info.status
                            serviceResultText = inStream.getText(info.contentEncoding)
                            contentType = info.getContentType()
                        })
            }
            catch (Exception ex) {
                logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": Error calling Spectrum service (" + serviceChannel + "): " + ex.toString())
                loopcounter++
                if (loopcounter == 5) {
                    throw new Exception("Error calling Spectrum service (" + serviceChannel + "): Retry Limit of 5 reached")
                }
                continue
            }
            if (contentType == "application/json") {
                // Service returned a json, so return a Map object
                serviceResultsCH = new JsonSlurperClassic().parseText(serviceResultText)

            } else {
                logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": Error calling Spectrum service (" + serviceChannel + "): " + serviceStatus + ", " + serviceResultText)
                throw new Exception("Error calling Spectrum service (" + serviceChannel + "): " + serviceStatus + ", " + serviceResultText)
            }
        }

        def serviceResultsPrettyCH = JsonOutput.prettyPrint(JsonOutput.toJson(serviceResultsCH).toString())
        logger.debug("serviceResultsChannel: " + serviceResultsPrettyCH)

        MDMPartnerID = serviceResultsCH.get("BusinessPartnerID").first().get("TECHMDMPartnerID")

        // return webservice response to workflow
        update_flag_CH = serviceResultsCH.BusinessPartnerID[0].hasUpdate
        logger.debug("hasUpdate_CH: " + update_flag_CH)

        //create json for ReadFullPartnerAllVersionsFromDate
        if (update_flag_CH == true) {
            // add to collection Map
            collectionMap.put("Channel", serviceResultsCH["BusinessPartnerID"][0])
            update_flag = true
            task.setVariable("update_flag", true)

        }

        if (serviceResultsCH.BusinessPartnerID[0].get("Status.Code") == "E") {
            throw new Exception(serviceResultsCH.BusinessPartnerID[0].get("Status.Description"))
        }

    }

    // Call Inbound Workflow for Location
    if (serviceMapLocation && !serviceMapDuplicates) {
        // add BITEMPMDMTimestamp
        serviceMapLocation.I.Row.first().put("BITEMPMDMTimestamp", Timestamp.from(BITEMPMDMTimestamp).format("yyyyMMddHHmmssSSS", TimeZone.getTimeZone("UTC")))
        serviceMapLocation.I.Row.first().put("TECHExternalPID", task.getVariable("system.workorder.id"))

        def mapAsJsonLocation = JsonOutput.toJson(serviceMapLocation)
        logger.debug("serviceMapLocation as Json: " + JsonOutput.prettyPrint(mapAsJsonLocation.toString()))

        // Call spectrum
        contentType = ""
        loopcounter = 0
        while (contentType != "application/json") {
            try {
                spectrum.request(serviceLocation)
                        .object("json", serviceMapLocation)
                        .acceptJSON()
                        .execute({ info, inStream ->
                            serviceStatus = info.status
                            serviceResultText = inStream.getText(info.contentEncoding)
                            contentType = info.getContentType()
                        })
            }
            catch (Exception ex) {
                logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": Error calling Spectrum service (" + serviceLocation + "): " + ex.toString())
                loopcounter++
                if (loopcounter == 5) {
                    throw new Exception("Error calling Spectrum service (" + serviceLocation + "): Retry Limit of 5 reached")
                }
                continue
            }
            if (contentType == "application/json") {
                // Service returned a json, so return a Map object
                serviceResultsLO = new JsonSlurperClassic().parseText(serviceResultText)

            } else {
                logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": Error calling Spectrum service (" + serviceLocation + "): " + serviceStatus + ", " + serviceResultText)
                throw new Exception("Error calling Spectrum service (" + serviceLocation + "): " + serviceStatus + ", " + serviceResultText)
            }
        }

        def serviceResultsPrettyLO = JsonOutput.prettyPrint(JsonOutput.toJson(serviceResultsLO).toString())
        logger.debug("serviceResultsLocation: " + serviceResultsPrettyLO)

        MDMPartnerID = serviceResultsLO.get("BusinessPartnerID").first().get("TECHMDMPartnerID")

        // return webservice response to workflow
        update_flag_LO = serviceResultsLO.BusinessPartnerID[0].hasUpdate
        logger.debug("hasUpdate_LO: " + update_flag_LO)

        //create json for ReadFullPartnerAllVersionsFromDate
        if (update_flag_LO == true) {
            // add to collection Map
            collectionMap.put("Location", serviceResultsLO["BusinessPartnerID"][0])
            update_flag = true
            task.setVariable("update_flag", true)
        }

        if (serviceResultsLO.BusinessPartnerID[0].get("Status.Code") == "E") {
            throw new Exception(serviceResultsLO.BusinessPartnerID[0].get("Status.Description"))
        }

    }

    // Call Inbound Workflow for Underwriting
    if (serviceMapUnderwriting && !serviceMapDuplicates) {
        // add BITEMPMDMTimestamp
        serviceMapUnderwriting.I.Row.first().put("BITEMPMDMTimestamp", Timestamp.from(BITEMPMDMTimestamp).format("yyyyMMddHHmmssSSS", TimeZone.getTimeZone("UTC")))
        serviceMapUnderwriting.I.Row.first().put("TECHExternalPID", task.getVariable("system.workorder.id"))

        def mapAsJsonUnderwriting = JsonOutput.toJson(serviceMapUnderwriting)
        logger.debug("serviceMapUnderwriting as Json: " + JsonOutput.prettyPrint(mapAsJsonUnderwriting.toString()))

        // Call spectrum
        contentType = ""
        loopcounter = 0
        while (contentType != "application/json") {
            try {
                spectrum.request(serviceUnderwriting)
                        .object("json", serviceMapUnderwriting)
                        .acceptJSON()
                        .execute({ info, inStream ->
                            serviceStatus = info.status
                            serviceResultText = inStream.getText(info.contentEncoding)
                            contentType = info.getContentType()
                        })
            }
            catch (Exception ex) {
                logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": Error calling Spectrum service (" + serviceUnderwriting + "): " + ex.toString())
                loopcounter++
                if (loopcounter == 5) {
                    throw new Exception("Error calling Spectrum service (" + serviceUnderwriting + "): Retry Limit of 5 reached")
                }
                continue
            }
            if (contentType == "application/json") {
                // Service returned a json, so return a Map object
                serviceResultsUW = new JsonSlurperClassic().parseText(serviceResultText)

            } else {
                logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": Error calling Spectrum service (" + serviceUnderwriting + "): " + serviceStatus + ", " + serviceResultText)
                throw new Exception("Error calling Spectrum service (" + serviceUnderwriting + "): " + serviceStatus + ", " + serviceResultText)
            }
        }

        def serviceResultsPrettyUW = JsonOutput.prettyPrint(JsonOutput.toJson(serviceResultsUW).toString())
        logger.debug("serviceResultsUnderwriting: " + serviceResultsPrettyUW)

        MDMPartnerID = serviceResultsUW.get("BusinessPartnerID").first().get("TECHMDMPartnerID")

        // return webservice response to workflow
        update_flag_UW = serviceResultsUW.BusinessPartnerID[0].hasUpdate
        logger.debug("hasUpdate_UW: " + update_flag_UW)

        //create json for ReadFullPartnerAllVersionsFromDate
        if (update_flag_UW == true) {
            // add to collection Map
            collectionMap.put("Underwriting", serviceResultsUW["BusinessPartnerID"][0])
            update_flag = true
            task.setVariable("update_flag", true)
        }

        if (serviceResultsUW.BusinessPartnerID[0].get("Status.Code") == "E") {
            throw new Exception(serviceResultsUW.BusinessPartnerID[0].get("Status.Description"))
        }
    }

    // Call Inbound Workflow for Duplicates
    if (serviceMapDuplicates) {
        // add BITEMPMDMTimestamp
        serviceMapDuplicates.I.Row.first().put("BITEMPMDMTimestamp", Timestamp.from(BITEMPMDMTimestamp).format("yyyyMMddHHmmssSSS", TimeZone.getTimeZone("UTC")))
        serviceMapDuplicates.I.Row.first().put("TECHExternalPID", task.getVariable("system.workorder.id"))

        def mapAsJsonDuplicates = JsonOutput.toJson(serviceMapDuplicates)
        logger.debug("serviceMapDuplicates as Json: " + JsonOutput.prettyPrint(mapAsJsonDuplicates.toString()))

        // Call spectrum
        contentType = ""
        loopcounter = 0
        while (contentType != "application/json") {
            try {
                spectrum.request(serviceDuplicates)
                        .object("json", serviceMapDuplicates)
                        .acceptJSON()
                        .execute({ info, inStream ->
                            serviceStatus = info.status
                            serviceResultText = inStream.getText(info.contentEncoding)
                            contentType = info.getContentType()
                        })
            }
            catch (Exception ex) {
                logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": Error calling Spectrum service (" + serviceDuplicates + "): " + ex.toString())
                loopcounter++
                if (loopcounter == 5) {
                    throw new Exception("Error calling Spectrum service (" + serviceDuplicates + "): Retry Limit of 5 reached")
                }
                continue
            }
            if (contentType == "application/json") {
                // Service returned a json, so return a Map object
                serviceResultsDU = new JsonSlurperClassic().parseText(serviceResultText)

            } else {
                logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": Error calling Spectrum service (" + serviceDuplicates + "): " + serviceStatus + ", " + serviceResultText)
                throw new Exception("Error calling Spectrum service (" + serviceDuplicates + "): " + serviceStatus + ", " + serviceResultText)
            }
        }

        def serviceResultsPrettyDU = JsonOutput.prettyPrint(JsonOutput.toJson(serviceResultsDU).toString())
        logger.debug("serviceResultsDuplicates: " + serviceResultsPrettyDU)

        MDMPartnerID = serviceResultsDU.get("BusinessPartnerID").first().get("TECHMDMPartnerID")

        // return webservice response to workflow
        update_flag_DU = serviceResultsDU.BusinessPartnerID[0].hasUpdate
        logger.debug("hasUpdate_DU: " + update_flag_DU)

        //create json for ReadFullPartnerAllVersionsFromDate
        if (update_flag_DU == true) {
            // add to collection Map
            // TODO: Change to "Duplicates". but this will
            collectionMap.put("Duplicates", serviceResultsDU["BusinessPartnerID"][0])
            update_flag = true
            task.setVariable("update_flag", true)
        }

        if (serviceResultsDU.BusinessPartnerID[0].get("Status.Code") == "E") {
            throw new Exception(serviceResultsDU.BusinessPartnerID[0].get("Status.Description"))
        }

    }
    // Step 4: Update MDM Status, update TransactionCommitted Status
    logger.debug("update TransactionCommited Status")
    functions.MDMTransactionStatus.updateTransactionCommittedStatus(task.getVariable("system.workorder.id"), workOrder.owner, true, update_flag)

    // Step 4: Update MDM Status, update MDMPartnerID Status
    logger.debug("update MDMPartnerID Status")
    functions.MDMTransactionStatus.updateMDMPartnerID(task.getVariable("system.workorder.id"), workOrder.owner, MDMPartnerID)

    // Build kafka outbound change
    Map temporaryMap = [:]

    collectionMap.each { mapName, map ->
        map.each { key, val ->
            if (key == "CRUD") {
                return
            } else if (key == "TECHInternalPID") {
                temporaryMap.put(key + "_" + mapName, val)
            } else if (!temporaryMap.containsKey(key)) {
                temporaryMap.put(key, val)
            } else if (temporaryMap.containsKey(key) && temporaryMap[key] != val && val[0] != null) {
                temporaryMap[key].add(val[0])
            }
        }
    }
    Map kafkaOutChangeMap = [
            "BusinessPartnerID": [
                    temporaryMap
            ]
    ]

    //create json for ReadFullPartnerAllVersionsFromDate
    if (update_flag == true) {

        // write kafka message for change topic
        resources.get("/kafka_out_change.json").save("json", kafkaOutChangeMap)

        // Step 5: Update MDM Status, update ChangeOutboundGenerated Status
        functions.MDMTransactionStatus.updateTransactionStatus(task.getVariable("system.workorder.id"), workOrder.owner, 2, true, MDMPartnerID, Timestamp.from(BITEMPMDMTimestamp), kafkaOutChangeMap, [resources: resources])

        // extract kafka_key
        Map isBusinessPartner = kafkaOutChangeMap.get("BusinessPartnerID").first().get("isBusinessPartner").first()
        isBusinessPartner.remove("BusinessPartner")
        isBusinessPartner.remove("TECHInternalPID")
        String kafka_key = JsonOutput.toJson(isBusinessPartner).toString()
        String isBusinessPartnerPretty = JsonOutput.prettyPrint(JsonOutput.toJson(isBusinessPartner).toString())
        task.setVariable("kafka_key", kafka_key)
        logger.debug("kafka_key: " + isBusinessPartnerPretty)

        task.setVariable("error", false)

        // load service definition for ReadFullPartnerAllVersionsFromDate
        String service1 = "readFullPartnerAllVersionsFromDate"
        def serviceMap1 = resources.get("deployment:/json/service/${service1}.json").load("json")

        logger.debug("serviceMap1: " + serviceMap1)

        // Fill service map from object map
        serviceMap1.I.Row[0].TECHMDMPartnerID = kafkaOutChangeMap.BusinessPartnerID[0].TECHMDMPartnerID
        serviceMap1.I.Row[0].CRUD = kafkaOutChangeMap.BusinessPartnerID[0].CRUD
        serviceMap1.I.Row[0].TECHInternalPIDIn = kafkaOutChangeMap.BusinessPartnerID[0].TECHInternalPID
        serviceMap1.I.Row[0].TECHSourceSystemIn = "CH_ZEPAS"
        serviceMap1.I.Row[0].keyDate = new Date().format("dd.MM.yyyy")

        logger.debug("serviceMap1: " + serviceMap1)

        // Call spectrum
        contentType = ""
        loopcounter = 0
        while (contentType != "application/json") {
            try {
                spectrum.request(service1)
                        .object("json", serviceMap1)
                        .acceptJSON()
                        .execute({ info, inStream ->
                            serviceStatus = info.status
                            serviceResultText = inStream.getText(info.contentEncoding)
                            contentType = info.getContentType()
                        })
            }
            catch (Exception ex) {
                logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": Error calling Spectrum service (" + service1 + "): " + ex.toString())
                loopcounter++
                if (loopcounter == 5) {
                    throw new Exception("Error calling Spectrum service (" + service1 + "): Retry Limit of 5 reached")
                }
                continue
            }
            if (contentType == "application/json") {
                // Service returned a json, so return a Map object
                serviceResults1 = new JsonSlurperClassic().parseText(serviceResultText)

            } else {
                logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": Error calling Spectrum service (" + service1 + "): " + serviceStatus + ", " + serviceResultText)
                throw new Exception("Error calling Spectrum service (" + service1 + "): " + serviceStatus + ", " + serviceResultText)
            }
        }

        def serviceResultsPretty1 = JsonOutput.prettyPrint(JsonOutput.toJson(serviceResults1).toString())
        logger.debug("serviceResults1: " + serviceResultsPretty1)

        // write kafka message
        resources.get("/kafka_out_state.json").save("json", serviceResults1)
        task.setVariable("state_kafka", true)

        // Step 6: Update MDM Status, update StateOutboundGenerated Status
        functions.MDMTransactionStatus.updateTransactionStatus(task.getVariable("system.workorder.id"), workOrder.owner, 3, true, MDMPartnerID, Timestamp.from(BITEMPMDMTimestamp), serviceResults1, [resources: resources])

        // call kafka producer for change topic
        logger.debug("Call Kafka producer for MDM Change Topic (" + application.getProperty("mdm.outboundChangeKafkaTopic") + "), key: " + kafka_key)
        messaging.message(application.getProperty("mdm.outboundChangeKafkaTopic"))
                .nodeId("AGENT_MESSAGING_KAFKA")
                .source("/kafka_out_change.json")
                .property("converter", "Json2Avro")
                .property("converter.subject", "ch-mdm.change.all")
                .property("converter.version", task.getVariable("avro.schema.change.version"))
                .property("key", kafka_key)
                .send()

        // Step 7: Update MDM Status, update ChangeOutboundCommitted Status
        functions.MDMTransactionStatus.updateTransactionStatus(task.getVariable("system.workorder.id"), workOrder.owner, 4, true)

        // call kafka producer for state topic
        logger.debug("Call Kafka producer for MDM State Topic (" + application.getProperty("mdm.outboundStateKafkaTopic") + "), key: " + kafka_key)
        messaging.message("ch-mdm.state.avro")
                .nodeId("AGENT_MESSAGING_KAFKA")
                .source("/kafka_out_state.json")
                .property("key", kafka_key)
                .property("converter", "Json2Avro")
                .property("converter.subject", "ch-mdm.state.all")
                .property("converter.version", task.getVariable("avro.schema.state.version"))
                .send()


        // Step 8: Update MDM Status, update StateOutboundCommitted Status
        functions.MDMTransactionStatus.updateTransactionStatus(task.getVariable("system.workorder.id"), workOrder.owner, 5, true)

    } else {
        // As there is no Delta, no output is generated
        // Step 5: Update MDM Status, update ChangeOutboundGenerated Status
        functions.MDMTransactionStatus.updateTransactionStatus(task.getVariable("system.workorder.id"), workOrder.owner, 2, true)
        // Step 6: Update MDM Status, update StateOutboundGenerated Status
        functions.MDMTransactionStatus.updateTransactionStatus(task.getVariable("system.workorder.id"), workOrder.owner, 3, true)
        // Step 7: Update MDM Status, update ChangeOutboundCommitted Status
        functions.MDMTransactionStatus.updateTransactionStatus(task.getVariable("system.workorder.id"), workOrder.owner, 4, true)
        // Step 8: Update MDM Status, update StateOutboundCommitted Status
        functions.MDMTransactionStatus.updateTransactionStatus(task.getVariable("system.workorder.id"), workOrder.owner, 5, true)

        task.setVariable("error", false)
    }

}
catch (Exception ex) {
    logger.error("Workorder ID: " + task.getVariable("system.workorder.id") + ": " + ex.toString())
    task.setVariable("error", true)
    task.setVariable("update_flag", false)
    task.setVariable("error_message", ex.toString())
}

logger.debug("End processTransaction")