logger.info("------------------writeJSONs START---------------")
import java.text.SimpleDateFormat

def transaction = resources.get("system:${application.getProperty("mdm.test")}/transaction.json").load("json")

Date dateObj =  new Date( ( (long)transaction[0].timestamp) )
def cleanDate = new SimpleDateFormat('yyyyMMddHHmmssSSS').format(dateObj).toString()

String externalPartnerID = transaction[0].kupeLaufnummer.toString()

String year = cleanDate.substring(0,4)
String month = cleanDate.substring(4,6)
String day = cleanDate.substring(6,8)
//String hour = cleanDate.substring(8,10)

// ------------ INBOUND ------------
// create the inbound folder path according to the timestamp
String inboundFolder = "/inbound/" + year + '/' + month + '/' + day + '/'

// name scheme "<Timestamp>_MDM_inbound_<source>_<PartnerID>_<ProcessID>.json"
String inboundFile = "${cleanDate}_MDM_inbound_CH_Zepas_${externalPartnerID}_.json"

// save json to inbound staging area
resources.get("system:${application.getProperty("mdm.stagingarea")}${inboundFolder}${inboundFile}").save("json", transaction)


// ------------ OUTBOUND ------------
// create the outbound folder path according to the timestamp
String outboundFolder = "/outbound/" + year + '/' + month + '/' + day + '/'

// name scheme "<Timestamp>_MDM_outbound_<state/change>_<PartnerID>_<ProcessID>.json"
String outboundFile = "${cleanDate}_MDM_outbound_state_${externalPartnerID}_.json"

// save json to outbound staging area
resources.get("system:${application.getProperty("mdm.stagingarea")}${outboundFolder}${outboundFile}").save("json", transaction)

logger.info("------------------writeJSONs DONE---------------")
