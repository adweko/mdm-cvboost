logger.info("------------------readJSONs START---------------")
Long queryTimestamp = 202004241000000
Long currentTimestamp = new Date().format( 'yyyyMMddmmssSSS' ).toLong()

Date queryDate = Date.parse("yyyyMMddmmssSSS", queryTimestamp.toString() ).clearTime()
Date currentDate = Date.parse("yyyyMMddmmssSSS", currentTimestamp.toString() ).clearTime()

// loop through all folders beginning with the timestamp and load all contained jsons
def foldersToScan = []
queryDate.upto(currentDate) {
    String dateString = it.format("yyyyMMdd").toString()
    String year = dateString.substring(0,4)
    String month = dateString.substring(4,6)
    String day = dateString.substring(6,8)
    foldersToScan.add("/${year}/${month}/${day}/")

}

logger.info("------------------readJSONs First Folder START---------------")

// go through all files in the first outbound folder and load and write all files from after the timestamp
List files = resources.search("system:${application.getProperty("mdm.stagingarea")}/outbound${foldersToScan[0]}*.json")
/*for (file in files) {
    if (file.basename[0..14].toLong() < queryTimestamp) {
        files.remove(file)
    }
}*/
newfiles = files.findAll {
    it.basename[0..14].toLong() >= queryTimestamp
}

logger.info("------------------readJSONs Other Folders START---------------")

// go through all the other outbound folders and load and write all files
for ( folder in foldersToScan[1..-1] ) {
    newfiles.add(resources.search("system:${application.getProperty("mdm.stagingarea")}/outbound${folder}*.json"))
}

for (file in newfiles) {
    logger.info(file.toString())
    messaging.message("ch-mdm.state.all")
            .source(file.toString())
            .send()
}

logger.info(newfiles.toString())
logger.info("------------------readJSONs DONE---------------")

