import java.sql.Timestamp
import java.time.*
import java.time.temporal.ChronoUnit

logger.debug("Start initializeWorkflow")

// use testfile
task.setVariable("useTestfile", false)
logger.debug("useTestfile: " + task.getVariable("useTestfile"))

// Set initial timestamps
Instant workorderStartTime = Instant.now().minus(7, ChronoUnit.DAYS)
task.setVariable("workorderStartTime", workorderStartTime)

Instant workorderEndTime = workorderStartTime.plus(8, ChronoUnit.DAYS)
task.setVariable("workorderEndTime", workorderEndTime)

// Set initial numberOfRows
task.setVariable("maxNumberOfRows", 100)

// Set Query parameter defaults
task.setVariable("WORKORDER_ID_", "%")
task.setVariable("STATUS_", "%")
task.setVariable("TRANSACTION_SOURCE_", "%")
task.setVariable("TRANSACTION_SOURCE_PROCESS_ID_", "%")
task.setVariable("TRANSACTION_TYPE_", "%")
task.setVariable("TRANSACTION_IDTYPE_", "%")
task.setVariable("TRANSACTION_IDNUMBER_", "%")
task.setVariable("MDM_PARTNERID_", "%")

task.setVariable("cvboosthomeurl", "${application.getProperty("mdm.baseurl")}")

logger.debug("End initializeWorkflow")