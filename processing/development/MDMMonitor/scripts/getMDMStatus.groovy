import groovy.json.JsonSlurperClassic

import java.sql.Timestamp
import java.time.Instant

logger.debug("Start getMDMStatus")

try {

    Map MDMMonitorInput = [:]

    // query Status table
    Instant workorderStartTime = task.getVariable("workorderStartTime")
    Instant workorderEndTime = task.getVariable("workorderEndTime")

    def result
    //result = functions.MDMTransactionStatus.queryTransactionStatus(Timestamp.from(workorderStartTime), task.getVariable("maxNumberOfRows"))
    result = functions.MDMTransactionStatus.queryTransactionStatus(
            Timestamp.from(workorderStartTime),
            Timestamp.from(workorderEndTime),
            task.getVariable("maxNumberOfRows"),
            task.getVariable("WORKORDER_ID_"),
            task.getVariable("STATUS_"),
            task.getVariable("TRANSACTION_SOURCE_"),
            task.getVariable("TRANSACTION_SOURCE_PROCESS_ID_"),
            task.getVariable("TRANSACTION_TYPE_"),
            task.getVariable("TRANSACTION_IDTYPE_"),
            task.getVariable("TRANSACTION_IDNUMBER_"),
            task.getVariable("MDM_PARTNERID_")
    )

    // set form field values
    try {
        MDMMonitorInput.put("ZEPASKafkaConsumer", "systemctl is-active cvboostconnector".execute().text)
        MDMMonitorInput.put("SyriusKafkaConsumer", "systemctl is-active cvboost_agent_kafka".execute().text)
    }
    catch (Exception ex) {
        MDMMonitorInput.put("ZEPASKafkaConsumer", "not supported on this platform")
        MDMMonitorInput.put("SyriusKafkaConsumer", "not supported on this platform")
    }

        // Call spectrum
    def service = "healthCheck"
    def serviceResultText
    def serviceStatus
    def serviceResults
    def contentType

    Map serviceMap = [:]
    Map checkHealth = [:]
    ArrayList RowList = []
    checkHealth.put("checkHealth", true)
    RowList.add(checkHealth)
    // add all rows to "Row"
    Map Input = [:]
    Input.put("Row", RowList)
    // add all rows to "I" to complete service Map
    serviceMap.put("Input", Input)
    contentType = ""
    try {
        spectrum.request(service)
                .object("json", serviceMap)
                .acceptJSON()
                .execute({ info, inStream ->
                    serviceStatus = info.status
                    serviceResultText = inStream.getText(info.contentEncoding)
                    contentType = info.getContentType()
                })
    }
    catch (Exception ex) {
        logger.error("Error calling Spectrum service (" + service + "): " + ex.toString())
        MDMMonitorInput.put("Spectrum", "Error: " + ex.toString())
    }
    if (contentType == "application/json") {
        // Service returned a json, so return a Map object
        serviceResults = new JsonSlurperClassic().parseText(serviceResultText)
        MDMMonitorInput.put("Spectrum", serviceResults.Output.first().result.toString())
    } else {
        logger.error("Error calling Spectrum service (" + service + "): " + serviceStatus + ", " + serviceResultText)
        MDMMonitorInput.put("Spectrum", "Error: " + serviceStatus + ", " + serviceResultText)
    }


    MDMMonitorInput.put("workorderStartTime", Timestamp.from(workorderStartTime).format("yyyyMMddHHmmssSSS", TimeZone.getTimeZone("UTC")))
    MDMMonitorInput.put("workorderEndTime", Timestamp.from(workorderEndTime).format("yyyyMMddHHmmssSSS", TimeZone.getTimeZone("UTC")))
    MDMMonitorInput.put("maxNumberOfRows", task.getVariable("maxNumberOfRows").toString())

    MDMMonitorInput.put("WORKORDER_ID_", task.getVariable("WORKORDER_ID_"))
    MDMMonitorInput.put("STATUS_", task.getVariable("STATUS_"))
    MDMMonitorInput.put("TRANSACTION_SOURCE_", task.getVariable("TRANSACTION_SOURCE_"))
    MDMMonitorInput.put("TRANSACTION_SOURCE_PROCESS_ID_", task.getVariable("TRANSACTION_SOURCE_PROCESS_ID_"))
    MDMMonitorInput.put("TRANSACTION_TYPE_", task.getVariable("TRANSACTION_TYPE_"))
    MDMMonitorInput.put("TRANSACTION_IDTYPE_", task.getVariable("TRANSACTION_IDTYPE_"))
    MDMMonitorInput.put("TRANSACTION_IDNUMBER_", task.getVariable("TRANSACTION_IDNUMBER_"))
    MDMMonitorInput.put("MDM_PARTNERID_", task.getVariable("MDM_PARTNERID_"))

    MDMMonitorInput.put("selectedNumberOfRows", result.get("selectedNumberOfRows").toString())
    MDMMonitorInput.put("selectedNumberOfFailedRows", result.get("selectedNumberOfFailedRows").toString())

    List statusList = []
    List localStatusList = []
    statusList = result.get("statusList")
    statusList.each { row ->
        Map listrow = [:]
        row.each { key, value ->
            if (value instanceof java.sql.Timestamp) {
                listrow.put(key, value.format("yyyyMMddHHmmssSSS", TimeZone.getTimeZone("UTC")))
            } else {
                listrow.put(key, value)
            }
            if ((key == "STATUS_") && (value == null)) {
                listrow.put(key, "No Status found")
            }
        }
        // get process state
        /* listrow.put("processIDStatus", "No State Found")
         com.compoverso.dmp.extension.core.connector.service.jdbc.builder.SelectBuilderImpl selectStatus = jdbc.select() // creates new select for internal database
                 .sql("select STATUS_ from WORK_ORDERS where ID_ like :id") // sets the query with a named parameter
                 .parameter("id", "%" + listrow.get("WORKORDER_ID_").toLowerCase() + "%") // sets the value for the named parameter
         selectStatus.query { workorder_status_row ->
             listrow.put("processIDStatus", workorder_status_row.STATUS_)
         }*/
        localStatusList.add(listrow)
    }

    Map MDMStatusTable = [:]
    MDMStatusTable.put("MDMStatus", localStatusList)
    MDMMonitorInput.put("MDMStatusTable", [MDMStatusTable])

    logger.debug("MDMMonitorInput: " + MDMMonitorInput.toString())
    task.setVariable("MDMMonitorInput", MDMMonitorInput)

}
catch (Exception ex) {

    logger.error(ex.toString())

}
finally {

}
logger.debug("End getMDMStatus")