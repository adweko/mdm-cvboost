import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter

logger.debug("Start processFormMDMMonitor")

// get form output
formOutput = task.getVariable("MDMMonitorOutput")
formAction = task.getVariable("formAction")

def processID

try {

    switch (formAction) {
        case "exit":
            // exit workorder
            task.setVariable("formAction", "exit")
            break

        case "refresh":
            // refresh display
            task.setVariable("formAction", "refresh")

            Instant workorderStartTime = LocalDateTime
                    .parse(formOutput.workorderStartTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"))
                    .atZone(ZoneId.of("UTC"))
                    .toInstant()

            task.setVariable("workorderStartTime", workorderStartTime)

            Instant workorderEndTime = LocalDateTime
                    .parse(formOutput.workorderEndTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"))
                    .atZone(ZoneId.of("UTC"))
                    .toInstant()

            task.setVariable("workorderEndTime", workorderEndTime)


            task.setVariable("maxNumberOfRows", formOutput.maxNumberOfRows.toInteger())
            task.setVariable("WORKORDER_ID_", formOutput.WORKORDER_ID_)
            task.setVariable("STATUS_", formOutput.STATUS_)
            task.setVariable("TRANSACTION_SOURCE_", formOutput.TRANSACTION_SOURCE_)
            task.setVariable("TRANSACTION_SOURCE_PROCESS_ID_", formOutput.TRANSACTION_SOURCE_PROCESS_ID_)
            task.setVariable("TRANSACTION_TYPE_", formOutput.TRANSACTION_TYPE_)
            task.setVariable("TRANSACTION_IDTYPE_", formOutput.TRANSACTION_IDTYPE_)
            task.setVariable("TRANSACTION_IDNUMBER_", formOutput.TRANSACTION_IDNUMBER_)
            task.setVariable("MDM_PARTNERID_", formOutput.MDM_PARTNERID_)



            break

        case "stagingArea":
            task.setVariable("formAction", "stagingArea")
            // get selected entry
            Map selectedEntry = [:]
            try {
                // go to selection
                ArrayList list = formOutput.MDMStatus
                list.each { line ->
                    if (line.selector == true) {
                        processID = line.WORKORDER_ID_
                    }
                }
            }
            catch (Exception ex) {
                task.setVariable("formAction", "refresh")
            }

            task.setVariable("processID", processID)

            functions.MDMTransactionStatus.getFiles(processID, [resources: resources])

            logger.debug("Files moved to Staging area for processID: " + processID)
            break

        case "workorder":
            Map link2WorkorderStatusInput = [:]

            task.setVariable("formTemplate", "link2WorkorderStatus")

            task.setVariable("formDefinition", resources.get("deployment:/json/form/${formTemplate}.json").load("json"))

            task.setVariable("formButton", "")

            task.setVariable("readonly", true)

            task.setVariable("formAction", "workorder")
            // get selected entry
            Map selectedEntry = [:]
            try {
                // go to selection
                ArrayList list = formOutput.MDMStatus
                list.each { line ->
                    if (line.selector == true) {
                        processID = line.WORKORDER_ID_
                    }
                }
            }
            catch (Exception ex) {
                task.setVariable("formAction", "refresh")
            }

            if (processID == null) {
                // nothing selected
                task.setVariable("formAction", "refresh")
            }

            link2WorkorderStatusInput.put("link2WorkorderStatus", "<a href=\"" + task.getVariable("cvboosthomeurl") + "/admin/processing/workorder/details?workorder=" + processID.toLowerCase() + "\" target=\"_blank\">Status: " + processID.toLowerCase() + "</a>")
            task.setVariable("link2WorkorderStatusInput", link2WorkorderStatusInput)

            task.setVariable("processID", processID.toLowerCase())

            logger.debug("Selected processID: " + processID)

            break
    }

}
catch (Exception ex) {
    logger.error(ex.toString())
}
finally {

}
logger.debug("End processFormFormMDMMonitor")




