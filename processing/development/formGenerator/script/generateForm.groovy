import groovy.json.JsonSlurper
import groovy.json.JsonBuilder
import groovy.json.JsonOutput
import com.compoverso.dmp.extension.core.workflow.usertask.form.ResultFormDataContextImpl
import com.compoverso.dmp.extension.core.workflow.usertask.form.builder.FormBuilder
import groovy.transform.Field
import java.sql.Timestamp

//
// MDM form generator
//

@Field def readSpectrum
readSpectrum = task.getVariable("readSpectrum")
if (readSpectrum == null) {
    readSpectrum = true
}

logger.debug("Start generateForm")
// load script wide variables from workflow context

// get MDM field definitions
// Read field definitions
@Field Map fieldDefinitions = resources.get("system:${application.getProperty("mdm.metadata")}").load("json")


// read "readReferenceData" service JSON from template
@Field Map readReferenceDataMap = resources.get("deployment:/json/service/readReferenceData.json").load("json")

// read form definition
@Field Map formMap = task.getVariable("formDefinition")

// try to set businessPartnerType
// possible values LegalPerson and NaturalPerson
@Field String businessPartnerType
try {
    businessPartnerType = task.getVariable("businessPartnerType")
}
catch (Exception ex) {

}

// get language of user
@Field Locale currentLocale = localization.getLocale()
@Field String lang = currentLocale.getLanguage()

// form input
@Field Map inputMap = [:]
@Field Map outputMap


// readonly mode
@Field Boolean readonly = false
try {
    readonly = task.getVariable("readonly")
}
catch (Exception ex) {
    readonly = false
}
// make sure readonly is correctly set
if (readonly == null) {
    readonly = false
}


// support variables for content from form output
@Field boolean flagContentFromFormOutput = false
@Field def firstDataPath

// to convert JSON to map
def jsonSlurper = new JsonSlurper()

// to convert map to JSON
def jsonBuilder = new JsonBuilder()

// set action type for form (Boolean = OK and false, String = user defined action buttons
String action
try {
    action = formMap.action
}
catch (Exception ex) {
// if form map contains no action form action is set to boolean
    action = "boolean"
}

// variables for form
def buttonTrue
def buttonFalse
def actionMap // action Map
def actionMapList = [:] // menu Map List
def actionString
def thisFormActionBuilder
FormBuilder thisForm


def parseFormMap(FormBuilder thisForm, Map formMap, Map inputMap, Map outputMap, Boolean readonly) {

// thisForm: class com.compoverso.dmp.extension.core.workflow.usertask.form.builder.FormBuilder


    // readonly can be:
    // true: fields are set to readonly unless overruled by form map
    // false (default): fields are set to editable unless overruled by formmap

    def type // type of field
    def required = false // required field true or false
    def shorttxt // label of field
    def longtxt // help for of field
    def width // width of field in form (1 to 12)
    boolean hasWidth = false
    def fill // if true fill row completely to the end
    def placeholder // placeholder text for empty fields
    Boolean hasPlaceholder = false
    def fieldReadonly = false // field readonly or not
    def addRow // table fields: User can add rows
    def deleteRow // table fields: User can delete rows
    def displayLimit // table fields: max no of lines of table shown in form
    def arealines // number of lines if string field is an textarea
    def textarea // is string field a textarea
    def listfieldRadio // show listfieldvalues as radio buttons
    def listfieldCheckbox // show listfieldvalues as checkboxes
    def listfieldDynamic // call script to create values dynamically
    def listfieldMulti // show MultiList
    def dataPath = null // path to entity in inputMap
    def thisColumn
    def columnLabel
    def columnHelp
    def columnRequired
    def columnReadonly
    def columnSortable
    def columnWidth
    def columnHidden
    def referenceDataResults = ""// Results of service call
    def referenceDataResultsMap // JSON results string converted to map
    def referenceDataResultsMapOutput // JSON results string converted to map
    def referenceDataMap = [:] // results map converted to form list values map
    def content = "" // prefilled content of field
    def thisField
    def displayEntityWithNoData
    def displayNodeHeader
    def structureContentMap = [:]
    def pathContentEntityData
    def nodeKey
    def nodeValue
    def nodeFields
    def nodeRequired
    def nodeReadonly
    def nodeWidth
    def nodeType = ""
    def nodeBusinessPartnerType

    String tableKey
    def tablePath

    String lang = lang
    String dateString
    Date dateContent
    Timestamp timestampContent

    // set form name
    try {
        thisForm.name(fieldDefinitions.get(formMap.form).language.get(lang).shorttxt)
        logger.debug("formName: " + formMap.form)
    }
    catch (Exception ex) {
        // if no translation available leave default
    }

    // process nodes
    ArrayList nodes = formMap.nodes

    nodes.each {

        it.each {

            nodeKey = it.key
            nodeValue = it.value

            try {
                tablePath = it.value.tablePath
            }
            catch (Exception ex) {
                tablePath = null
            }

            try {
                tableKey = it.value.tableKey
            }
            catch (Exception ex) {
                tableKey = null
            }

            // check type of node
            // tableNode
            // default entityNode
            try {
                nodeType = it.value.nodeType
                if (nodeType == null) {
                    nodeType = "entityNode"
                }
            }
            catch (Exception ex) {
                nodeType = "entityNode" // defaulting to entityNode
            }


            try {
                fill = it.value.fill
                if (fill == null) {
                    fill = false
                }
            }
            catch (Exception ex) {
                fill = false
            }

            nodeBusinessPartnerType = ""
            nodeBusinessPartnerType = it.value.businessPartnerType


            // skip form fields with not matching businessPartnerType
            if ((businessPartnerType != "") && (businessPartnerType != null) &&
                    (nodeBusinessPartnerType != "") && (nodeBusinessPartnerType != null)) {
                if (businessPartnerType != nodeBusinessPartnerType) {
                    return
                }

            }

            try {
                nodeRequired = it.value.required
                if (nodeRequired == null) {
                    nodeRequired = false
                }
            }
            catch (Exception ex) {
                nodeRequired = false
            }


            try {
                nodeReadonly = it.value.readonly
                if (nodeReadonly == null) {
                    nodeReadonly = readonly
                }
            }
            catch (Exception ex) {
                nodeReadonly = readonly
            }

            try {
                nodeWidth = it.value.width
                if (nodeWidth == null) {
                    nodeWidth = false
                }
            }
            catch (Exception ex) {
                nodeWidth = false
            }

            // display header line for entity
            displayNodeHeader = false
            try {
                displayNodeHeader = it.value.displayNodeHeader
                if (displayNodeHeader == null) {
                    displayNodeHeader = false
                }
            }
            catch (Exception ex) {
                displayNodeHeader = false // defaulting to show no header
            }


            try {
                displayEntityWithNoData = it.value.displayEntityWithNoData
                if (displayEntityWithNoData == null) {
                    displayEntityWithNoData = true
                }
            }
            catch (Exception ex) {
                displayEntityWithNoData = true // defaulting to show empty fields
            }

            //
            if (nodeBusinessPartnerType == businessPartnerType) {
                displayEntityWithNoData = true
            }

            try {
                displayLimit = it.value.displayLimit
                if (displayLimit == null) {
                    displayLimit = 5
                }
            }
            catch (Exception ex) {
                displayLimit = 5 // defaulting to show empty fields
            }

            try {
                addRow = it.value.addRow
            }
            catch (Exception ex) {
                // Add Row is allowed if readonly is false
                if (nodeReadonly == false) {
                    addRow = true
                }
                // Delete Row is allowed if readonly is false
                if (nodeReadonly == false) {
                    deleteRow = false // always false
                }
            }

            try {
                dataPath = it.value.dataPath
            }
            catch (Exception ex) {
                // path is optional
            }

            nodeDataMap = inputMap

            // read form data
            try {
                if ((dataPath != null) && (dataPath != false)) {
                    dataPath.each {
                        nodeDataMap = nodeDataMap.get(it).first()
                    }
                }
            }
            catch (Exception ex) {
                // when there is no data available clear data map
                nodeDataMap = [:]
            }

            // Display entity header if empty entites are displayed
            if (displayNodeHeader && displayEntityWithNoData) {
                try {
                    shorttxt = fieldDefinitions.get(nodeKey).language.get(lang).shorttxt
                }
                catch (Exception ex) {
                    shorttxt = nodeKey // defaulting to technical field name
                }
                def message = "<h4>" + shorttxt + "</h4>"
                thisField = thisForm.newText(it.key + "NodeHeader", message)
                thisField.width(12)
                thisField.markdown()
                thisField.add()

            } else if (displayNodeHeader && (!nodeDataMap.isEmpty())) {
                // Display entity header if data is available for entity
                try {
                    shorttxt = fieldDefinitions.get(it.key).language.get(lang).shorttxt
                }
                catch (Exception ex) {
                    shorttxt = it.key // defaulting to technical field name
                }
                def message = "<h4>" + shorttxt + "</h4>"
                def nodeHeaderFieldKey = it.key + "NodeHeader"
                thisField = thisForm.newText(nodeHeaderFieldKey, message)
                thisField.width(12)
                thisField.markdown()
                thisField.add()

            }

            // skip entity without data if configured
            if ((!displayEntityWithNoData) && (nodeDataMap.isEmpty()) && (nodeType == "entityNode")) {
                // skip entity if no there is no data to display
                return
            }

            switch (nodeType) {
                case "tableNode":

                    tableDataMap = inputMap
                    // read form data

                    try {
                        if (tablePath != null) {
                            tablePath.each {
                                tableDataMap = tableDataMap.get(it).first()
                            }
                        }
                    }
                    catch (Exception ex) {
                        // when there is no data available clear data map
                        tableDataMap = [:]
                    }

                    // define table
                    thisField = thisForm.newTable(it.key)

                    if (addRow) {
                        thisField.addRow()
                    }

                    if (deleteRow) {
                        thisField.deleteRow()
                    }

                    thisField.displayLimit(displayLimit.toInteger())

                    thisField.transferAll()

                    // define table columns
                    it.value.nodes.first().each { entity ->

                        entity.value.fields.first().each { tableFieldKey, tableFieldValue ->
                            // get field metadata for column
                            try {
                                columnLabel = fieldDefinitions.get(tableFieldKey).language.get(lang).shorttxt
                            }
                            catch (Exception ex) {
                                columnLabel = tableFieldKey
                            }
                            try {
                                columnHelp = fieldDefinitions.get(tableFieldKey).language.get(lang).longtxt
                            }
                            catch (Exception ex) {
                                columnHelp = tableFieldKey
                            }
                            // get layout data for column
                            try {
                                columnRequired = tableFieldValue.required
                                if (columnRequired == null) {
                                    columnRequired = false
                                }
                            }
                            catch (Exception ex) {
                                columnRequired = false
                            }

                            try {
                                columnReadonly = tableFieldValue.readonly
                                if (columnReadonly == null) {
                                    columnReadonly = readonly
                                }
                            }
                            catch (Exception ex) {
                                columnReadonly = readonly
                            }

                            try {
                                columnSortable = tableFieldValue.sortable
                                if (columnSortable == null) {
                                    columnSortable = true
                                }
                            }
                            catch (Exception ex) {
                                columnSortable = true
                            }

                            try {
                                columnHidden = tableFieldValue.hidden
                                if (columnHidden == null) {
                                    columnHidden = false
                                }
                            }
                            catch (Exception ex) {
                                columnHidden = false
                            }

                            try {
                                columnWidth = ((CharSequence) (tableField.value).width).toInteger()
                                if (columnWidth == null) {
                                    columnWidth = 1
                                }
                            }
                            catch (Exception ex) {
                                columnWidth = 1
                            }

                            // add column
                            try {
                                type = fieldDefinitions.get(tableFieldKey).type
                            }
                            catch (Exception ex) {
                                type = "string"
                            }
                            switch (type) {
                                case "date":
                                    thisColumn = thisField.addDateColumn(tableFieldKey)
                                    break
                                case "boolean":
                                    thisColumn = thisField.addBooleanColumn(tableFieldKey)
                                case "structure":
                                    break
                                case "referenceData":
                                    // load reference data from MDM
                                    readReferenceDataMap.I.Row[0].attributeName = tableFieldKey.toString()
                                    readReferenceDataMap.I.Row[0].language = lang

                                    // call MDM readReferenceData service
                                    if (readSpectrum) {
                                        spectrum.request("readReferenceData")
                                                .object("json", readReferenceDataMap)
                                                .acceptJSON()
                                                .execute({info, inStream ->
                                                    referenceDataResultsText = inStream.getText(info.contentEncoding)
                                                    contentType = info.getContentType()
                                                })
                                        if (contentType == "application/json") {
                                            // Service returned a json, worked fine
                                            referenceDataResults = new groovy.json.JsonSlurperClassic().parseText(referenceDataResultsText)

                                            referenceDataResultsMapOutput = referenceDataResults.O
                                            referenceDataMap.clear()
                                            for (s in referenceDataResultsMapOutput) {
                                                referenceDataMap.put(s.get("value.key"), s.("metaData.shortTxt"))
                                            }
                                        }
                                        else{
                                            // error happened
                                            logger.error("Error when calling service readReferenceData: " + referenceDataResultsText)
                                        }

                                        thisColumn = thisField.addListColumn(tableFieldKey)
                                                .values({ list ->
                                                    referenceDataMap.each { entry ->
                                                        list.value(entry.key, entry.value)
                                                    }
                                                })
                                    } else {
                                        logger.debug("Service readReferenceData will not be be called, readSpectrum == false")
                                        thisColumn = thisField.addListColumn(tableFieldKey)
                                    }

                                    break

                                default: // string
                                    thisColumn = thisField.addStringColumn(tableFieldKey)
                            }
                            thisColumn.label(columnLabel)
                            thisColumn.help(columnHelp)
                            thisColumn.width(columnWidth)
                            if (columnSortable) {
                                thisColumn.sortable()
                            }
                            if (columnReadonly) {
                                thisColumn.readonly()
                            }
                            if (columnRequired) {
                                thisColumn.required()
                            }
                            if (columnHidden) {
                                thisColumn.hidden()
                            }
                            thisColumn.add()
                        }
                    }
                    // finished definition of table columns

                    //  now for the content of the table fields
                    def contentData
                    // 1. Step: insert values from inputMap into table
                    try {
                        contentData = tableDataMap.get(tableKey)
                        if (contentData == null) {
                            contentData = [:]
                        }
                    }
                    catch (Exception ex) {
                        contentData = [:]
                    }

                    // 2. Step: replace with data from outputMap if available
                    flagContentFromFormOutput = false
                    if (outputMap.get(nodeKey) != null) {
                        if (!outputMap.get(nodeKey).isEmpty()) {
                            contentData = []
                            outputMap.get(nodeKey).each { line ->
                                contentData.add(line)
                            }
                            flagContentFromFormOutput = true
                        }
                    }

                    // loop over each entity's content
                    if (flagContentFromFormOutput) {
                        // we have a flat list from form output for the table
                        // => no dataPath
                        contentData.each { contentLine ->
                            Map tableRowMap = [:] // a new Map for each line

                            it.value.nodes.first().eachWithIndex { formEntityData, formEntityIndex ->
                                // every entity in a table node

                                formEntityData.value.fields.first().each { tableFieldKey, tableFieldValue ->
                                    // read content
                                    content = contentLine.get(tableFieldKey)

                                    // every field of a table node entity
                                    try {
                                        type = fieldDefinitions.get(tableFieldKey).type
                                    }
                                    catch (Exception ex) {
                                        type = "string"
                                    }

                                    switch (type) {
                                        case "date":
                                            if (content in Map) {
                                                // date field from Spectrum delivered as map [year:2019, month:5, day:8] => convert
                                                dateString = content.get("day") + "." + content.get("month") + "." + content.get("year")
                                                dateContent = new Date().parse("dd.MM.yyyy", dateString)
                                            } else {
                                                dateContent = content
                                            }
                                            tableRowMap.put(tableFieldKey, dateContent)
                                            break
                                        case "timestamp":
                                            if (content != null) {
                                                timestampContent = Date.parse("yyyyMMddHHmmssSSS", content.take(17)).toTimestamp()
                                                tableRowMap.put(tableFieldKey, timestampContent.toString())
                                            } else {
                                                tableRowMap.put(tableFieldKey, "")
                                            }
                                            break
                                        case "boolean":
                                            tableRowMap.put(tableFieldKey, content)
                                            break
                                        case "structure":
                                            break
                                        default: // string
                                            tableRowMap.put(tableFieldKey, content)
                                    } // switch (type)

                                } // formEntityData.value.fields.first().each { tableFieldKey, tableFieldValue ->
                            } // it.value.nodes.first().eachWithIndex { formEntityData, formEntityIndex ->

                            // add table content line
                            thisField.row(tableRowMap)
                        } // contentData.each { contentLine ->

                    } else {
                        Map tableRowMapFormInput = [:]
                        contentData.each { contentEntityData ->
                           tableRowMapFormInput = [:]

                            // for each entity inside the form definition list
                            // we need to loop over the contentEntityData
                            it.value.nodes.first().each { formEntityData ->

                                // read the formEntityData contentEntityData path
                                try {
                                    dataPath = formEntityData.value.dataPath
                                }
                                catch (Exception ex) {
                                    dataPath = null
                                }

                                pathContentEntityData = contentEntityData
                                try {
                                    if (dataPath != null) {
                                        dataPath.each {
                                            if (pathContentEntityData in Map) {
                                                def tempNodeDataList
                                                try {
                                                    tempNodeDataList = pathContentEntityData.get(it)
                                                    if (tempNodeDataList != null) {
                                                        pathContentEntityData = tempNodeDataList
                                                    }
                                                }
                                                catch (Exception ex) {
                                                }
                                                pathContentEntityData = tempNodeDataList
                                            } else if (pathContentEntityData in List) {
                                                def tempNodeDataList
                                                try {
                                                    tempNodeDataList = pathContentEntityData.first().get(it)
                                                    if (tempNodeDataList != null) {
                                                        pathContentEntityData = tempNodeDataList
                                                    }
                                                }
                                                catch (Exception ex) {
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        pathContentEntityData = [contentEntityData]
                                    }
                                }
                                catch (Exception ex) {
                                    // when there is no contentEntityData available clear pathContentEntityData
                                    pathContentEntityData = []
                                }

                                // now we loop over the content lines of one formEntityData
                                pathContentEntityData.each { nodeDataMap ->

                                    formEntityData.value.fields.first().each { tableFieldKey, tableFieldValue ->
                                        content = nodeDataMap.get(tableFieldKey)

                                        try {
                                            type = fieldDefinitions.get(tableFieldKey).type
                                        }
                                        catch (Exception ex) {
                                            type = "string"
                                        }

                                        switch (type) {
                                            case "date":
                                                if (content in Map) {
                                                    // date field from Spectrum delivered as map [year:2019, month:5, day:8] => convert
                                                    dateString = content.get("day") + "." + content.get("month") + "." + content.get("year")
                                                    dateContent = new Date().parse("dd.MM.yyyy", dateString)
                                                } else {
                                                    dateContent = content
                                                }
                                                tableRowMapFormInput.put(tableFieldKey, dateContent)
                                                break
                                            case "timestamp":
                                                if (content != null) {
                                                    timestampContent = Date.parse("yyyyMMddHHmmssSSS", content.take(17)).toTimestamp()
                                                    tableRowMapFormInput.put(tableFieldKey, timestampContent.format("yyyy-MM-dd HH:mm:ss.SSS").toString())
                                                } else {
                                                    tableRowMapFormInput.put(tableFieldKey, "")
                                                }
                                                break
                                            case "boolean":
                                                tableRowMapFormInput.put(tableFieldKey, content)
                                                break
                                            case "structure":
                                                break
                                            default: // string
                                                tableRowMapFormInput.put(tableFieldKey, content)
                                        } // switch (type)
                                    } // formEntityData.value.fields.first().each
                                } // End of content contentEntityData field loop
                            } // it.value.nodes.first().each
                            // at the end of each content line add a table row
                            thisField.row(tableRowMapFormInput)

                        } // contentData.each { data ->
                    } // if (flagContentFromFormOutput) {
                    // display table form
                    thisField.width(nodeWidth)
                    thisField.required(nodeRequired)
                    thisField.readonly(nodeReadonly)
                    thisField.add()

                    if (fill) {
                        thisForm.fill()
                    }

                    if (contentData.isEmpty()) {
                        thisForm.newText(it.key +"noData", fieldDefinitions.get("noData").language.get(lang).longtxt).add()
                    }
                    break

                default:

                    try {
                        nodeFields = nodeValue.fields.first()

                        nodeFields.each { field ->
                            //
                            // layout metadata from formMap
                            //
                            try {
                                addRow = field.value.addRow
                                if (addRow == null) {
                                    addRow = false
                                }
                            }
                            catch (Exception ex) {
                                addRow = false // defaulting to false
                            }

                            try {
                                deleteRow = field.value.deleteRow
                                if (deleteRow == null) {
                                    deleteRow = false
                                }
                            }
                            catch (Exception ex) {
                                deleteRow = false // defaulting to false
                            }

                            try {
                                displayLimit = field.value.displayLimit
                                if (displayLimit == null) {
                                    displayLimit = 5
                                }
                            }
                            catch (Exception ex) {
                                displayLimit = 5 // defaulting to false
                            }

                            try {
                                required = field.value.required
                                if (required == null) {
                                    required = false
                                }
                            }
                            catch (Exception ex) {
                                required = false // defaulting to not required
                            }

                            try {
                                fill = field.value.fill
                                if (fill == null) {
                                    fill = false
                                }
                            }
                            catch (Exception ex) {
                                fill = false // defaulting to no fill
                            }

                            try {
                                fieldReadonly = field.value.readonly
                                if (fieldReadonly == null) {
                                    fieldReadonly = nodeReadonly
                                }
                            }
                            catch (Exception ex) {
                                fieldReadonly = nodeReadonly
                            }

                            try {
                                width = ((CharSequence) (field.value).width).toInteger()
                                hasWidth = true
                            }
                            catch (Exception ex) {
                            }

                            try {
                                arealines = ((CharSequence) (field.value).arealines).toInteger()
                            }
                            catch (Exception ex) {
                                arealines = 1 // defaulting to 1 line
                            }

                            try {
                                textarea = field.value.textarea
                                if (textarea == null) {
                                    textarea = false
                                }
                            }
                            catch (Exception ex) {
                                textarea = false
                            }

                            try {
                                listfieldRadio = field.value.radio
                                if (listfieldRadio == null) {
                                    listfieldRadio = false
                                }
                            }
                            catch (Exception ex) {
                                listfieldRadio = false
                            }

                            try {
                                listfieldCheckbox = field.value.checkbox
                                if (listfieldCheckbox == null) {
                                    listfieldCheckbox = false
                                }
                            }
                            catch (Exception ex) {
                                listfieldCheckbox = false
                            }

                            try {
                                listfieldDynamic = field.value.dynamic
                                if (listfieldDynamic == null) {
                                    listfieldDynamic = false
                                }
                            }
                            catch (Exception ex) {
                                listfieldDynamic = false
                            }

                            try {
                                listfieldMulti = field.value.multi
                                if (listfieldMulti == null) {
                                    listfieldMulti = false
                                }
                            }
                            catch (Exception ex) {
                                listfieldMulti = false
                            }

                            //
                            // read metadata from mdmFieldDefinitions about the type of the field
                            //

                            try {
                                type = fieldDefinitions.(field.key).type
                            }
                            catch (Exception ex) {
                                type = "string" // defaulting to string
                            }

                            try {
                                shorttxt = fieldDefinitions.get(field.key).language.get(lang).shorttxt
                            }
                            catch (Exception ex) {
                                shorttxt = field.key // defaulting to technical field name
                            }

                            try {
                                longtxt = fieldDefinitions.get(field.key).language.get(lang).longtxt
                            }
                            catch (Exception ex) {
                                longtxt = field.key // defaulting to technical field name
                            }

                            try {
                                placeholder = fieldDefinitions.get(field.key).language.get(lang).placeholder
                                if (!fieldReadonly) {
                                    hasPlaceholder = true
                                }
                            }
                            catch (Exception ex) {
                            }


                            //
                            // 1. Step: read content from nodeDataMap
                            //
                            content = null
                            try {
                                content = nodeDataMap.get(field.key)
                            }
                            catch (Exception ex) {
                                // no content available
                            }
                            //
                            // 2. Step: if available, overwrite content from outputMap
                            //
                            if (outputMap.get(field.key) != null) {
                                try {
                                    content = outputMap.get(field.key)
                                }
                                catch (Exception ex) {
                                    // no content available
                                }
                            }


                            //
                            // 3. Step: no content available, get content from form map
                            //
                            if (content == null) {
                                try {
                                    if (type == "date") {
                                        def formContentDate = field.value.content.toString()
                                        if (formContentDate == "now") {
                                            content = new Date()
                                        } else {
                                            content = new Date().parse("dd.MM.yyyy", field.value.content)
                                        }
                                    } else {
                                        content = field.value.content
                                    }
                                    /*if (type == "timestamp") {
                                        if (field.value.content == "now") {
                                            content = new Timestamp().format("yyyyMMddHHmmssSSSS").toString()
                                        }
                                    } else {
                                        content = field.value.content
                                    }*/
                                }
                                catch (Exception ex) {
                                    if ((type == "date") || (type == "timestamp")) {
                                        // if type is date null is OK as an initial value
                                    } else {
                                        content = ""
                                    }
                                }
                            }

                            // assure content is not null
                            if (content == null) {
                                if ((type == "date") || (type == "timestamp")) {
                                    // if type is date null is OK as an initial value
                                } else {
                                    content = ""
                                }
                            }
                            switch (type) {
                                case "date":
                                    if (content in Map) {
                                        // date field from Spectrum delivered as map [year:2019, month:5, day:8] => convert
                                        dateString = content.get("day") + "." + content.get("month") + "." + content.get("year")
                                        dateContent = new Date().parse("dd.MM.yyyy", dateString)
                                    } else {
                                        dateContent = content
                                    }

                                    thisField = thisForm.newDate(field.key, dateContent)
                                    thisField.label(shorttxt)
                                    thisField.help(longtxt)
                                    if (hasWidth) {
                                        thisField.width(width)
                                    }
                                    if (hasPlaceholder) {
                                        thisField.placeholder(placeholder)
                                    }
                                    thisField.required(required)
                                    thisField.readonly(fieldReadonly)
                                    thisField.add()
                                    if (fill) {
                                        thisForm.fill()
                                    }
                                    break

                                case "timestamp":
                                    if (content != null) {
                                        timestampContent = Date.parse("yyyyMMddHHmmssSSS", content.take(17)).toTimestamp()
                                        thisField = thisForm.newString(field.key, timestampContent.format("yyyy-MM-dd HH:mm:ss.SSS").toString())
                                    } else {
                                        Date now = new Date()
                                        thisField = thisForm.newString(field.key, now.format("yyyy-MM-dd HH:mm:ss.SSS", TimeZone.getTimeZone('UTC')))
                                    }
                                    thisField.label(shorttxt)
                                    thisField.help(longtxt)
                                    if (hasWidth) {
                                        thisField.width(width)
                                    }
                                    if (hasPlaceholder) {
                                        thisField.placeholder(placeholder)
                                    }
                                    thisField.required(required)
                                    thisField.readonly(fieldReadonly)
                                    if (textarea) {
                                        thisField.textarea(arealines)
                                    }
                                    // form check
                                    thisField.regexp("[0-9]{1,4}-[0-9]{1,2}-[0-9]{1,2} [0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}.[0-9]{1,3}", "yyyy-MM-dd HH:mm:ss.SSS")
                                    thisField.add()
                                    if (fill) {
                                        thisForm.fill()
                                    }
                                    break

                                case "referenceData":
                                    // load reference data from MDM
                                    readReferenceDataMap.I.Row[0].attributeName = field.key.toString()
                                    readReferenceDataMap.I.Row[0].language = lang

                                    // call MDM readReferenceData service
                                    if (readSpectrum) {
                                        spectrum.request("readReferenceData")
                                                .object("json", readReferenceDataMap)
                                                .acceptJSON()
                                                .execute({info, inStream ->
                                                    referenceDataResultsText = inStream.getText(info.contentEncoding)
                                                    contentType = info.getContentType()
                                                })
                                        if (contentType == "application/json") {
                                            // Service returned a json, worked fine
                                            referenceDataResults = new groovy.json.JsonSlurperClassic().parseText(referenceDataResultsText)

                                            referenceDataResultsMapOutput = referenceDataResults.O
                                            referenceDataMap.clear()
                                            for (s in referenceDataResultsMapOutput) {
                                                referenceDataMap.put(s.get("value.key"), s.("metaData.shortTxt"))
                                            }
                                        }
                                        else{
                                            // error happened
                                            logger.error("Error when calling service readReferenceData: " + referenceDataResultsText)
                                        }

                                        if(!listfieldMulti == true) {
                                            // add form list field
                                            thisField = form.newList(field.key, content)
                                                    .values({ list ->
                                                        referenceDataMap.each { entry ->
                                                            list.value(entry.key, entry.value)
                                                        }
                                                    })
                                        }
                                        else {
                                            // add form multiList field
                                            thisField = form.newMultiList(field.key, [])
                                                    .values({ list ->
                                                        referenceDataMap.each { entry ->
                                                            list.value(entry.key, entry.value)
                                                        }
                                                    })
                                        }
                                    } else {
                                        logger.debug("Service readReferenceData will not be be called, readSpectrum == false")
                                        thisField = form.newList(field.key, content)
                                    }
                                    thisField.label(shorttxt)
                                    thisField.help(longtxt)
                                    if (hasWidth) {
                                        thisField.width(width)
                                    }
                                    if (hasPlaceholder) {
                                        thisField.placeholder(placeholder)
                                    }
                                    thisField.required(required)
                                    thisField.readonly(fieldReadonly)
                                    if (listfieldRadio) {
                                        thisField.radio()
                                    }
                                    if (listfieldCheckbox) {
                                        thisField.checkbox()
                                    }
                                    thisField.add()
                                    if (fill) {
                                        thisForm.fill()
                                    }
                                    break

                            /*case "list":
                                thisField = form.newList(field.key, "")
                                        .values({ list ->
                                            content.each { it ->
                                                list.value(it)
                                            }
                                        })
                                thisField.label(shorttxt)
                                thisField.help(longtxt)
                                if (hasWidth) {
                                    thisField.width(width)
                                }
                                if (hasPlaceholder) {
                                    thisField.placeholder(placeholder)
                                }
                                thisField.required(required)
                                thisField.readonly(fieldReadonly)
                                if (listfieldRadio) {
                                    thisField.radio()
                                }
                                if (listfieldCheckbox) {
                                    thisField.checkbox()
                                }
                                if (listfieldDynamic) {
                                    thisField.dynamic()
                                }
                                thisField.add()
                                if (fill) {
                                    thisForm.fill()
                                }
                                break*/


                                case "text":
                                    def message = "<h5>" + shorttxt + "</h5>"
                                    thisField = thisForm.newText(field.key, message)
                                    thisField.markdown()
                                    if (hasWidth) {
                                        thisField.width(width)
                                    }
                                    if (hasPlaceholder) {
                                        thisField.placeholder(placeholder)
                                    }
                                    thisField.add()
                                    if (fill) {
                                        thisForm.fill()
                                    }
                                    break

                                case "markdown":
                                    thisField = thisForm.newText(field.key, content)
                                    thisField.label(shorttxt)
                                    thisField.help(longtxt)
                                    thisField.markdown()
                                    if (hasWidth) {
                                        thisField.width(width)
                                    }
                                    if (hasPlaceholder) {
                                        thisField.placeholder(placeholder)
                                    }
                                    thisField.required(required)
                                    thisField.readonly(fieldReadonly)
                                    if (textarea) {
                                        thisField.textarea(arealines)
                                    }
                                    thisField.add()
                                    if (fill) {
                                        thisForm.fill()
                                    }
                                    break

                                case "boolean":
                                    thisField = thisForm.newBoolean(field.key, content)
                                    thisField.label(shorttxt)
                                    thisField.help(longtxt)
                                    if (width != false) {
                                        thisField.width(width)
                                    }
                                    thisField.required(required)
                                    thisField.readonly(fieldReadonly)
                                    thisField.add()
                                    if (fill) {
                                        thisForm.fill()
                                    }
                                    break

                                default:
                                    thisField = thisForm.newString(field.key, content)
                                    thisField.label(shorttxt)
                                    thisField.help(longtxt)
                                    if (hasWidth) {
                                        thisField.width(width)
                                    }
                                    if (hasPlaceholder) {
                                        thisField.placeholder(placeholder)
                                    }
                                    thisField.required(required)
                                    thisField.readonly(fieldReadonly)
                                    if (textarea) {
                                        thisField.textarea(arealines)
                                    }
                                    thisField.add()
                                    if (fill) {
                                        thisForm.fill()
                                    }
                            }
                        }
                    }
                    catch (Exception ex) {
                        logger.error(ex.toString())
                    }
            }
        }
    }

    return thisForm
}

def addResult2OutputMap(ResultFormDataContextImpl result, Map outputMap) {

    def resultMap = result.asMap()
    resultMap.each { key, value ->
        // add or replace key value pairs in outputMap to be modified
        outputMap.put(key, value)
    }
    return outputMap

}

//--------------------------------------------------------------------------
// Main program
//--------------------------------------------------------------------------

if (load) {
// code is executed when form is loaded
    logger.debug("Start load")

    String langCode = lang
    def actionShorttxt
    def showReadonly

    //
    // get form input
    //
    try {
        inputMap = task.getVariable("formInput")
    }
    catch (Exception ex) {
        // if no content variable exists in Workorder context, inputMap remains empty
    }
    if ((inputMap == "") || (inputMap == null)) {
        inputMap = [:]
    }
    def dataAsJson = JsonOutput.toJson(inputMap)
    logger.debug("formInput as JSON: " + JsonOutput.prettyPrint(dataAsJson.toString()))

    //
    // get form output of former edits
    //
    try {
        outputMap = task.getVariable("formOutput")
    }
    catch (Exception ex) {
        // if no content variable exists in Workorder context, outputMap remains empty
    }
    if ((outputMap == "") || (outputMap == null)) {
        outputMap = [:]
    }
    dataAsJson = JsonOutput.toJson(outputMap)
    logger.debug("formOutput on load as JSON: " + JsonOutput.prettyPrint(dataAsJson.toString()))

    //
    // create form
    //

    // set action buttons for form
    try {
        buttonTrue = fieldDefinitions.buttonTrue.language.get(langCode).shorttxt
    }
    catch (Exception ex) {
        buttonTrue = "OK" // defaulting to OK
    }
    try {
        buttonFalse = fieldDefinitions.buttonFalse.language.get(langCode).shorttxt
    }
    catch (Exception ex) {
        buttonFalse = "Cancel" // defaulting to Cancel
    }


    switch (action) {
        case "string":
            // read String definition
            actionMap = task.getVariable("formButton")
            if (!(actionMap in Map)) {
                // if action definition cannot be found use default
                thisForm = form.actionBoolean("formAction")
                        .action(buttonFalse, false)
                        .action(buttonTrue, true)
                        .set()
                logger.error("Variable formButton empty or not found, default Buttons are used")
                break
            }

            dataAsJson = JsonOutput.toJson(actionMap)

            // prepare form actions

            thisFormActionBuilder = form.actionString("formAction")

            actionMap.action.first().each { akey, avalue ->

                try {
                    actionShorttxt = avalue.language.get(langCode).shorttxt
                }
                catch (Exception ex) {
                    actionShorttxt = akey
                }

                showReadonly = true
                try {
                    showReadonly = avalue.showReadonly
                }
                catch (Exception ex) {
                    showReadonly = true
                }

                if (readonly) {
                    if (showReadonly == false) {
                        // do not show button
                    } else {
                        thisFormActionBuilder.action(actionShorttxt, akey).set()
                    }
                } else {
                    thisFormActionBuilder.action(actionShorttxt, akey).set()
                }
            }
            thisForm = thisFormActionBuilder.set()
            break

        default:
            thisForm = form.actionBoolean("formAction")
                    .action(buttonFalse, false)
                    .action(buttonTrue, true)
                    .set()
            break
    }

    // add fields to form

    thisForm = parseFormMap(thisForm, formMap, inputMap, outputMap, readonly)

    logger.debug("End load")
}


if (save) {
    logger.debug("Start save")
    // Save action chosen by user
    task.setVariable("formAction", result.formAction)

    dataAsJson = JsonOutput.toJson(outputMap)
    logger.debug("formAction on save: " + result.formAction)

    // modify <formTemplate>Output task variable from form results
    // has to be modify/update not create, as result data can be modified during multiple editings before save
    try {
        // try to read output variable that may exist because of former edits
        outputMap = task.getVariable("formOutput")
    }
    catch (Exception ex) {
    }
    if ((outputMap == "") || (outputMap == null)) {
        outputMap = [:]
    }
    addResult2OutputMap(result, outputMap)
    // save modified <formTemplate>Output
    task.setVariable("formOutput", outputMap)

    def mapAsJson = JsonOutput.toJson(outputMap)
    logger.debug("formOutput on save as Json: " + JsonOutput.prettyPrint(mapAsJson.toString()))

    logger.debug("End save")
}

logger.debug("End generateForm")