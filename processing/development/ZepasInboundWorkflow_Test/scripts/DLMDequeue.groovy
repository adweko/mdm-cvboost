logger.info("Start DLMDequeue")
try {
// Get BusinessPartnerID
    def businessPartnerID = task.getVariable("KUPE_LAUFNUMMER")

// Get ProcessPID = WorkorderID
    def processID = task.getVariable("system.workorder.id")

// Get DLMTestMode
    def DLMTestMode = task.getVariable("DLMTestMode")

// DLMDequeue
    boolean DLMDequeue = functions.DLMQueueManagement.dequeue(businessPartnerID, processID, "zepasCalls", DLMTestMode)
    if (!DLMDequeue) {
        throw new Exception("DLMQueueManagement: dequeue Error")
    }

    def MDMPartnerID = task.getVariable("MDMPartnerID")
    if (MDMPartnerID != null){
        boolean DLMDequeueUpdate = functions.DLMQueueManagement.dequeue(MDMPartnerID, processID, DLMTestMode)
        if (!DLMDequeueUpdate) {
            throw new Exception("DLMQueueManagement: dequeue Error")
        }
    }

}
catch (Exception ex) {
    logger.error(ex.toString())
    task.setVariable("error", true)
    task.setVariable("update_flag", false)
    task.setVariable("error_message", ex.toString())
}
logger.info("End DLMDequeue")