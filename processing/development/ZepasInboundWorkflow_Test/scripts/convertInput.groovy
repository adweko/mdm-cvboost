import groovy.json.JsonOutput
import groovy.json.JsonSlurperClassic

logger.info("Start convertInput")

def contentType
Integer loopcounter
def status
def headers

// Map to convert topic to ZEPAS table name
Map topic2Table = ["CDC.ZDKUB000.Z00.TKUPE_PERSON"      : "TKUPE_PERSON",
                   "CDC.ZDKUB000.Z00.TKUPR_PRIVATPERSON": "TKUPR_PRIVATPERSON",
                   "CDC.ZDKUB000.Z00.TKUUN_UNTERNEHMUNG": "TKUUN_UNTERNEHMUNG",
                   "CDC.ZDKUB000.Z00.TKUAD_ADRESSE"     : "TKUAD_ADRESSE",
                   "CDC.ZDKUB000.Z00.TKURO_ROLLE"       : "TKURO_ROLLE",
                   "CDC.ZDKUB000.Z00.TKUKO_KOMMELEMENT" : "TKUKO_KOMMELEMENT",
                   "CDC.ZDKUB000.Z00.TKUZV_ZAHLUNGSVERB": "TKUZV_ZAHLUNGSVERB",
                   "CDC.ZDKUB000.Z00.TKUMI_MITARBEITER" : "TKUMI_MITARBEITER",
                   "CDC.ZDKUB000.Z00.TKUPF_FMAREGISTER" : "TKUPF_FMAREGISTER",
                   "CDC.ZDKUB000.Z00.TKUPI_PEZUSATZINFO": "TKUPI_PEZUSATZINFO",
                   "CDC.ZDKUB000.Z00.TKUCP_CODEZURPERS" : "TKUCP_CODEZURPERS",
                   "CDC.ZDKUB000.Z00.TKUPZ_PERSZUS"     : "TKUPZ_PERSZUS",
                   "CDC.ZDKUB000.Z00.TKUKB_KOELVERWBEZ" : "TKUKB_KOELVERWBEZ"]

// load service definition for inboundTrafoZepasFull
String service = "inboundTrafoZepasFull"
def serviceDefinition = resources.get("deployment:/json/service/${service}.json").load("json")

def useTestfile = task.getVariable("useTestfile")

// load transaction file
def transaction
if (useTestfile) {
    transaction = resources.get("deployment:/test/transaction.json").load "json"
} else {
    transaction = resources.get("workorder:/transaction.json").load("json")
}

// Save KUPE_LAUFNUMMER
task.setVariable("KUPE_LAUFNUMMER", transaction.first().kupeLaufnummer.toString())

// Individual ArrayList object for each ZEPAS table
ArrayList tkupe_person = []
ArrayList tkupr_privatperson = []
ArrayList tkuun_unternehmung = []
ArrayList tkuad_addresse = []
ArrayList tkuro_rolle = []
ArrayList tkuko_kommelement = []
ArrayList tkuzv_zahlungsverb = []
ArrayList tkumi_mitarbeiter = []
ArrayList tkupf_fmaregister = []
ArrayList tkupi_pezusatzinfo = []
ArrayList tkucp_codezurpers = []
ArrayList tkupz_perszus = []
ArrayList tkukb_koelverwbez = []


// service components
Map inboundTrafoZepasFull = [:]
Map inboundTrafoZepasNaturalPerson = [:]
Map inboundTrafoZepasLegalPerson = [:]
Map inboundTrafoZepasChannel = [:]
Map inboundTrafoZepasLocation = [:]
Map inboundTrafoZepasUnderwriting = [:]
Map inboundTrafoZepasDuplicates = [:]

Map I = [:]
Map INaturalPerson = [:]
Map ILegalPerson = [:]
Map IChannel = [:]
Map ILocation = [:]
Map IUnderwriting = [:]
Map IDuplicates = [:]

ArrayList RowList = []
ArrayList RowListNaturalPerson = []
ArrayList RowListLegalPerson = []
ArrayList RowListChannel = []
ArrayList RowListLocation = []
ArrayList RowListUnderwriting = []
ArrayList RowListDuplicates = []

// process each message
transaction.each { message ->
    try {
        Map value = message.get("value")
        if (value != null) {
            // we process only messages with a value key
            // get table name
            String zepasTable = topic2Table.get(message.get("topic").toUpperCase())
            // create Map with fieldName, fieldValue pairs (a line of a zepas table)
            Map zepasTableMap = serviceDefinition.get(zepasTable).first()
            zepasTableMap.each { fieldName, fieldValue ->
                try {

                    if (value.get(fieldName) != null) {
                        fieldValue = value.get(fieldName).toString().trim()
                    } else {
                        fieldValue = null
                    }
                }
                catch (Exception ex) {
                    // key not found in values - remove from table
                    logger.info("Exception: fieldname: " + fieldName + ": " + fieldValue + " not found")
                }
                finally {
                }
                zepasTableMap.put(fieldName, fieldValue)
            }
            // fill matching table
            def cloneZepasTableMap = [:]
            cloneZepasTableMap << zepasTableMap
            switch (zepasTable) {
                case "TKUPE_PERSON":
                    tkupe_person.add(cloneZepasTableMap)
                    break
                case "TKUPR_PRIVATPERSON":
                    tkupr_privatperson.add(cloneZepasTableMap)
                    break
                case "TKUUN_UNTERNEHMUNG":
                    tkuun_unternehmung.add(cloneZepasTableMap)
                    break
                case "TKUAD_ADRESSE":
                    tkuad_addresse.add(cloneZepasTableMap)
                    break
                case "TKURO_ROLLE":
                    tkuro_rolle.add(cloneZepasTableMap)
                    break
                case "TKUKO_KOMMELEMENT":
                    tkuko_kommelement.add(cloneZepasTableMap)
                    break
                case "TKUZV_ZAHLUNGSVERB":
                    tkuzv_zahlungsverb.add(cloneZepasTableMap)
                    break
                case "TKUMI_MITARBEITER":
                    tkumi_mitarbeiter.add(cloneZepasTableMap)
                    break
                case "TKUPF_FMAREGISTER":
                    tkupf_fmaregister.add(cloneZepasTableMap)
                    break
                case "TKUPI_PEZUSATZINFO":
                    tkupi_pezusatzinfo.add(cloneZepasTableMap)
                    break
                case "TKUCP_CODEZURPERS":
                    tkucp_codezurpers.add(cloneZepasTableMap)
                    break
                case "TKUPZ_PERSZUS":
                    tkupz_perszus.add(cloneZepasTableMap)
                    break
                case "TKUKB_KOELVERWBEZ":
                    tkukb_koelverwbez.add(cloneZepasTableMap)
                    break
            }
        }
    }
    catch (Exception ex) {
        // no value => Deletion => is ignored at the moment
    }
}

try {

    // Update MDM Transaction Status, update IDNumber and IDType
    functions.MDMTransactionStatus.updateIDNumber(task.getVariable("system.workorder.id"), workOrder.owner, "CH_ZEPAS", task.getVariable("KUPE_LAUFNUMMER"))

    // try to read MDMPartnerID
    def serviceResultsReadMDMPartnerID
    String serviceResultText
    String serviceReadMDMPartnerID = "readMDMPartnerIDfromIdentificationNumber"
    Map serviceMapReadMDMPartnerID = [:]
    Map ReadMDMPartnerID = [:]
    ArrayList RowListReadMDMPartnerID = []
    ReadMDMPartnerID.put("IDNumber", task.getVariable("KUPE_LAUFNUMMER"))
    ReadMDMPartnerID.put("IDType", "1")
    RowListReadMDMPartnerID.add(ReadMDMPartnerID)
    // add all rows to "Row"
    Map IReadMDMPartnerID = [:]
    IReadMDMPartnerID.put("Row", RowListReadMDMPartnerID)
    // add all rows to "I" to complete service Map
    serviceMapReadMDMPartnerID.put("I", IReadMDMPartnerID)

    // Call spectrum
    contentType = ""
    loopcounter = 0
    while (contentType != "application/json") {
        try {
            spectrum.request(serviceReadMDMPartnerID)
                    .object("json", serviceMapReadMDMPartnerID)
                    .acceptJSON()
                    .execute({ info, inStream ->
                        status = info.status
                        serviceResultText = inStream.getText(info.contentEncoding)
                        contentType = info.getContentType()
                    })
        }
        catch (Exception ex) {
            logger.error("Error calling Spectrum service (" + serviceReadMDMPartnerID + "): " + ex.toString())
            loopcounter++
            if (loopcounter == 5) {
                throw new Exception("Error calling Spectrum service (" + serviceReadMDMPartnerID + "): Retry Limit of 5 reached")
            }
            continue
        }
        if (contentType == "application/json") {
            // Service returned a json, so return a Map object
            serviceResultsReadMDMPartnerID = new JsonSlurperClassic().parseText(serviceResultText)

        } else {
            logger.error("Error calling Spectrum service (" + serviceReadMDMPartnerID + "): " + status + ", " + serviceResultText)
            throw new Exception("Error calling Spectrum service (" + serviceReadMDMPartnerID + "): " + status + ", " + serviceResultText)
        }
    }

    def MDMPartnerID = serviceResultsReadMDMPartnerID.O.first().get("MDMPartnerID")
    def partnerType = serviceResultsReadMDMPartnerID.O.first().get("partnerType")
    logger.info("MDM PartnerID: " + MDMPartnerID)
    logger.info("MDM Partner Typ: " + partnerType)
    task.setVariable("MDMPartnerID", MDMPartnerID)
    if (MDMPartnerID != null) {
        // Step 4: Update MDM Status, update MDMPartnerID Status
        functions.MDMTransactionStatus.updateMDMPartnerID(task.getVariable("system.workorder.id"), workorder.owner, MDMPartnerID)
    }

    // fill list per table and add table list as service row
    Map RowNaturalPerson = [:]
    Map RowLegalPerson = [:]
    Map RowChannel = [:]
    Map RowLocation = [:]
    Map RowUnderwriting = [:]
    Map RowDuplicates = [:]
    Map Row = [:]
    if (!tkupe_person.isEmpty()) {

        if (tkupe_person.first().PERSONENKENNZ.toString() == "P") {
            RowNaturalPerson.put("TKUPE_PERSON", tkupe_person)
        }
        if (tkupe_person.first().PERSONENKENNZ.toString() == "U") {
            RowLegalPerson.put("TKUPE_PERSON", tkupe_person)
        }
    }
    if (!tkupr_privatperson.isEmpty()) {
        RowNaturalPerson.put("TKUPR_PRIVATPERSON", tkupr_privatperson)
    }
    if (!tkuun_unternehmung.isEmpty()) {
        RowLegalPerson.put("TKUUN_UNTERNEHMUNG", tkuun_unternehmung)
    }
    if (!tkuad_addresse.isEmpty()) {
        RowLocation.put("TKUAD_ADRESSE", tkuad_addresse)
    }
    if (!tkuro_rolle.isEmpty()) {
        Row.put("TKURO_ROLLE", tkuro_rolle)
    }
    if (!tkuko_kommelement.isEmpty()) {
        RowChannel.put("TKUKO_KOMMELEMENT", tkuko_kommelement)
    }
    if (!tkukb_koelverwbez.isEmpty()) {
        RowChannel.put("TKUKB_KOELVERWBEZ", tkukb_koelverwbez)
    }
    if (!tkuzv_zahlungsverb.isEmpty()) {
        Row.put("TKUZV_ZAHLUNGSVERB", tkuzv_zahlungsverb)
    }
    if (!tkumi_mitarbeiter.isEmpty()) {
        RowNaturalPerson.put("TKUMI_MITARBEITER", tkumi_mitarbeiter)
    }
    if (!tkupf_fmaregister.isEmpty()) {
        RowLegalPerson.put("TKUPF_FMAREGISTER", tkupf_fmaregister)
    }

    // Decide natural person or legal person
    if (!tkupi_pezusatzinfo.isEmpty()) {
        switch (partnerType) {
            case "NaturalPerson":
                RowNaturalPerson.put("TKUPI_PEZUSATZINFO", tkupi_pezusatzinfo)
                break
            case "LegalPerson":
                RowLegalPerson.put("TKUPI_PEZUSATZINFO", tkupi_pezusatzinfo)
                break
        }
    }
    if (!tkucp_codezurpers.isEmpty()) {
        RowUnderwriting.put("TKUCP_CODEZURPERS", tkucp_codezurpers)
    }
    if (!tkupz_perszus.isEmpty()) {
        RowDuplicates.put("TKUPZ_PERSZUS", tkupz_perszus)
        logger.info("tkupz_perszus: " + tkupz_perszus)
        logger.info("RowDuplicates: " + RowDuplicates)
    }

    if (!Row.isEmpty()) {
        RowList.add(Row)
        // add all rows to "Row"
        I.put("Row", RowList)
        // add all rows to "I" to complete service Map
        inboundTrafoZepasFull.put("I", I)

        def mapAsJson = JsonOutput.toJson(inboundTrafoZepasFull)
        // store service Map in workorder variable
        task.setVariable("jsonMap", inboundTrafoZepasFull)
    }

    if (!RowNaturalPerson.isEmpty()) {
        RowListNaturalPerson.add(RowNaturalPerson)
        // add all rows to "Row"
        INaturalPerson.put("Row", RowListNaturalPerson)
        // add all rows to "I" to complete service Map
        inboundTrafoZepasNaturalPerson.put("I", INaturalPerson)

        def mapAsJsonNaturalPerson = JsonOutput.toJson(inboundTrafoZepasNaturalPerson)
        // store service Map in workorder variable
        task.setVariable("jsonMapNaturalPerson", inboundTrafoZepasNaturalPerson)
    }

    if (!RowLegalPerson.isEmpty()) {
        RowListLegalPerson.add(RowLegalPerson)
        // add all rows to "Row"
        ILegalPerson.put("Row", RowListLegalPerson)
        // add all rows to "I" to complete service Map
        inboundTrafoZepasLegalPerson.put("I", ILegalPerson)

        def mapAsJsonLegalPerson = JsonOutput.toJson(inboundTrafoZepasLegalPerson)
        // store service Map in workorder variable
        task.setVariable("jsonMapLegalPerson", inboundTrafoZepasLegalPerson)
    }

    if (!RowChannel.isEmpty()) {
        RowListChannel.add(RowChannel)
        // add all rows to "Row"
        IChannel.put("Row", RowListChannel)
        // add all rows to "I" to complete service Map
        inboundTrafoZepasChannel.put("I", IChannel)

        def mapAsJsonChannel = JsonOutput.toJson(inboundTrafoZepasChannel)
        // store service Map in workorder variable
        task.setVariable("jsonMapChannel", inboundTrafoZepasChannel)
    }

    if (!RowLocation.isEmpty()) {
        RowListLocation.add(RowLocation)
        // add all rows to "Row"
        ILocation.put("Row", RowListLocation)
        // add all rows to "I" to complete service Map
        inboundTrafoZepasLocation.put("I", ILocation)

        def mapAsJsonLocation = JsonOutput.toJson(inboundTrafoZepasLocation)
        // store service Map in workorder variable
        task.setVariable("jsonMapLocation", inboundTrafoZepasLocation)
    }

    if (!RowUnderwriting.isEmpty()) {
        RowListUnderwriting.add(RowUnderwriting)
        // add all rows to "Row"
        IUnderwriting.put("Row", RowListUnderwriting)
        // add all rows to "I" to complete service Map
        inboundTrafoZepasUnderwriting.put("I", IUnderwriting)

        def mapAsJsonUnderwriting = JsonOutput.toJson(inboundTrafoZepasUnderwriting)
        // store service Map in workorder variable
        task.setVariable("jsonMapUnderwriting", inboundTrafoZepasUnderwriting)
    }

    if (!RowDuplicates.isEmpty()) {
        RowListDuplicates.add(RowDuplicates)
        // add all rows to "Row"
        IDuplicates.put("Row", RowListDuplicates)
        // add all rows to "I" to complete service Map
        inboundTrafoZepasDuplicates.put("I", IDuplicates)

        def mapAsJsonDuplicates = JsonOutput.toJson(inboundTrafoZepasDuplicates)
        // store service Map in workorder variable
        task.setVariable("jsonMapDuplicates", inboundTrafoZepasDuplicates)
    }

    task.setVariable("error", false)
}
catch (Exception ex) {
    logger.error(ex.toString())
    task.setVariable("error", true)
    task.setVariable("update_flag", false)
    task.setVariable("error_message", ex.toString())
}

logger.info("End convertInput")