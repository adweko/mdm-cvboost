logger.info("Start DLMCheck")
try {
    // Get BusinessPartnerID
    def businessPartnerID = task.getVariable("KUPE_LAUFNUMMER")

    // Get ProcessPID = WorkorderID
    def processID = task.getVariable("system.workorder.id")

    // Get DLMTestMode
    def DLMTestMode = task.getVariable("DLMTestMode")

    // DLMCheck
    boolean DLMCheck = functions.DLMQueueManagement.check(businessPartnerID, processID, "zepasCalls", DLMTestMode)

    def MDMPartnerID = task.getVariable("MDMPartnerID")
    if (MDMPartnerID != null){
        boolean DLMCheckUpdate = functions.DLMQueueManagement.check(MDMPartnerID, processID, DLMTestMode)
        if (!DLMCheckUpdate){
            DLMCheck = false
        }
    }

    task.setVariable("process", DLMCheck)

    task.setVariable("error", false)
}
catch (Exception ex) {
    consoleLog("ERROR", ex.toString())
    task.setVariable("error", true)
    task.setVariable("update_flag", false)
    task.setVariable("error_message", ex.toString())
}

logger.info("End DLMCheck")