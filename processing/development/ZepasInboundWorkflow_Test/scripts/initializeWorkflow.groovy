logger.info("Start initializeWorkflow")

task.setVariable("error", false)

try {
// Use Kafka true/false
    task.setVariable("useKafka", true)
    logger.info("useKafka: " + task.getVariable("useKafka"))

// Use Spectrum true/false
    task.setVariable("useSpectrum", true)
    logger.info("useSpectrum: " + task.getVariable("useSpectrum"))

// use testfile
    task.setVariable("useTestfile", true)
    logger.info("useTestfile: " + task.getVariable("useTestfile"))

// set Distributed Lock Manager DLM test mode usage to true or false
    task.setVariable("DLMTestMode", true)
    logger.info("DLMTestMode: " + task.getVariable("DLMTestMode"))

// Create MDM Transaction Status
    functions.MDMTransactionStatus.createTransactionStatus(task.getVariable("system.workorder.id"), workOrder.creator, workOrder.owner)

}
catch (Exception ex) {
    log("ERROR", ex.toString())
    task.setVariable("error", true)
    task.setVariable("update_flag", false)
    task.setVariable("error_message", ex.toString())
}

logger.info("End initializeWorkflow")