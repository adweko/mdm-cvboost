logger.debug("Start getLockData")

try {

    Map DLMQueueManagementMap = [:]

// get zepasCalls Semaophore
    int zepasCallsAvailablePermits = functions.DLMQueueManagement.availablePermits("zepasCalls")
    DLMQueueManagementMap.put("zepasPermitsInitial", "${application.getProperty("mdm.zepasPermits")}")
    DLMQueueManagementMap.put("zepasPermitsAvailable", zepasCallsAvailablePermits.toString())
    DLMQueueManagementMap.put("zepasPermitsModify", "10")

// get syriusCalls Semaophore
    int syriusCallsAvailablePermits = functions.DLMQueueManagement.availablePermits("syriusCalls")
    DLMQueueManagementMap.put("syriusPermitsInitial", "${application.getProperty("mdm.syriusPermits")}")
    DLMQueueManagementMap.put("syriusPermitsAvailable", syriusCallsAvailablePermits.toString())
    DLMQueueManagementMap.put("syriusPermitsModify", "10")

// get lockMap

    List lockList = functions.DLMQueueManagement.getLockList()


    DLMQueueManagementMap.put("lockMap", lockList)

    Map DLMQueueManagementInput = [:]
    DLMQueueManagementInput.put("DLMQueueManagement", [DLMQueueManagementMap])

    logger.debug("DLMQueueManagementInput: " + DLMQueueManagementInput.toString())

    task.setVariable("DLMQueueManagementInput", DLMQueueManagementInput)

}
catch (Exception ex) {

    logger.error(ex.toString())

}
finally {

}
logger.debug("End getLockData")