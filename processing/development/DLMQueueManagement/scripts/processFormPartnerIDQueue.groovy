logger.debug("Start processFormPartnerIDQueue")

// get form output
formOutput = task.getVariable("partnerIDQueueOutput")
formAction = task.getVariable("formAction")
def processID

switch (formAction) {
    case "back":
        // go back to first screen
        break

    case "workorder":
        // get selected entry
        Map selectedEntry = [:]
        try {
            // go to selection
            ArrayList list = formOutput.partnerIDQueue
            list.each { line ->
                if (line.selector == true) {
                    processID = line.processID
                }
            }
        }
        catch (Exception ex) {
        }

        task.setVariable("processID", processID)

        logger.debug("Selected processID: " + processID)

        break

    case "delete":
        // delete processID

        // get selected entry
        Map selectedEntry = [:]
        try {
            // go to selection
            ArrayList list = formOutput.partnerIDQueue
            list.each { line ->
                if (line.selector == true) {
                    processID = line.processID
                }
            }
        }
        catch (Exception ex) {
        }

        logger.debug("Selected processID: " + processID)

        // if nothing is selected, set formAction to "refresh"
        if (processID == null) {
            task.setVariable("formAction", "back")
        } else {
            def businessPartnerID = task.getVariable("partnerID")

            // DLMDequeue
            boolean DLMDequeue = functions.DLMQueueManagement.dequeue(businessPartnerID, processID,
            "zepasCalls" , false )

            // Get processIDMap
            Map processIDMap = functions.DLMQueueManagement.getProcessIDMap(businessPartnerID, false)

            // create formInput
            List partnerIDQueueList = []

            processIDMap.each { key, value ->
                Map tabline = [:]
                tabline.put("processID", key)
                tabline.put("BITEMPMDMTimestamp", value.format("yyyyMMddHHmmssSSS", TimeZone.getTimeZone("UTC")))
                // get process state
                tabline.put("processIDStatus", "No State Found")
                com.compoverso.dmp.extension.core.connector.service.jdbc.builder.SelectBuilderImpl select = jdbc.select() // creates new select for internal database
                        .sql("select STATUS_ from WORK_ORDERS where ID_ like :id") // sets the query with a named parameter
                        .parameter("id", "%" + key + "%") // sets the value for the named parameter
                select.query { row ->
                    tabline.put("processIDStatus", row.STATUS_)
                }

                partnerIDQueueList.add(tabline)
            }

            Map partnerIDQueueInput = [:]
            partnerIDQueueInput.put("partnerIDQueue", partnerIDQueueList)
            partnerIDQueueInput.put("partnerID", partnerID)

            logger.debug("partnerIDQueueInput: " + partnerIDQueueInput.toString())
            task.setVariable("partnerIDQueueInput", partnerIDQueueInput)
        }
        break
}

logger.debug("End processFormPartnerIDQueue")