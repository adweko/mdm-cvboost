logger.debug("Start initializeWorkflow")

// use testfile
task.setVariable("useTestfile", false)
logger.debug( "useTestfile: " + task.getVariable("useTestfile"))

// set locking method "hazelcast" or "elasticsearch"
task.setVariable("lockMethod", "hazelcast")

task.setVariable("hazelcasturl", "${application.getProperty("mdm.hazelcasturl")}")
logger.debug("hazelcasturl: ${application.getProperty("mdm.hazelcasturl")}")
task.setVariable("hazelcastname", "${application.getProperty("mdm.hazelcastname")}")
logger.debug("hazelcastname: ${application.getProperty("mdm.hazelcastname")}")
task.setVariable("hazelcastpw", "${application.getProperty("mdm.hazelcastpw")}")
logger.debug("hazelcastpw: ${application.getProperty("mdm.hazelcastpw")}")

logger.debug("End initializeWorkflow")