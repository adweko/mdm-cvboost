import java.text.SimpleDateFormat
import java.text.DateFormat
import java.util.Date

logger.debug("Start processFormDLMQueueManagement")

// get form output
formOutput = task.getVariable("DLMQueueManagementOutput")
formAction = task.getVariable("formAction")
def partnerID
Map processIDMap = [:]

try {

    switch (formAction) {
        case "exit":
            // exit workorder
            break

        case "refresh":
            // refresh display
            break

        case "forceUnlock":
            // force unlock of a partnerID

            // get selected partnerID
            Map selectedEntry = [:]
            try {
                // go to selection
                ArrayList list = formOutput.lockMap
                list.each { line ->
                    if (line.selector == true) {
                        partnerID = line.partnerID
                    }
                }
            }
            catch (Exception ex) {
            }

            logger.debug("Selected partnerID: " + partnerID)

            // if nothing is selected, set formAction to "refresh"
            if (partnerID == null) {
                task.setVariable("formAction", "refresh")
            } else {
                task.setVariable("formAction", "refresh")
                task.setVariable("partnerID", partnerID)
                // forceUnlock
                boolean forceUnlock = functions.DLMQueueManagement.forceUnlock(partnerID,
                false )
            }
            break

        case "clearLockList":
            // clear Lock map

            logger.debug("Clearing Queue")
            boolean clearLockMap = functions.DLMQueueManagement.clearLockList()
            task.setVariable("formAction", "refresh")
            break

        case "clearPermitsMap":
            // clear Ressources

            logger.debug("Clearing Ressources")
            boolean clearPermitsMap = functions.DLMQueueManagement.clearPermitsMap()
            task.setVariable("formAction", "refresh")
            break

        case "modifyZepasPermits":
            // rise ZepasPermits

            logger.debug("Modify semaphore for zepasCalls")
            boolean riseZepasPermits = functions.DLMQueueManagement.modifyPermits("zepasCalls", formOutput.get("zepasPermitsModify").toInteger() )

            task.setVariable("formAction", "refresh")
            break

        case "modifySyriusPermits":
            // lower SyriusPermits

            logger.debug("Modify semaphore for syriusCalls")
            boolean lowerSyriusPermits = functions.DLMQueueManagement.modifyPermits("syriusCalls", formOutput.get("syriusPermitsModify").toInteger() )

            task.setVariable("formAction", "refresh")
            break

        case "detailqueuePartnerID":
            // get selected partnerID
            Map selectedEntry = [:]
            try {
                // go to selection
                ArrayList list = formOutput.lockMap
                list.each { line ->
                    if (line.selector == true) {
                        partnerID = line.partnerID
                    }
                }
            }
            catch (Exception ex) {
            }

            logger.debug("Selected partnerID: " + partnerID)

            // if nothing is selected, set formAction to "refresh"
            if (partnerID == null) {
                task.setVariable("formAction", "refresh")
            } else {
                task.setVariable("partnerID", partnerID)
                // else read details for partnerID and prepare formInput for form partnerIDQueue


                // Get processIDMap
                processIDMap = functions.DLMQueueManagement.getProcessIDMap(partnerID, false)
                logger.debug("processIDMap: " + processIDMap.toString())
                // create formInput
                List partnerIDQueueList = []

                processIDMap.each { key, value ->
                    Map tabline = [:]
                    tabline.put("processID", key)
                    tabline.put("BITEMPMDMTimestamp", value.format("yyyyMMddHHmmssSSS", TimeZone.getTimeZone("UTC")))

                    // get process state
                    com.compoverso.dmp.extension.core.connector.service.jdbc.builder.SelectBuilderImpl select = jdbc.select() // creates new select for internal database
                            .sql("select STATUS_ from WORK_ORDERS where ID_ like :id") // sets the query with a named parameter
                            .parameter("id", "%" + key + "%") // sets the value for the named parameter
                    select.query { row ->
                        tabline.put("processIDStatus", row.STATUS_)
                    }

                    partnerIDQueueList.add(tabline)
                }

                Map partnerIDQueueInput = [:]
                partnerIDQueueInput.put("partnerIDQueue", partnerIDQueueList)
                partnerIDQueueInput.put("partnerID", partnerID)

                logger.debug("partnerIDQueueInput: " + partnerIDQueueInput.toString())
                task.setVariable("partnerIDQueueInput", partnerIDQueueInput)
            }
            break
    }

}
catch (Exception ex) {
    logger.error(ex.toString())
}
finally {

}
logger.debug("End processFormDLMQueueManagement")




