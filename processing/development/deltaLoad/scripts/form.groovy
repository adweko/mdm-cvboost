if(load) {
// get all variables for current process
    def variables = task.getVariables()
// create form
    form.actionBoolean("approved")
                .action("Start DeltaLoad", true)
                .set()
            .newString("queryTimestampString", variables["queryTimestampString"])
                .label("Enter Query Timestamp [yyyyMMddHHmmssSSS or yyyy-MM-dd HH:mm:ss.SSS]")
                .required()
                .add()
}

if(save) {
    // write back field values
    task.setVariable("queryTimestampString", result.queryTimestampString.replaceAll("[-:. ]", ""))
}
