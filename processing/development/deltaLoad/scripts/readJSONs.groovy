import groovy.json.JsonOutput

logger.info("------------------readJSONs START---------------")

Date queryTimestamp = Date.parse( "yyyyMMddHHmmssSSS", task.getVariable("queryTimestampString") )

com.compoverso.dmp.extension.core.connector.service.jdbc.builder.SelectBuilderImpl select = jdbc.select() // select for internal database
        .sql("SELECT * FROM MDM_TRANSACTION_STATUS WHERE TRANSACTION_HAS_UPDATE_ = 1 AND WORKORDER_START_TIME_ >= :queryTimestamp")
        .parameter("queryTimestamp", queryTimestamp)

List cleanedInboundFiles = []
List cleanedStateFiles = []
List cleanedChangeFiles = []

select.query { row ->
    datapathInbound = row.get("TRANSACTION_DATAPATH_")
    logger.info("datapathInbound: " + datapathInbound)
    datapathOutboundChange = row.get("CHANGE_OUTBOUND_DATAPATH_")
    logger.info("datapathOutboundChange: " + datapathOutboundChange)
    datapathOutboundState = row.get("STATE_OUTBOUND_DATAPATH_")
    logger.info("datapathOutboundState: " + datapathOutboundState)

	if (datapathOutboundChange) {
		cleanedChangeFiles.add(datapathOutboundChange)
	}
	
	if (datapathOutboundState) {
    	cleanedStateFiles.add(datapathOutboundState)
	}

	if (datapathInbound) {
	    cleanedInboundFiles.add(resources.get(datapathInbound).load("json"))
	}

}

logger.info("cleanedInboundFiles: " + cleanedInboundFiles)
logger.info("cleanedChangeFiles: " + cleanedChangeFiles)
logger.info("cleanedStateFiles: " + cleanedStateFiles)

def extractKafkaKey(String filepath, String topicType) {
    def fileJSON = resources.get(filepath).load("json")

    Map isBusinessPartner

    if ( topicType == "state" ) {
        isBusinessPartner = fileJSON["BusinessPartnerID"][0]["Versions"][0]["isBusinessPartner"][0]
    } else {
        isBusinessPartner = fileJSON["BusinessPartnerID"][0]["isBusinessPartner"][0]
    }

    isBusinessPartner.remove("BusinessPartner")
    isBusinessPartner.remove("TECHInternalPID")

    isBusinessPartner.put( "TECHMDMPartnerID", fileJSON["BusinessPartnerID"][0]["TECHMDMPartnerID"].toString() )

    def isBusinessPartnerPretty = JsonOutput.prettyPrint(JsonOutput.toJson(isBusinessPartner).toString())
 
    task.setVariable("kafka_key", isBusinessPartnerPretty.toString())

    return isBusinessPartnerPretty
}

for (file in cleanedStateFiles) {
    String kafkaKey = extractKafkaKey(file.toString(), "state")

    messaging.message("ch-mdm.state.all")
            .source(file.toString())
            .property("key", kafkaKey)
            .send()
}

logger.info("------------------files written to State Kafka---------------")

for (file in cleanedChangeFiles) {
    String kafkaKey = extractKafkaKey(file.toString(), "change")

    messaging.message("ch-mdm.change.all")
            .source(file.toString())
            .property("key", kafkaKey)
            .send()
}

logger.info("------------------files written to Change Kafka---------------")

