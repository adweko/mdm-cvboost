import groovy.json.JsonOutput

logger.info("------------------readJSONs START---------------")

Long queryTimestamp = task.getVariable("queryTimestampString").toLong()
Long currentTimestamp = new Date().format( 'yyyyMMddmmssSSS' ).toLong()

Date queryDate = Date.parse("yyyyMMddmmssSSS", queryTimestamp.toString() ).clearTime()
Date currentDate = Date.parse("yyyyMMddmmssSSS", currentTimestamp.toString() ).clearTime()

// loop through all folders beginning with the timestamp and load all contained jsons
def foldersToScan = []
queryDate.upto(currentDate) {
    String dateString = it.format("yyyyMMdd").toString()
    String year = dateString.substring(0,4)
    String month = dateString.substring(4,6)
    String day = dateString.substring(6,8)
    foldersToScan.add("/${year}/${month}/${day}/")

}

logger.info("------------------readJSONs First Folder START---------------")

// go through all files in the first outbound folder and load and write all files to a list
List stateFiles = resources.search("system:${application.getProperty("mdm.stagingarea")}/outbound/state${foldersToScan[0]}*.json")
List changeFiles = resources.search("system:${application.getProperty("mdm.stagingarea")}/outbound/change${foldersToScan[0]}*.json")


// go through list and write all files after timestamp to cleaned list
cleanedStateFiles = stateFiles.findAll {
    it.basename[0..16].toLong() >= queryTimestamp
}

cleanedChangeFiles = changeFiles.findAll {
    it.basename[0..16].toLong() >= queryTimestamp
}


logger.info("------------------readJSONs Other Folders START---------------")


// go through all the other outbound folders and load and write all files
if ( foldersToScan.size() > 1 ) {
	for ( folder in foldersToScan[1..-1] ) {
		logger.info(folder)
	    if (resources.search("system:${application.getProperty("mdm.stagingarea")}/outbound/state${folder}*.json")) {
	        cleanedStateFiles.add(resources.search("system:${application.getProperty("mdm.stagingarea")}/outbound/state${folder}*.json"))
	    }
	
	    if (resources.search("system:${application.getProperty("mdm.stagingarea")}/outbound/change${folder}*.json")) {
	        cleanedChangeFiles.add(resources.search("system:${application.getProperty("mdm.stagingarea")}/outbound/change${folder}*.json"))
	    }
	}
}

logger.info("------------------start writing to State Kafka---------------")

def extractKafkaKey(String filepath, String topicType) {
    def fileJSON = resources.get(filepath).load("json")

    Map isBusinessPartner

    if ( topicType == "state" ) {
        isBusinessPartner = fileJSON["BusinessPartnerID"][0]["Versions"][0]["isBusinessPartner"][0]
    } else {
        isBusinessPartner = fileJSON["BusinessPartnerID"][0]["isBusinessPartner"][0]
    }

    isBusinessPartner.remove("BusinessPartner")
    isBusinessPartner.remove("TECHInternalPID")

    isBusinessPartner.put( "TECHMDMPartnerID", fileJSON["BusinessPartnerID"][0]["TECHMDMPartnerID"].toString() )

    def isBusinessPartnerPretty = JsonOutput.prettyPrint(JsonOutput.toJson(isBusinessPartner).toString())

    task.setVariable("kafka_key", isBusinessPartnerPretty.toString())

    return isBusinessPartnerPretty
}


for (file in cleanedStateFiles) {
    String kafkaKey = extractKafkaKey(file.toString(), "state")

    messaging.message("ch-mdm.state.all")
            .source(file.toString())
            .property("key", kafkaKey)
            .send()
}

logger.info("------------------files written to State Kafka---------------")

for (file in cleanedChangeFiles) {
    String kafkaKey = extractKafkaKey(file.toString(), "change")

    messaging.message("ch-mdm.change.all")
            .source(file.toString())
            .property("key", kafkaKey)
            .send()
}

logger.info("------------------files written to Change Kafka---------------")


