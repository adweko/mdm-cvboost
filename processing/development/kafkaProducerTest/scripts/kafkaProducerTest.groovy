import groovy.json.JsonOutput
import groovy.json.JsonSlurperClassic
import groovy.transform.Field

// consoleLogLevel
// INFO
// allgemeine Informationen (Programm gestartet, Programm beendet, Verbindung zu Host Foo aufgebaut, Verarbeitung dauerte SoUndSoviel Sekunden …)
// ERROR
// Fehler (Ausnahme wurde abgefangen. Bearbeitung wurde alternativ fortgesetzt)
// OFF
// Logging ausgeschaltet (default)
@Field String consoleLogLevel
consoleLogLevel = "INFO"
task.setVariable("consoleLogLevel", consoleLogLevel)
// console logging

def consoleLog(String level, String message) {
    Boolean doPrint
    switch (consoleLogLevel) {
        case "INFO":
            logger.info(message)
            break
        case "ERROR":
            logger.error(message)
            break
    }
}


logger.info("app.home is set to " + application.getProperty("app.home"))

