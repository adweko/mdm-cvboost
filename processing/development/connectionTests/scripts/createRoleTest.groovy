// Echo Test Service to test Spectrum Connection
def variables = task.getVariables()
def jsonSlurper = new groovy.json.JsonSlurper()
def roleMap = """
{"Input":{"Row":[{"BITEMPValidFrom":"01.01.2019",
"BITEMPValidTo":"31.12.2019",
"BITEMPMDMTimestamp":"20190101120000",
"TECHMDMPartnerID":"ROLETEST",
"TECHExternalPID":"ext001",
"TECHInternalPID":"xx",
"TECHSourceSystem":"CVBoostTest",
"companyCode":"ADWEKO",
"costumerStatus":"Satisfied",
"customerNumber":"0815",
"customerSince":"BeginOfTime",
"employeeCode":"4711",
"employmentStatus":"employed",
"entryDate":"01.01.2019",
"exitDate":"31.12.2019",
"generalAgency":"CIA",
"partnerStatusInformation":"divorced",
"personalNumber":"007",
"purpose":"none",
"roleID":"xx",
"roleType":"someType",
"RoleIDLabel":"xx",
"RoleLabel":"xx"}]}}
""";

def roleObject = jsonSlurper.parseText(roleMap)

def result = 
	http.request("https://mdm-spectrum-devl.helvetia.ch:8443/rest/createRole/results.json") // creates new request object
    	.post() // sets the method to post
    	.body()
			.mimeType("application/json; charset=UTF-8")
			.encoding("ignore")
			.set("json",roleObject)
    	.secure()   // add security
        	.basic()    // with basic authentication
            	.username("cvboost") // username for authentication
            	.password("cvboost") // password for authentication
        	.set()  // adds security to request
    .execute()  // executes the request
        .body("JSON"); // returns the body as object

        
        
println "call: " +roleObject
println "response: " +result       