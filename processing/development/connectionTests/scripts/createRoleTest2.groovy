// Echo Test Service to test Spectrum Connection
def variables = task.getVariables()
def jsonSlurper = new groovy.json.JsonSlurper()
def roleObject = jsonSlurper.parseText('{"Input":{"Row":[{"BITEMPValidFrom":"01.01.2019"}]}}')

def result = 
	http.request("https://mdm-spectrum-devl.helvetia.ch:8443/rest/createRole/results.json") // creates new request object
    	.post() // sets the method to post
    	.body()
			.mimeType("application/json; charset=UTF-8")
			.encoding("ignore")
			.set("json",roleObject)
    	.secure()   // add security
        	.basic()    // with basic authentication
            	.username("cvboost") // username for authentication
            	.password("cvboost") // password for authentication
        	.set()  // adds security to request
    .execute()  // executes the request
        .body("JSON"); // returns the body as object

        
        
println "call: " +roleObject
println "response: " +result       