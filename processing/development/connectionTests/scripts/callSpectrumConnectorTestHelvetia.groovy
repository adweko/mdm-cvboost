// Echo Test Service to test Spectrum Connection
def variables = task.getVariables()
def jsonSlurper = new groovy.json.JsonSlurper()
def echoObject = jsonSlurper.parseText('{"Input":{"Row":[{"Echo":"Hello World!"}]}}')

def result = 
	http.request("https://mdm-spectrum-devl.helvetia.ch:8443/rest/Echo/results.json") // creates new request object
    	.post() // sets the method to post
    	.body()
			.mimeType("application/json; charset=UTF-8")
			.encoding("ignore")
			.set("json",echoObject)
    	.secure()   // add security
        	.basic()    // with basic authentication
            	.username("cvboost") // username for authentication
            	.password("cvboost") // password for authentication
        	.set()  // adds security to request
    .execute()  // executes the request
        .body("JSON"); // returns the body as object
        
println "call: " +echoObject
println "response: " +result       