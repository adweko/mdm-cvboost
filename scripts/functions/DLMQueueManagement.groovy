import com.hazelcast.client.HazelcastClient
import com.hazelcast.client.config.ClientConfig
import com.hazelcast.client.config.ClientNetworkConfig
import com.hazelcast.core.HazelcastInstance
import com.hazelcast.core.ILock
import com.hazelcast.core.IMap
import groovy.transform.Canonical

import java.sql.Timestamp
import java.time.Instant

def connectHazelcast() {

    // Connect to Hazelcast instance
    def hazelcasturl = application.getProperty("mdm.hazelcasturl")
    def hazelcastname = application.getProperty("mdm.hazelcastname")
    def hazelcastpw = application.getProperty("mdm.hazelcastpw")
    ClientConfig clientConfig = new ClientConfig()
    ClientNetworkConfig clientNetConfig = new ClientNetworkConfig()
    clientNetConfig.addAddress(hazelcasturl)
    clientConfig.setNetworkConfig(clientNetConfig)
    clientConfig.getGroupConfig().setName(hazelcastname).setPassword(hazelcastpw)

    HazelcastInstance hazelcastClient = HazelcastClient.newHazelcastClient(clientConfig)

    return hazelcastClient
}

def queue(String businessPartnerID, String processID, boolean testmode) {
    logger.debug("PID:" + processID + " Start DLMQueue")

    Instant BITEMPMDMTimestamp

    if (testmode) {
        logger.debug("PID:" + processID + " Testmode active")
        BITEMPMDMTimestamp = Instant.now()
        return BITEMPMDMTimestamp
    }

    if (businessPartnerID == null || businessPartnerID == "") {
        logger.warning("PID:" + processID + " no businessPartnerID provided")
        BITEMPMDMTimestamp = Instant.now()
        return BITEMPMDMTimestamp
    }

    logger.debug("PID:" + processID + " Connecting Hazelcast")
    // Connect to Hazelcast instance
    HazelcastInstance hazelcastClient = connectHazelcast()

    try {
/* Create entry in processIDMap, synchronized by using Hazelcast IMap locks
 */
        IMap<Object, Object> lockMap = hazelcastClient.getMap("lockMap")

        // lock businessPartnerID, waiting until lock is available
        logger.debug("PID:" + processID + " locking lockMap")
        lockMap.lock(businessPartnerID)
        logger.debug("PID:" + processID + " locked")

        try {

            // Get Map for businessPartnerID
            Map processIDMap = [:]
            if (!lockMap.isEmpty()) {
                processIDMap = lockMap.get(businessPartnerID)
            }
            if (processIDMap == null) {
                processIDMap = [:]
            }
            // Get Timestamp
            BITEMPMDMTimestamp = Instant.now()

            logger.debug("PID:" + processID + " Queueing processID: " + processID + " Timestamp: " + BITEMPMDMTimestamp.toString() + " in lockMap entry key: " + businessPartnerID)

            // insert request in queue
//            processIDMap.put(processID, BITEMPMDMTimestamp.toString())
            processIDMap.put(processID, Timestamp.from(BITEMPMDMTimestamp))
            logger.debug("PID:" + processID + " processIDMap " + businessPartnerID + ": " + (processIDMap as CharSequence))

            // update lockMap
            lockMap.put(businessPartnerID, processIDMap)
        }
        finally {
            logger.debug("PID:" + processID + " unlocking lockMap")
            lockMap.unlock(businessPartnerID)
            logger.debug("PID:" + processID + " unlocked")
        }

    }
    catch (Exception ex) {
        logger.error(ex.toString())
    }
    finally {
        hazelcastClient.shutdown()
        clientConfig = null
        logger.debug("PID:" + processID + " End DLMQueue")
        logger.debug("BITEMPMDMTimestamp: " + BITEMPMDMTimestamp.toString())
        return BITEMPMDMTimestamp
    }

}


// define priority queue for processIDs and timestamps
@Canonical
class Task implements Comparable<Task> {
    String timestamp
    String processID

    int compareTo(Task o) { timestamp <=> o?.timestamp }
}


def check(String businessPartnerID, String processID, String permitType, boolean testmode) {
    logger.debug("PID:" + processID + " Start DLMCheck")

    if (testmode) {
        logger.debug("PID:" + processID + " Testmode active, return true")
        return true
    }

    if (businessPartnerID == null || businessPartnerID == "") {
        logger.debug("PID:" + processID + " no businessPartnerID provided, return true")
        return true
    }

    logger.debug("PID: " + processID + " Connect Hazelcast")

    boolean ourTurn

    // Connect to Hazelcast instance
    HazelcastInstance hazelcastClient = connectHazelcast()
    try {

/* get processIDMap, synchronized by using Hazelcast IMap locks
 */
        IMap<Object, Object> lockMap = hazelcastClient.getMap("lockMap")

        Map processIDMap = [:]


        // lock businessPartnerID, waiting until lock is available
        logger.debug("PID: " + processID + " locking lockMap")
        lockMap.lock(businessPartnerID)
        logger.debug("PID: " + processID + " locked")
        try {

            // get Map for businessPartnerID
            if (!lockMap.isEmpty()) {
                processIDMap = lockMap.get(businessPartnerID)
            }

            def processIDQueue = new PriorityQueue<Task>()

            // insert Map entries into Priority queue
            processIDMap.each { processID2, timestamp2 ->
                processIDQueue.add(new Task(timestamp: timestamp2, processID: processID2))
            }
            logger.debug("PID: " + processID + " processIDQueue: " + processIDQueue.toString())

            // read top priority entry
            String topProcessID
            try {
                topProcessID = processIDQueue.first().processID
            }
            catch (Exception ex) {
                // if there is no topPriority, we win
                topProcessID = processID
            }

            // check if its the own processID
            if (topProcessID == processID) {

                // try to acquire ressource
                logger.debug("PID: " + processID + " Trying to acquire one semaphore for " + permitType + ". ")
                Boolean acquired = acquirePermit(permitType)

                if (acquired) {
                    logger.debug("PID: " + processID + "Acquiring successful.")

                    // we are on top => continue
                    logger.debug("PID: " + processID + " It's our turn, continue")
                    ourTurn = true

                } else {
                    logger.debug("PID: " + processID + " It was our turn, but no " + permitType + " available, so we wait")
                    ourTurn = false
                }


            } else {

                logger.debug("PID: " + processID + " It was not our turn, so we wait")
                ourTurn = false
            }
            logger.debug("PID: " + processID + " partnerID: " + businessPartnerID + " topProcessID: " + topProcessID)

        } finally {
            lockMap.unlock(businessPartnerID)

        }
    }
    catch (Exception ex) {
        logger.error(ex.toString())
    }
    finally {
        hazelcastClient.shutdown()
        clientConfig = null
        logger.debug("PID: " + processID + "End DLMCheck")
        return ourTurn
    }

}

// check without reserving permits
def check(String businessPartnerID, String processID, boolean testmode) {
    logger.debug("PID:" + processID + " Start DLMCheck")

    if (testmode) {
        logger.debug("PID:" + processID + " Testmode active, return true")
        return true
    }

    if (businessPartnerID == null || businessPartnerID == "") {
        logger.debug("PID:" + processID + " no businessPartnerID provided, return true")
        return true
    }

    logger.debug("PID: " + processID + " Connect Hazelcast")

    boolean ourTurn

    // Connect to Hazelcast instance
    HazelcastInstance hazelcastClient = connectHazelcast()
    try {

/* get processIDMap, synchronized by using Hazelcast IMap locks
 */
        IMap<Object, Object> lockMap = hazelcastClient.getMap("lockMap")

        Map processIDMap = [:]


        // lock businessPartnerID, waiting until lock is available
        logger.debug("PID: " + processID + " locking lockMap")
        lockMap.lock(businessPartnerID)
        logger.debug("PID: " + processID + " locked")
        try {

            // get Map for businessPartnerID
            if (!lockMap.isEmpty()) {
                processIDMap = lockMap.get(businessPartnerID)
            }

            def processIDQueue = new PriorityQueue<Task>()

            // insert Map entries into Priority queue
            processIDMap.each { processID2, timestamp2 ->
                processIDQueue.add(new Task(timestamp: timestamp2, processID: processID2))
            }
            logger.debug("PID: " + processID + " processIDQueue: " + processIDQueue.toString())

            // read top priority entry
            String topProcessID
            try {
                topProcessID = processIDQueue.first().processID
            }
            catch (Exception ex) {
                // if there is no topPriority, we win
                topProcessID = processID
            }

            // check if its the own processID
            if (topProcessID == processID) {

                logger.debug("PID: " + processID + " It's our turn, continue")
                ourTurn = true

            } else {

                logger.debug("PID: " + processID + " It was not our turn, so we wait")
                ourTurn = false
            }
            logger.debug("PID: " + processID + " partnerID: " + businessPartnerID + " topProcessID: " + topProcessID)

        } finally {
            lockMap.unlock(businessPartnerID)
        }
    }
    catch (Exception ex) {
        logger.error(ex.toString())
    }
    finally {
        hazelcastClient.shutdown()
        clientConfig = null
        logger.debug("PID: " + processID + "End DLMCheck")
        return ourTurn
    }

}

def dequeue(String businessPartnerID, String processID, String permitType, boolean testmode) {
    logger.debug("PID:" + processID + " Start dequeue and release permit")

    Boolean dequeued = true

    if (testmode) {
        logger.debug("PID:" + processID + " Testmode active, return true")
        return dequeued
    }

    if (businessPartnerID == null || businessPartnerID == "") {
        logger.debug("PID:" + processID + " no businessPartnerID provided, return true")
        return dequeued
    }

    logger.debug("PID: " + processID + " Connect Hazelcast")
    // Connect to Hazelcast instance
    HazelcastInstance hazelcastClient = connectHazelcast()
    try {

        IMap<Object, Object> lockMap = hazelcastClient.getMap("lockMap")

        lockMap.lock(businessPartnerID)
        try {
            // get Map for businessPartnerID
            Map processIDMap = [:]
            if (!lockMap.isEmpty()) {
                processIDMap = lockMap.get(businessPartnerID)
            }

            if (processIDMap != null) {
                if (processIDMap.containsKey(processID)) {
                    processIDMap.remove(processID)
                }

                logger.debug("PID: " + processID + " Removed processID " + processID + " from processIDMap for lockMap entry " + businessPartnerID)

                if (processIDMap.isEmpty()) {
                    // if this was the only processID, remove lockMap entry
                    if (lockMap.containsKey(businessPartnerID)) {
                        lockMap.remove(businessPartnerID)
                    }
                    logger.debug("PID: " + processID + " Removed lockMap entry for " + businessPartnerID + " as this was actually the only processID in the queue for " + businessPartnerID)

                } else {
                    // update lockMap entry
                    lockMap.put(businessPartnerID, processIDMap)
                    logger.debug("PID: " + processID + " Updated lockMap entry for " + businessPartnerID)
                }
            }

        }
        catch (Exception ex) {
            // do nothing
        } finally {
            // permit calls
            logger.debug("PID: " + processID + " Releasing one semaphore for " + permitType)
            // release one semaphore
            Boolean released = releasePermit(permitType)

            logger.debug("PID: " + processID + " One released.")

            // unlock
            try {
                lockMap.unlock(businessPartnerID)
            }
            catch (Exception ex) {

            }
        }
    }
    catch (Exception ex) {
        logger.error(ex.toString())
        dequeued = false
    }
    finally {
        hazelcastClient.shutdown()
        clientConfig = null
        logger.debug("PID: " + processID + " End dequeue and release permit")
        return dequeued
    }
}

// dequeue without releasing permits
def dequeue(String businessPartnerID, String processID, boolean testmode) {
    logger.debug("PID:" + processID + " Start dequeue")

    Boolean dequeued = true

    if (testmode) {
        logger.debug("PID:" + processID + " Testmode active, return true")
        return dequeued
    }

    if (businessPartnerID == null || businessPartnerID == "") {
        logger.debug("PID:" + processID + " no businessPartnerID provided, return true")
        return dequeued
    }

    logger.debug("PID: " + processID + " Connect Hazelcast")
    // Connect to Hazelcast instance
    HazelcastInstance hazelcastClient = connectHazelcast()
    try {

        IMap<Object, Object> lockMap = hazelcastClient.getMap("lockMap")

        lockMap.lock(businessPartnerID)
        try {
            // get Map for businessPartnerID
            Map processIDMap = [:]
            if (!lockMap.isEmpty()) {
                processIDMap = lockMap.get(businessPartnerID)
            }

            if (processIDMap != null) {
                if (processIDMap.containsKey(processID)) {
                    processIDMap.remove(processID)
                }

                logger.debug("PID: " + processID + " Removed processID " + processID + " from processIDMap for lockMap entry " + businessPartnerID)

                if (processIDMap.isEmpty()) {
                    // if this was the only processID, remove lockMap entry
                    if (lockMap.containsKey(businessPartnerID)) {
                        lockMap.remove(businessPartnerID)
                    }
                    logger.debug("PID: " + processID + " Removed lockMap entry for " + businessPartnerID + " as this was actually the only processID in the queue for " + businessPartnerID)

                } else {
                    // update lockMap entry
                    lockMap.put(businessPartnerID, processIDMap)
                    logger.debug("PID: " + processID + " Updated lockMap entry for " + businessPartnerID)
                }
            }

        }
        catch (Exception ex) {
            // do nothing
        } finally {
            // unlock
            try {
                lockMap.unlock(businessPartnerID)
            }
            catch (Exception ex) {

            }
        }
    }
    catch (Exception ex) {
        logger.error(ex.toString())
        dequeued = false
    }
    finally {
        hazelcastClient.shutdown()
        clientConfig = null
        logger.debug("PID: " + processID + " End dequeue")
        return dequeued
    }
}

def getProcessIDMap(String businessPartnerID, boolean testmode) {
    logger.debug("Start DLMGetProcessIDMap")

    Map processIDMap = [:]

    if (testmode) {
        logger.debug("Testmode active, return empty Map")
        return processIDMap
    }

    if (businessPartnerID == null || businessPartnerID == "") {
        logger.debug("PID:" + processID + " no businessPartnerID provided, return true")
        return true
    }

    logger.debug("Connect Hazelcast")
    // Connect to Hazelcast instance
    HazelcastInstance hazelcastClient = connectHazelcast()

/* get processIDMap, synchronized by using Hazelcast IMap locks
 */
    IMap<Object, Object> lockMap = hazelcastClient.getMap("lockMap")

    try {
        // lock businessPartnerID, waiting until lock is available
        logger.debug("Locking lockMap")
        lockMap.lock(businessPartnerID)
        logger.debug("Locked")


        // get Map for businessPartnerID
        if (!lockMap.isEmpty()) {
            processIDMap = lockMap.get(businessPartnerID)
        }

    }
    catch (Exception ex) {
        logger.error(ex.toString())
    }
    finally {
        lockMap.unlock(businessPartnerID)
        hazelcastClient.shutdown()
        clientConfig = null
        logger.debug("End DLMGetProcessIDMap")
        return processIDMap
    }

}

def forceUnlock(String businessPartnerID, boolean testmode) {
    logger.debug("Start DLMForceUnlock")


    if (testmode) {
        logger.debug("Testmode active, return true")
        return true
    }

    if (businessPartnerID == null || businessPartnerID == "") {
        logger.debug("PID:" + processID + " no businessPartnerID provided, return true")
        return true
    }

    logger.debug("Connect Hazelcast")
    // Connect to Hazelcast instance
    HazelcastInstance hazelcastClient = connectHazelcast()
    try {
        IMap<Object, Object> lockMap = hazelcastClient.getMap("lockMap")

        // get Map for businessPartnerID
        lockMap.forceUnlock(businessPartnerID)
        logger.debug("Forced unlock of " + businessPartnerID + " in lockMap")

    }
    catch (Exception ex) {
        logger.error(ex.toString())
        return false
    }
    finally {
        hazelcastClient.shutdown()
        clientConfig = null
        logger.debug("End DLMForceUnlock")
        return true
    }
}

def getLockList() {
    logger.debug("Start DLMGetLockList")

    int availablePermits = 0

    List lockList = []


    logger.debug("Connect Hazelcast")
    // Connect to Hazelcast instance
    HazelcastInstance hazelcastClient = connectHazelcast()
    try {
        IMap lockIMap = hazelcastClient.getMap("lockMap")


        lockIMap.each { key, value ->
            Map tabline = [:]
            tabline.put("partnerID", key)
            boolean locked = lockIMap.tryLock(key)
            if (locked) {
                lockIMap.unlock(key)
                tabline.put("lockState", "unlocked")
            } else {
                tabline.put("lockState", "locked")
            }
            tabline.put("processIDcount", value.size())
            lockList.add(tabline)
        }
    }
    catch (Exception ex) {
        logger.error(ex.toString())
    }
    finally {
        hazelcastClient.shutdown()
        clientConfig = null
        logger.debug("End DLMGetLockList")
        return lockList
    }
}

def clearLockList() {
    logger.debug("Start DLMClearLockList")

    int availablePermits = 0

    logger.debug("Connect Hazelcast")
    // Connect to Hazelcast instance
    HazelcastInstance hazelcastClient = connectHazelcast()
    try {
        IMap lockIMap = hazelcastClient.getMap("lockMap")

        // unlock every entry in lock ma
        lockIMap.each { key, value ->
            lockIMap.forceUnlock(key)
        }
        // clera LockMap
        lockIMap.clear()
    }
    catch (Exception ex) {
        logger.error(ex.toString())
        logger.debug("End DLMClearLockList")
        return false
    }
    finally {
        hazelcastClient.shutdown()
        clientConfig = null
        logger.debug("End DLMClearLockList")
        return true
    }
}

def clearPermitsMap() {
    logger.debug("Start DLMClearPermitsMap")

    int availablePermits = 0

    List lockList = []


    logger.debug("Connect Hazelcast")
    // Connect to Hazelcast instance
    HazelcastInstance hazelcastClient = connectHazelcast()
    try {
        IMap permitsMap = hazelcastClient.getMap("permitsMap")
        permitsMap.forceUnlock("zepasCalls")
        permitsMap.forceUnlock("syriusCalls")
        permitsMap.clear()
    }
    catch (Exception ex) {
        logger.error(ex.toString())
        logger.debug("End DLMClearPermitsMap")
        return false
    }
    finally {
        hazelcastClient.shutdown()
        clientConfig = null
        logger.debug("End DLMClearPermitsMap")
        return true
    }
}


def acquirePermit(String permitType) {
    logger.debug("Start DLMAcquirePermit")
    Integer zepasPermits = application.getProperty("mdm.zepasPermits").toInteger()
    Integer syriusPermits = application.getProperty("mdm.syriusPermits").toInteger()

    logger.debug("Connect Hazelcast")
    // Connect to Hazelcast instance
    HazelcastInstance hazelcastClient = connectHazelcast()

    IMap<Object, Object> permitsMap = hazelcastClient.getMap("permitsMap")

    Integer permitsValue

    try {
        // lock permitType, waiting until lock is available
        logger.debug("Locking permitsMap")
        permitsMap.lock(permitType)
        logger.debug("Locked")

        // get permitsValue
        if (permitsMap.containsKey(permitType)) {
            permitsValue = permitsMap.get(permitType)
        } else {
            switch (permitType) {
                case "zepasCalls":
                    permitsValue = zepasPermits
                    break
                case "syriusCalls":
                    permitsValue = syriusPermits
                    break
            }
        }

        if (permitsValue > 0) {
            permitsMap.put(permitType, permitsValue - 1)
            permitsMap.unlock(permitType)
            hazelcastClient.shutdown()
            clientConfig = null
            logger.debug("End DLMAcquirePermit")
            return true
        } else {
            permitsMap.unlock(permitType)
            hazelcastClient.shutdown()
            clientConfig = null
            logger.debug("End DLMAcquirePermit")
            return false
        }


    }
    catch (Exception ex) {
        permitsMap.unlock(permitType)
        hazelcastClient.shutdown()
        clientConfig = null
        logger.error(ex.toString())
        logger.debug("End DLMAcquirePermit")
        return false
    }
}

def releasePermit(String permitType) {
    logger.debug("Start DLMReleasePermit")

    logger.debug("Connect Hazelcast")
    // Connect to Hazelcast instance
    HazelcastInstance hazelcastClient = connectHazelcast()

    IMap<Object, Object> permitsMap = hazelcastClient.getMap("permitsMap")

    Integer permitsMax
    Integer permitsValue

    try {
        // lock permitType, waiting until lock is available
        logger.debug("Locking permitsMap")
        permitsMap.lock(permitType)
        logger.debug("Locked")

        // get permitsValue
        if (!permitsMap.isEmpty()) {
            permitsValue = permitsMap.get(permitType)
        } else {
            permitsValue = 0
        }

        permitsMap.put(permitType, permitsValue + 1)
        permitsMap.unlock(permitType)
        hazelcastClient.shutdown()
        clientConfig = null
        logger.debug("End DLMReleasePermit")
        return true

    }
    catch (Exception ex) {
        hazelcastClient.shutdown()
        clientConfig = null
        logger.error(ex.toString())
        logger.debug("End DLMReleasePermit")
        return false
    }
}

def modifyPermits(String permitType, Integer value) {
    logger.debug("Start DLMModifyPermits")

    logger.debug("Connect Hazelcast")
    // Connect to Hazelcast instance
    HazelcastInstance hazelcastClient = connectHazelcast()

    IMap<Object, Object> permitsMap = hazelcastClient.getMap("permitsMap")

    try {
        // lock permitType, waiting until lock is available
        logger.debug("Locking permitsMap")
        permitsMap.lock(permitType)
        logger.debug("Locked")

        permitsMap.put(permitType, value)

    }
    catch (Exception ex) {
        logger.error(ex.toString())
    }
    finally {
        permitsMap.unlock(permitType)
        hazelcastClient.shutdown()
        clientConfig = null
        logger.debug("End DLMModifyPermits")
        return true
    }
}

def availablePermits(String permitType) {
    logger.debug("Start DLMAvailablePermits")
    Integer zepasPermits = application.getProperty("mdm.zepasPermits").toInteger()
    Integer syriusPermits = application.getProperty("mdm.syriusPermits").toInteger()

    Integer availablePermits = 0
    Integer permitsMax = 0

    logger.debug("Connect Hazelcast")
    // Connect to Hazelcast instance
    HazelcastInstance hazelcastClient = connectHazelcast()

    IMap<Object, Object> permitsMap = hazelcastClient.getMap("permitsMap")

    try {
        // lock permitType, waiting until lock is available
        logger.debug("Locking permitsMap")
        permitsMap.lock(permitType)
        logger.debug("Locked")

        switch (permitType) {
            case "zepasCalls":
                permitsMax = zepasPermits
                break
            case "syriusCalls":
                permitsMax = syriusPermits
                break
        }

        // get availablePermits
        if (!permitsMap.isEmpty()) {
            availablePermits = permitsMap.get(permitType)
        } else {
            availablePermits = permitsMax
        }

        if (availablePermits == null) {
            availablePermits = permitsMax
        }
    }
    catch (Exception ex) {
        logger.error(ex.toString())
    }
    finally {
        permitsMap.unlock(permitType)
        hazelcastClient.shutdown()
        clientConfig = null
        logger.debug("End DLMAvailablePermits")
        return availablePermits
    }
}

def lockReprocessing() {
    logger.debug("Start lockReprocessing")

    logger.debug("Connect Hazelcast")
    // Connect to Hazelcast instance
    HazelcastInstance hazelcastClient = connectHazelcast()
    boolean lockStatus
    try {
        ILock reprocessingLock = hazelcastClient.getLock("Reprocessing")
        lockStatus = reprocessingLock.tryLock()
    }
    catch (Exception ex) {
        logger.error(ex.toString())
    }
    finally {
        hazelcastClient.shutdown()
        clientConfig = null

        logger.debug("End lockReprocessing")
        return lockStatus
    }
}

def reprocessingLockStatus() {
    logger.debug("Start reprocessingStatus")

    logger.debug("Connect Hazelcast")
    // Connect to Hazelcast instance
    HazelcastInstance hazelcastClient = connectHazelcast()
    boolean lockStatus
    try {
        ILock reprocessingLock = hazelcastClient.getLock("Reprocessing")
        lockStatus = reprocessingLock.isLocked()
    }
    catch (Exception ex) {
        logger.error(ex.toString())
    }
    finally {
        hazelcastClient.shutdown()
        clientConfig = null
        logger.debug("End reprocessingStatus")
        return lockStatus
    }
}

def unlockReprocessing() {
    logger.debug("Start unlockReprocessing")

    logger.debug("Connect Hazelcast")
    // Connect to Hazelcast instance
    HazelcastInstance hazelcastClient = connectHazelcast()

    try {
        ILock reprocessingLock = hazelcastClient.getLock("Reprocessing");
        reprocessingLock.forceUnlock();
    }
    catch (Exception ex) {
        logger.error(ex.toString())
    }
    finally {
        hazelcastClient.shutdown()
        clientConfig = null

        logger.debug("End unlockReprocessing")
    }
}