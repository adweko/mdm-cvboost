package functions

import java.sql.Timestamp
import java.time.Instant


def createTransactionStatus(String workorderID, String creator, String owner) {

    // set values
    Instant now = Instant.now()
    Timestamp lastUpdatedByTime = Timestamp.from(now)

    // prepare SQL
    com.compoverso.dmp.extension.core.connector.service.jdbc.builder.UpdateBuilderImpl update = jdbc.update() // creates new update for internal database
            .sql("""INSERT INTO MDM_TRANSACTION_STATUS (
                        WORKORDER_ID_,
                        WORKORDER_START_TIME_,
                        WORKORDER_CREATOR_,
                        WORKORDER_OWNER_,
                        LAST_UPDATED_BY_,
                        LAST_UPDATED_BY_TIME_
                    )
                    VALUES (
                        :workorderID,
                        :workorderStartTime,
                        :workorderCreator,
                        :workorderOwner,
                        :lastUpdatedBy,
                        :lastUpdatedByTime
                    )""")
            .parameters([
                    workorderID       : workorderID.toUpperCase(),
                    workorderStartTime: lastUpdatedByTime,
                    workorderCreator  : creator,
                    workorderOwner    : owner,
                    lastUpdatedBy     : creator,
                    lastUpdatedByTime : lastUpdatedByTime
            ])

    Integer rowcount = update.execute()

    logger.debug("Status created. workorderID: " + workorderID)
}

def updateMDMPartnerID(String workorderID, String lastUpdatedBy, String MDMPartnerID) {

    Instant now = Instant.now()
    Timestamp lastUpdatedByTime = Timestamp.from(now)

    if (MDMPartnerID != null) {
        MDMPartnerID = MDMPartnerID.toUpperCase()
    }

    com.compoverso.dmp.extension.core.connector.service.jdbc.builder.UpdateBuilderImpl update = jdbc.update() // update for internal database
            .sql("UPDATE MDM_TRANSACTION_STATUS SET MDM_PARTNERID_ = :MDMPartnerID, LAST_UPDATED_BY_ = :lastUpdatedBy,  LAST_UPDATED_BY_TIME_ = :lastUpdatedByTime WHERE WORKORDER_ID_ = :workorderID")
            .parameter("workorderID", workorderID.toUpperCase())
            .parameter("MDMPartnerID", MDMPartnerID)
            .parameter("lastUpdatedBy", lastUpdatedBy)
            .parameter("lastUpdatedByTime", lastUpdatedByTime)

    Integer rowcount = update.execute()

    logger.debug("MDMPartnerID updated. workorderID: " + workorderID + " MDMPartnerID: " + MDMPartnerID)
}

def updateIDNumber(String workorderID, String lastUpdatedBy, String IDType, String IDNumber, Integer transactionSource, String transactionSourceProcessID, Integer transactionType) {

    Instant now = Instant.now()
    Timestamp lastUpdatedByTime = Timestamp.from(now)

    if (IDNumber != null) {
        IDNumber = IDNumber.toUpperCase()
    }

    // prepare SQL
    com.compoverso.dmp.extension.core.connector.service.jdbc.builder.UpdateBuilderImpl update = jdbc.update() // update for internal database
            .sql("UPDATE MDM_TRANSACTION_STATUS SET " +
                    "TRANSACTION_SOURCE_ = :transactionSource, " +
                    "TRANSACTION_SOURCE_PROCESS_ID_ = :transactionSourceProcessID, " +
                    "TRANSACTION_TYPE_ = :transactionType, " +
                    "TRANSACTION_IDTYPE_ = :IDType, " +
                    "TRANSACTION_IDNUMBER_ = :IDNumber, " +
                    "LAST_UPDATED_BY_ = :lastUpdatedBy, " +
                    "LAST_UPDATED_BY_TIME_ = :lastUpdatedByTime " +
                    "WHERE WORKORDER_ID_ = :workorderID")
            .parameter("transactionSource", transactionSource)
            .parameter("transactionSourceProcessID", transactionSourceProcessID.toUpperCase())
            .parameter("transactionType", transactionType)
            .parameter("IDType", IDType)
            .parameter("IDNumber", IDNumber)
            .parameter("lastUpdatedBy", lastUpdatedBy)
            .parameter("lastUpdatedByTime", lastUpdatedByTime)
            .parameter("workorderID", workorderID.toUpperCase())
    Integer rowcount = update.execute()

    logger.debug("IDNumber updated. workorderID: " + workorderID + " IDType: " + IDType + " IDNumber: " + IDNumber)
}

def updateTransactionQueuedStatus(String workorderID, String lastUpdatedBy, Boolean status, Timestamp MDMTimestamp, String IDNumber) {

    // set values
    Instant now = Instant.now()
    Timestamp lastUpdatedByTime = Timestamp.from(now)

    if (IDNumber != null) {
        IDNumber = IDNumber.toUpperCase()
    }

    // prepare SQL
    Integer statusInt = 0
    if (status) {
        statusInt = 1
    }

    com.compoverso.dmp.extension.core.connector.service.jdbc.builder.UpdateBuilderImpl update = jdbc.update() // update for internal database
            .sql("UPDATE MDM_TRANSACTION_STATUS SET TRANSACTION_QUEUED_ = :status, TRANSACTION_QUEUED_TIME_ = :MDMTimestamp, LAST_UPDATED_BY_ = :lastUpdatedBy, LAST_UPDATED_BY_TIME_ = :lastUpdatedByTime WHERE WORKORDER_ID_ = :workorderID")
            .parameter("workorderID", workorderID.toUpperCase())
            .parameter("status", statusInt)
            .parameter("MDMTimestamp", MDMTimestamp)
            .parameter("lastUpdatedBy", lastUpdatedBy)
            .parameter("lastUpdatedByTime", lastUpdatedByTime)

    Integer rowcount = update.execute()

    logger.debug("Status TRANSACTION_QUEUED_ updated. workorderID: " + workorderID + " status " + status)
}

def updateTransactionQueuedStatus(String workorderID, String lastUpdatedBy, Boolean status, Timestamp MDMTimestamp, String IDNumber, List transaction, Map context) {

    // set values
    Instant now = Instant.now()
    Timestamp lastUpdatedByTime = Timestamp.from(now)

    if (IDNumber != null) {
        IDNumber = IDNumber.toUpperCase()
    }

    // save transaction to Inbound staging area
    def cleanDate = MDMTimestamp.format("yyyyMMddHHmmssSSS", TimeZone.getTimeZone("UTC")).toString()
    String year = cleanDate.substring(0, 4)
    String month = cleanDate.substring(4, 6)
    String day = cleanDate.substring(6, 8)

    // create the inbound folder path according to the timestamp
    String inboundFolder = "/inbound/" + year + '/' + month + '/' + day + '/'

    // name scheme "<Timestamp>_MDM_inbound_<partnerID>_<workorderID>.json"
    String inboundFile = "${cleanDate}_MDM_inbound_${IDNumber}_${workorderID}.json"

    // save json to inbound staging area
    String datapath = "system:${application.getProperty("mdm.stagingarea")}${inboundFolder}${inboundFile}"
    context.resources.get(datapath).save("json", transaction)
    logger.debug("Transaction staged. Datapath: " + datapath)

    // prepare SQL
    Integer statusInt = 0
    if (status) {
        statusInt = 1
    }

    com.compoverso.dmp.extension.core.connector.service.jdbc.builder.UpdateBuilderImpl update = jdbc.update() // update for internal database
            .sql("UPDATE MDM_TRANSACTION_STATUS SET TRANSACTION_QUEUED_ = :status, TRANSACTION_QUEUED_TIME_ = :MDMTimestamp, TRANSACTION_DATAPATH_ = :datapath, LAST_UPDATED_BY_ = :lastUpdatedBy, LAST_UPDATED_BY_TIME_ = :lastUpdatedByTime WHERE WORKORDER_ID_ = :workorderID")
            .parameter("workorderID", workorderID.toUpperCase())
            .parameter("status", statusInt)
            .parameter("MDMTimestamp", MDMTimestamp)
            .parameter("datapath", datapath)
            .parameter("lastUpdatedBy", lastUpdatedBy)
            .parameter("lastUpdatedByTime", lastUpdatedByTime)

    Integer rowcount = update.execute()

    logger.debug("Status TRANSACTION_QUEUED_ updated. workorderID: " + workorderID + " status " + status)
}

def updateTransactionQueuedStatus(String workorderID, String lastUpdatedBy, Boolean status, Timestamp MDMTimestamp, String IDNumber, Map transaction, Map context) {

    // set values
    Instant now = Instant.now()
    Timestamp lastUpdatedByTime = Timestamp.from(now)

    if (IDNumber != null) {
        IDNumber = IDNumber.toUpperCase()
    }

    // save transaction to Inbound staging area
    def cleanDate = MDMTimestamp.format("yyyyMMddHHmmssSSS", TimeZone.getTimeZone("UTC")).toString()
    String year = cleanDate.substring(0, 4)
    String month = cleanDate.substring(4, 6)
    String day = cleanDate.substring(6, 8)

    // create the inbound folder path according to the timestamp
    String inboundFolder = "/inbound/" + year + '/' + month + '/' + day + '/'

    // name scheme "<Timestamp>_MDM_inbound_<partnerID>_<workorderID>.json"
    String inboundFile = "${cleanDate}_MDM_inbound_${IDNumber}_${workorderID}.json"

    // save json to inbound staging area
    String datapath = "system:${application.getProperty("mdm.stagingarea")}${inboundFolder}${inboundFile}"
    context.resources.get(datapath).save("json", transaction)
    logger.debug("Transaction staged. Datapath: " + datapath)

    // prepare SQL
    Integer statusInt = 0
    if (status) {
        statusInt = 1
    }

    com.compoverso.dmp.extension.core.connector.service.jdbc.builder.UpdateBuilderImpl update = jdbc.update() // update for internal database
            .sql("UPDATE MDM_TRANSACTION_STATUS SET TRANSACTION_QUEUED_ = :status, TRANSACTION_QUEUED_TIME_ = :MDMTimestamp, TRANSACTION_DATAPATH_ = :datapath, LAST_UPDATED_BY_ = :lastUpdatedBy, LAST_UPDATED_BY_TIME_ = :lastUpdatedByTime WHERE WORKORDER_ID_ = :workorderID")
            .parameter("workorderID", workorderID.toUpperCase())
            .parameter("status", statusInt)
            .parameter("MDMTimestamp", MDMTimestamp)
            .parameter("datapath", datapath)
            .parameter("lastUpdatedBy", lastUpdatedBy)
            .parameter("lastUpdatedByTime", lastUpdatedByTime)

    Integer rowcount = update.execute()

    logger.debug("Status TRANSACTION_QUEUED_ updated. workorderID: " + workorderID + " status " + status)
}

def updateTransactionStatus(String workorderID, String lastUpdatedBy, Integer statusType, Boolean status, String MDMPartnerID, Timestamp MDMTimestamp, List statusObject, Map context) {

    String statusTypeString = "TRANSACTION_COMM_"
    Integer statusInt = 0
    if (status) {
        statusInt = 1
    }

    if (MDMPartnerID != null) {
        MDMPartnerID = MDMPartnerID.toUpperCase()
    }

    // set values
    Instant now = Instant.now()
    Timestamp statusTime = Timestamp.from(now)
    String datapathName
    String topicType

    switch (statusType) {
        case 2:
            statusTypeString = "CHANGE_OUTBOUND_GEN_"
            datapathName = "CHANGE_OUTBOUND_DATAPATH_"
            topicType = "change"
            break
        case 3:
            statusTypeString = "STATE_OUTBOUND_GEN_"
            datapathName = "STATE_OUTBOUND_DATAPATH_"
            topicType = "state"
            break
        default:
            return false
            break
    }

// save statusObject to outbound staging area
    def cleanDate = MDMTimestamp.format("yyyyMMddHHmmssSSS", TimeZone.getTimeZone("UTC")).toString()
    String year = cleanDate.substring(0, 4)
    String month = cleanDate.substring(4, 6)
    String day = cleanDate.substring(6, 8)
    // create the outbound folder path according to the timestamp
    String outboundFolder = "/outbound/" + topicType + "/" + year + '/' + month + '/' + day + '/'

    // name scheme "<Timestamp>_MDM_outbound_<state/change>_<PartnerID>_<ProcessID>.json"
    String outboundFile = "${cleanDate}_MDM_outbound_" + topicType + "_${MDMPartnerID}_${workorderID}.json"

    // save json to outbound staging area
    String datapath = "system:${application.getProperty("mdm.stagingarea")}${outboundFolder}${outboundFile}"
    context.resources.get(datapath).save("json", statusObject)
    logger.debug("Outbound staged. Datapath: " + datapath)

    // prepare SQL
    com.compoverso.dmp.extension.core.connector.service.jdbc.builder.UpdateBuilderImpl update = jdbc.update() // update for internal database
            .sql("UPDATE MDM_TRANSACTION_STATUS SET "
                    + statusTypeString + " = :status, "
                    + statusTypeString + "TIME_ = :statusTime, "
                    + datapathName + " = :datapath, "
                    + "LAST_UPDATED_BY_ = :lastUpdatedBy, LAST_UPDATED_BY_TIME_ = :lastUpdatedByTime "
                    + "WHERE WORKORDER_ID_ = :workorderID")
            .parameter("workorderID", workorderID.toUpperCase())
            .parameter("status", statusInt)
            .parameter("statusTime", statusTime)
            .parameter("datapath", datapath)
            .parameter("lastUpdatedBy", lastUpdatedBy)
            .parameter("lastUpdatedByTime", statusTime)

    Integer rowcount = update.execute()

    logger.debug("Status updated. workorderID: " + workorderID + " " + statusTypeString + " " + status)
}

def updateTransactionStatus(String workorderID, String lastUpdatedBy, Integer statusType, Boolean status, String MDMPartnerID, Timestamp MDMTimestamp, Map statusObject, Map context) {

    String statusTypeString = "TRANSACTION_COMM_"
    Integer statusInt = 0
    if (status) {
        statusInt = 1
    }

    if (MDMPartnerID != null) {
        MDMPartnerID = MDMPartnerID.toUpperCase()
    }

    // set values
    Instant now = Instant.now()
    Timestamp statusTime = Timestamp.from(now)
    String datapathName
    String topicType

    switch (statusType) {
        case 2:
            statusTypeString = "CHANGE_OUTBOUND_GEN_"
            datapathName = "CHANGE_OUTBOUND_DATAPATH_"
            topicType = "change"
            break
        case 3:
            statusTypeString = "STATE_OUTBOUND_GEN_"
            datapathName = "STATE_OUTBOUND_DATAPATH_"
            topicType = "state"
            break
        default:
            return false
            break
    }

// save statusObject to outbound staging area
    def cleanDate = MDMTimestamp.format("yyyyMMddHHmmssSSS", TimeZone.getTimeZone("UTC")).toString()
    String year = cleanDate.substring(0, 4)
    String month = cleanDate.substring(4, 6)
    String day = cleanDate.substring(6, 8)
    // create the outbound folder path according to the timestamp
    String outboundFolder = "/outbound/" + topicType + "/" + year + '/' + month + '/' + day + '/'

    // name scheme "<Timestamp>_MDM_outbound_<state/change>_<PartnerID>_<ProcessID>.json"
    String outboundFile = "${cleanDate}_MDM_outbound_" + topicType + "_${MDMPartnerID}_${workorderID}.json"

    // save json to outbound staging area
    String datapath = "system:${application.getProperty("mdm.stagingarea")}${outboundFolder}${outboundFile}"
    context.resources.get(datapath).save("json", statusObject)
    logger.debug("Outbound staged. Datapath: " + datapath)

    // prepare SQL
    com.compoverso.dmp.extension.core.connector.service.jdbc.builder.UpdateBuilderImpl update = jdbc.update() // update for internal database
            .sql("UPDATE MDM_TRANSACTION_STATUS SET "
                    + statusTypeString + " = :status, "
                    + statusTypeString + "TIME_ = :statusTime, "
                    + datapathName + " = :datapath, "
                    + "LAST_UPDATED_BY_ = :lastUpdatedBy, LAST_UPDATED_BY_TIME_ = :lastUpdatedByTime "
                    + "WHERE WORKORDER_ID_ = :workorderID")
            .parameter("workorderID", workorderID.toUpperCase())
            .parameter("status", statusInt)
            .parameter("statusTime", statusTime)
            .parameter("datapath", datapath)
            .parameter("lastUpdatedBy", lastUpdatedBy)
            .parameter("lastUpdatedByTime", statusTime)

    Integer rowcount = update.execute()

    logger.debug("Status updated. workorderID: " + workorderID + " " + statusTypeString + " " + status)
}

def updateTransactionStatus(String workorderID, String lastUpdatedBy, Integer statusType, Boolean status) {

    Instant now = Instant.now()
    Timestamp lastUpdatedByTime = Timestamp.from(now)
    Timestamp statusTime = lastUpdatedByTime
    String statusTypeString = "TRANSACTION_COMM_"
    Integer statusInt = 0
    if (status) {
        statusInt = 1
    }
    // set values

    String datapathName
    String datapath

    switch (statusType) {
        case 1:
            statusTypeString = "TRANSACTION_COMM_"
            break
        case 2:
            statusTypeString = "CHANGE_OUTBOUND_GEN_"
            break
        case 3:
            statusTypeString = "STATE_OUTBOUND_GEN_"
            break
        case 4:
            statusTypeString = "CHANGE_OUTBOUND_COMM_"
            break
        case 5:
            statusTypeString = "STATE_OUTBOUND_COMM_"
            break
        case 6:
            statusTypeString = "FILES_ARCHIVED_"
            break
        default:
            return false
            break
    }

    com.compoverso.dmp.extension.core.connector.service.jdbc.builder.UpdateBuilderImpl update = jdbc.update() // update for internal database
            .sql("UPDATE MDM_TRANSACTION_STATUS SET " + statusTypeString + " = :status, " + statusTypeString + "TIME_ = :statusTime, LAST_UPDATED_BY_ = :lastUpdatedBy, LAST_UPDATED_BY_TIME_ = :lastUpdatedByTime WHERE WORKORDER_ID_ = :workorderID")
            .parameter("workorderID", workorderID.toUpperCase())
            .parameter("status", statusInt)
            .parameter("statusTime", statusTime)
            .parameter("lastUpdatedBy", lastUpdatedBy)
            .parameter("lastUpdatedByTime", lastUpdatedByTime)

    Integer rowcount = update.execute()

    logger.debug("Status updated. workorderID: " + workorderID + " " + statusTypeString + " " + status)
}

def updateTransactionCommittedStatus(String workorderID, String lastUpdatedBy, Boolean status, Boolean hasUpdate) {

    Instant now = Instant.now()
    Timestamp lastUpdatedByTime = Timestamp.from(now)
    Timestamp statusTime = lastUpdatedByTime
    Integer statusInt = 0
    if (status) {
        statusInt = 1
    }
    Integer hasUpdateInt = 0
    if (hasUpdate) {
        hasUpdateInt = 1
    }
    // set values

    com.compoverso.dmp.extension.core.connector.service.jdbc.builder.UpdateBuilderImpl update = jdbc.update() // update for internal database
            .sql("UPDATE MDM_TRANSACTION_STATUS SET TRANSACTION_COMM_ = :status, TRANSACTION_HAS_UPDATE_ = :hasUpdate, TRANSACTION_COMM_TIME_ = :statusTime, LAST_UPDATED_BY_ = :lastUpdatedBy, LAST_UPDATED_BY_TIME_ = :lastUpdatedByTime WHERE WORKORDER_ID_ = :workorderID")
            .parameter("workorderID", workorderID.toUpperCase())
            .parameter("status", statusInt)
            .parameter("statusTime", statusTime)
            .parameter("hasUpdate", hasUpdateInt)
            .parameter("lastUpdatedBy", lastUpdatedBy)
            .parameter("lastUpdatedByTime", lastUpdatedByTime)

    Integer rowcount = update.execute()

    logger.debug("Status updated. workorderID: " + workorderID + " TRANSACTION_COMM_ " + status + " TRANSACTION_HAS_UPDATE_ " + hasUpdate)
}


def queryTransactionStatus(Timestamp workorderStartTime, Integer maxNumberOfRows) {

    List statusList = []
    Integer selectedNumberOfRows = 0

    Map result = [:]

    try {

        // prepare SQL
        com.compoverso.dmp.extension.core.connector.service.jdbc.builder.SelectBuilderImpl select = jdbc.select() // select for internal database
                .sql("SELECT * FROM MDM_TRANSACTION_STATUS WHERE WORKORDER_START_TIME_ >= :workorderStartTime ORDER BY WORKORDER_START_TIME_ DESC FETCH FIRST :maxNumberOfRows ROWS ONLY")
                .parameter("workorderStartTime", workorderStartTime)
                .parameter("maxNumberOfRows", maxNumberOfRows)

        select.query { row ->
            Map listrow = [:]
            row.each { key, value ->
                listrow.put(key, value)
            }
            statusList.add(listrow)
        }

        result.put("statusList", statusList)

        com.compoverso.dmp.extension.core.connector.service.jdbc.builder.SelectBuilderImpl selectCount = jdbc.select() // select for internal database
                .sql("SELECT COUNT(*) FROM MDM_TRANSACTION_STATUS WHERE WORKORDER_START_TIME_ >= :workorderStartTime GROUP BY TRANSACTION_QUEUED_TIME_")
                .parameter("workorderStartTime", workorderStartTime)
        selectCount.query { row ->
            selectedNumberOfRows = selectedNumberOfRows + row.get("COUNT(*)")
        }

        result.put("selectedNumberOfRows", selectedNumberOfRows)

    }

    catch (Exception ex) {
        logger.error(ex.toString())
    }

    logger.debug("Status queried. Timestamp >= " + workorderStartTime.toString())


    return result
}

def queryTransactionStatus(Timestamp workorderStartTime, Timestamp workorderEndTime, Integer maxNumberOfRows, String workorderID, String workorderStatus, String transactionSource, String transactionSourceProcessID, String transactionType, String idType, String idNumber, String MDMPartnerID) {

    List statusList = []
    Integer selectedNumberOfRows = 0
    Integer selectedNumberOfFailedRows = 0

    Map result = [:]

    try {

        // prepare SQL
        com.compoverso.dmp.extension.core.connector.service.jdbc.builder.SelectBuilderImpl select = jdbc.select() // select for internal database
                .sql("SELECT * FROM MDM_TRANSACTION_STATUS " +
                        "LEFT JOIN WORK_ORDERS ON UPPER(MDM_TRANSACTION_STATUS.WORKORDER_ID_)=UPPER(WORK_ORDERS.ID_) " +
                        "WHERE WORKORDER_START_TIME_ BETWEEN :workorderStartTime AND :workorderEndTime AND " +
                        "(WORKORDER_ID_ LIKE :workorderID OR WORKORDER_ID_ IS NULL) AND " +
                        "(STATUS_ LIKE :workorderStatus OR STATUS_ IS NULL) AND " +
                        "(TRANSACTION_SOURCE_ LIKE :transactionSource OR TRANSACTION_SOURCE_ IS NULL) AND " +
                        "(TRANSACTION_SOURCE_PROCESS_ID_ LIKE :transactionSourceProcessID OR TRANSACTION_SOURCE_PROCESS_ID_ IS NULL) AND " +
                        "(TRANSACTION_TYPE_ LIKE :transactionType OR TRANSACTION_TYPE_ IS NULL) AND " +
                        "(TRANSACTION_IDTYPE_ LIKE :idType OR TRANSACTION_IDTYPE_ IS NULL) AND " +
                        "(TRANSACTION_IDNUMBER_ LIKE :idNumber OR TRANSACTION_IDNUMBER_ IS NULL) AND " +
                        "(MDM_PARTNERID_ LIKE :MDMPartnerID OR MDM_PARTNERID_ IS NULL) " +
                        "ORDER BY WORKORDER_START_TIME_ DESC FETCH FIRST :maxNumberOfRows ROWS ONLY")
                .parameter("workorderStartTime", workorderStartTime)
                .parameter("workorderEndTime", workorderEndTime)
                .parameter("maxNumberOfRows", maxNumberOfRows)
                .parameter("workorderID", workorderID.toUpperCase())
                .parameter("workorderStatus", workorderStatus)
                .parameter("transactionSource", transactionSource)
                .parameter("transactionSourceProcessID", transactionSourceProcessID.toUpperCase())
                .parameter("transactionType", transactionType)
                .parameter("idType", idType)
                .parameter("idNumber", idNumber.toUpperCase())
                .parameter("MDMPartnerID", MDMPartnerID.toUpperCase())


        select.query { row ->
            Map listrow = [:]
            boolean add = true
            row.each { key, value ->
                switch (key) {
                    case "WORKORDER_ID_":
                        if ((workorderID != "%") && (value == null)) {
                            add = false
                        }
                        break
                    case "STATUS_":
                        if ((workorderStatus != "%") && (value == null)) {
                            add = false
                        }
                        break
                    case "TRANSACTION_SOURCE_":
                        if ((transactionSource != "%") && (value == null)) {
                            add = false
                        }
                        break
                    case "TRANSACTION_SOURCE_PROCESS_ID_":
                        if ((transactionSourceProcessID != "%") && (value == null)) {
                            add = false
                        }
                        break
                    case "TRANSACTION_TYPE_ ":
                        if ((transactionType != "%") && (value == null)) {
                            add = false
                        }
                        break
                    case "TRANSACTION_IDTYPE_":
                        if ((idType != "%") && (value == null)) {
                            add = false
                        }
                        break
                    case "TRANSACTION_IDNUMBER_":
                        if ((idNumber != "%") && (value == null)) {
                            add = false
                        }
                        break
                    case "MDM_PARTNERID_":
                        if ((MDMPartnerID != "%") && (value == null)) {
                            add = false
                        }
                        break
                }
                listrow.put(key, value)
            }
            if (add) {
                statusList.add(listrow)
            }
        }

        logger.debug("statusList: " + statusList)
        result.put("statusList", statusList)

        com.compoverso.dmp.extension.core.connector.service.jdbc.builder.SelectBuilderImpl selectCount = jdbc.select() // select for internal database
                .sql("SELECT COUNT(*) FROM MDM_TRANSACTION_STATUS " +
                        "LEFT JOIN WORK_ORDERS ON UPPER(MDM_TRANSACTION_STATUS.WORKORDER_ID_)=UPPER(WORK_ORDERS.ID_) " +
                        "WHERE WORKORDER_START_TIME_  BETWEEN :workorderStartTime AND :workorderEndTime AND " +
                        "(WORKORDER_ID_ LIKE :workorderID OR WORKORDER_ID_ IS NULL) AND " +
                        "(STATUS_ LIKE :workorderStatus OR STATUS_ IS NULL) AND " +
                        "(TRANSACTION_SOURCE_ LIKE :transactionSource OR TRANSACTION_SOURCE_ IS NULL) AND " +
                        "(TRANSACTION_SOURCE_PROCESS_ID_ LIKE :transactionSourceProcessID OR TRANSACTION_SOURCE_PROCESS_ID_ IS NULL) AND " +
                        "(TRANSACTION_TYPE_ LIKE :transactionType OR TRANSACTION_TYPE_ IS NULL) AND " +
                        "(TRANSACTION_IDTYPE_ LIKE :idType OR TRANSACTION_IDTYPE_ IS NULL) AND " +
                        "(TRANSACTION_IDNUMBER_ LIKE :idNumber OR TRANSACTION_IDNUMBER_ IS NULL) AND " +
                        "(MDM_PARTNERID_ LIKE :MDMPartnerID OR MDM_PARTNERID_ IS NULL) " +
                        "GROUP BY TRANSACTION_QUEUED_TIME_")
                .parameter("workorderStartTime", workorderStartTime)
                .parameter("workorderEndTime", workorderEndTime)
                .parameter("workorderID", workorderID.toUpperCase())
                .parameter("workorderStatus", workorderStatus)
                .parameter("transactionSource", transactionSource)
                .parameter("transactionSourceProcessID", transactionSourceProcessID.toUpperCase())
                .parameter("transactionType", transactionType)
                .parameter("idType", idType)
                .parameter("idNumber", idNumber.toUpperCase())
                .parameter("MDMPartnerID", MDMPartnerID.toUpperCase())
        selectCount.query { row ->
            selectedNumberOfRows = selectedNumberOfRows + row.get("COUNT(*)")
        }

        result.put("selectedNumberOfRows", selectedNumberOfRows)

        com.compoverso.dmp.extension.core.connector.service.jdbc.builder.SelectBuilderImpl selectCountFailed = jdbc.select() // select for internal database
                .sql("SELECT COUNT(*) FROM MDM_TRANSACTION_STATUS " +
                        "LEFT JOIN WORK_ORDERS ON UPPER(MDM_TRANSACTION_STATUS.WORKORDER_ID_)=UPPER(WORK_ORDERS.ID_) " +
                        "WHERE WORKORDER_START_TIME_  BETWEEN :workorderStartTime AND :workorderEndTime AND " +
                        "(WORKORDER_ID_ LIKE :workorderID OR WORKORDER_ID_ IS NULL) AND " +
                        "STATUS_ = 'FAILED' AND " +
                        "(TRANSACTION_SOURCE_ LIKE :transactionSource OR TRANSACTION_SOURCE_ IS NULL) AND " +
                        "(TRANSACTION_SOURCE_PROCESS_ID_ LIKE :transactionSourceProcessID OR TRANSACTION_SOURCE_PROCESS_ID_ IS NULL) AND " +
                        "(TRANSACTION_TYPE_ LIKE :transactionType OR TRANSACTION_TYPE_ IS NULL) AND " +
                        "(TRANSACTION_IDTYPE_ LIKE :idType OR TRANSACTION_IDTYPE_ IS NULL) AND " +
                        "(TRANSACTION_IDNUMBER_ LIKE :idNumber OR TRANSACTION_IDNUMBER_ IS NULL) AND " +
                        "(MDM_PARTNERID_ LIKE :MDMPartnerID OR MDM_PARTNERID_ IS NULL) " +
                        "GROUP BY TRANSACTION_QUEUED_TIME_")
                .parameter("workorderStartTime", workorderStartTime)
                .parameter("workorderEndTime", workorderEndTime)
                .parameter("workorderID", workorderID.toUpperCase())
                .parameter("transactionSource", transactionSource)
                .parameter("transactionSourceProcessID", transactionSourceProcessID.toUpperCase())
                .parameter("transactionType", transactionType)
                .parameter("idType", idType)
                .parameter("idNumber", idNumber.toUpperCase())
                .parameter("MDMPartnerID", MDMPartnerID.toUpperCase())
        selectCountFailed.query { row ->
            selectedNumberOfFailedRows = selectedNumberOfFailedRows + row.get("COUNT(*)")
        }

        result.put("selectedNumberOfFailedRows", selectedNumberOfFailedRows)

    }

    catch (Exception ex) {
        logger.error(ex.toString())
    }

    logger.debug("Status queried. Timestamp >= " + workorderStartTime.toString())

    return result
}

def getFiles(String workorderID, Map context) {

    String datapathInbound
    String datapathOutboundState
    String datapathOutboundChange

    def inboundFile
    def outboundChangeFile
    def outboundStateFile


    try {

        // prepare SQL
        com.compoverso.dmp.extension.core.connector.service.jdbc.builder.SelectBuilderImpl select = jdbc.select() // select for internal database
                .sql("SELECT * FROM MDM_TRANSACTION_STATUS WHERE WORKORDER_ID_ = :workorderID")
                .parameter("workorderID", workorderID.toUpperCase())

        select.query { row ->
            datapathInbound = row.get("TRANSACTION_DATAPATH_")
            datapathOutboundChange = row.get("CHANGE_OUTBOUND_DATAPATH_")
            datapathOutboundState = row.get("STATE_OUTBOUND_DATAPATH_")
        }

        // read files
        if (datapathInbound == null) {
            inboundFile = "Inbound file not found"
        } else {
            inboundFile = context.resources.get(datapathInbound).load("json")
        }
        if (datapathOutboundChange == null) {
            outboundChangeFile = "Outbound Change file not found"
        } else {
            outboundChangeFile = context.resources.get(datapathOutboundChange).load("json")
        }
        if (datapathOutboundState == null) {
            outboundStateFile = "Outbound State file not found"
        } else {
            outboundStateFile = context.resources.get(datapathOutboundState).load("json")
        }
        // copy to staging area
        String datapath = "system:${application.getProperty("mdm.stagingarea")}/inbound.json"
        context.resources.get(datapath).save("json", inboundFile, [prettyPrint: true])
        datapath = "system:${application.getProperty("mdm.stagingarea")}/outbound_state.json"
        context.resources.get(datapath).save("json", outboundStateFile, [prettyPrint: true])
        datapath = "system:${application.getProperty("mdm.stagingarea")}/outbound_change.json"
        context.resources.get(datapath).save("json", outboundChangeFile, [prettyPrint: true])

    }
    catch (Exception ex) {
        logger.error(ex.toString())
    }

    logger.debug("Files copied to StagingArea")
}

def getFiles(Timestamp queryTimestamp, Map context) {

    String datapathInbound
    String datapathOutboundState
    String datapathOutboundChange

    def inboundFile
    def outboundChangeFile
    def outboundStateFile


    try {

        // prepare SQL
        com.compoverso.dmp.extension.core.connector.service.jdbc.builder.SelectBuilderImpl select = jdbc.select() // select for internal database
                .sql("SELECT * FROM MDM_TRANSACTION_STATUS WHERE WORKORDER_START_TIME_ >= :queryTimestamp")
                .parameter("queryTimestamp", queryTimestamp)

        select.query { row ->
            datapathInbound = row.get("TRANSACTION_DATAPATH_")
            datapathOutboundChange = row.get("CHANGE_OUTBOUND_DATAPATH_")
            datapathOutboundState = row.get("STATE_OUTBOUND_DATAPATH_")
        }

        // read files
        if (datapathInbound == null) {
            inboundFile = "Inbound file not found"
        } else {
            inboundFile = context.resources.get(datapathInbound).load("json")
        }
        if (datapathOutboundChange == null) {
            outboundChangeFile = "Outbound Change file not found"
        } else {
            outboundChangeFile = context.resources.get(datapathOutboundChange).load("json")
        }
        if (datapathOutboundState == null) {
            outboundStateFile = "Outbound State file not found"
        } else {
            outboundStateFile = context.resources.get(datapathOutboundState).load("json")
        }
        // copy to staging area
        String datapath = "system:${application.getProperty("mdm.stagingarea")}/inbound.json"
        context.resources.get(datapath).save("json", inboundFile)
        datapath = "system:${application.getProperty("mdm.stagingarea")}/outbound_state.json"
        context.resources.get(datapath).save("json", outboundStateFile)
        datapath = "system:${application.getProperty("mdm.stagingarea")}/outbound_change.json"
        context.resources.get(datapath).save("json", outboundStateFile)

    }
    catch (Exception ex) {
        logger.error(ex.toString())
    }

    logger.debug("Files copied to StagingArea")
}