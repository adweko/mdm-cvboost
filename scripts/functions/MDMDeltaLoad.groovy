import java.sql.Timestamp
import java.time.Instant

def getDeltaLoadsList() {
    logger.debug("Start getDeltaLoadsList")

    List deltaLoadsList = []

    com.compoverso.dmp.extension.core.connector.service.jdbc.builder.SelectBuilderImpl select = jdbc.select() // select for internal database
            .sql("SELECT * FROM MDM_DELTA_LOADS LEFT JOIN WORK_ORDERS ON UPPER(MDM_DELTA_LOADS.WORKORDER_ID_)=UPPER(WORK_ORDERS.ID_) ORDER BY WORKORDER_START_TIME_ DESC")

    select.query { row ->
        Map listrow = [:]
        row.each { key, value ->
            if (value instanceof java.sql.Timestamp) {
                listrow.put(key, value.format("yyyyMMddHHmmssSSS", TimeZone.getTimeZone("UTC")))
            } else {
                listrow.put(key, value)
            }
            if ((key == "STATUS_") && (value == null)){
                listrow.put(key, "No Status found")
            }
        }
        deltaLoadsList.add(listrow)
    }

    return deltaLoadsList

    logger.debug("End getDeltaLoadsList")
}

def logStartDeltaLoad(String workorderID, String creator, String owner, Integer deltaLoadType, Timestamp deltaLoadSelParamTimestamp, String deltaLoadSelParamMDMPartnerID, String comment){
        // set values
        Instant now = Instant.now()
        Timestamp lastUpdatedByTime = Timestamp.from(now)

        // prepare SQL
        com.compoverso.dmp.extension.core.connector.service.jdbc.builder.UpdateBuilderImpl update = jdbc.update() // creates new update for internal database
                .sql("""INSERT INTO MDM_DELTA_LOADS (
                        WORKORDER_ID_,
                        WORKORDER_START_TIME_,
                        WORKORDER_CREATOR_,
                        WORKORDER_OWNER_,
                        DL_TYPE_,
                        DL_SEL_PARAM_TIMESTAMP_,
                        DL_SEL_PARAM_MDMPARTNERID_,
                        COMMENT_,                 
                        LAST_UPDATED_BY_,
                        LAST_UPDATED_BY_TIME_
                    )
                    VALUES (
                        :workorderID,
                        :workorderStartTime,
                        :workorderCreator,
                        :workorderOwner,
                        :deltaLoadType,
                        :deltaLoadSelParamTimestamp,
                        :deltaLoadSelParamMDMPartnerID,
                        :comment,
                        :lastUpdatedBy,
                        :lastUpdatedByTime
                    )""")
                .parameters([
                        workorderID                     : workorderID.toUpperCase(),
                        workorderStartTime              : lastUpdatedByTime,
                        workorderCreator                : creator,
                        workorderOwner                  : owner,
                        deltaLoadType                   : deltaLoadType,
                        deltaLoadSelParamTimestamp      : deltaLoadSelParamTimestamp,
                        deltaLoadSelParamMDMPartnerID   : deltaLoadSelParamMDMPartnerID,
                        comment                         : comment,
                        lastUpdatedBy                   : creator,
                        lastUpdatedByTime               : lastUpdatedByTime
                ])

        Integer rowcount = update.execute()

        logger.debug("DeltaLoad Start logged. workorderID: " + workorderID)
    }

def logEndDeltaLoad(String workorderID, Integer deltaLoadNumberOfTransactions, String lastUpdatedBy){
    // set values
    Instant now = Instant.now()
    Timestamp lastUpdatedByTime = Timestamp.from(now)

    // prepare SQL
    com.compoverso.dmp.extension.core.connector.service.jdbc.builder.UpdateBuilderImpl update = jdbc.update() // update for internal database
            .sql("UPDATE MDM_DELTA_LOADS SET DL_NUMBER_OF_TRANSACTIONS_ =  :deltaLoadNumberOfTransactions, LAST_UPDATED_BY_ = :lastUpdatedBy,  LAST_UPDATED_BY_TIME_ = :lastUpdatedByTime WHERE WORKORDER_ID_ = :workorderID")
            .parameter("deltaLoadNumberOfTransactions", deltaLoadNumberOfTransactions)
            .parameter("workorderID", workorderID.toUpperCase())
            .parameter("lastUpdatedBy", lastUpdatedBy)
            .parameter("lastUpdatedByTime", lastUpdatedByTime)

    Integer rowcount = update.execute()

    logger.debug("DeltaLoad End logged. workorderID: " + workorderID)
}