# Code Repository CVBoost Entwicklungen

CVBoost-Entwicklungen bestehen hauptsächlich aus .bpmn- und .groovy-Dateien sowie zu den Programmen gehörende Dateien, zum Beispiel .json-Dateien mit Modelldefinitionen.

Dieses Repository hält die Referenzen für alle CVBoost-Entwicklungen.
Entwicklungen, Tests und der Betrieb finden statt auf laufenden Serverinstanzen.
Dazu müssen Entwicklungsstände auf diese Server deployed werden bzw. von diesen Servern exportiert werden.
Es sind die Bereiche Versionierung von Entwicklungen, 
Deployment von Entwicklungen und Deployment von Systemen zu betrachten.

## Versionierung von Entwicklungen
### Struktur und Funktion
Entwicklungen bestehen ausschließlich aus Dateien, die in einer bestimmer Ordnerstruktur abgelegt werden müssen.
Die Struktur dieses Repositories spiegelt die Struktur einer CVBoost-Installation wieder.
Ordner und Dateien, die zur Server-seitigen Installation gehören und nicht versioniert werden sollen, sind in der .gitignore aufgenommen.
Die für CVBoost (im Kontext MDM) direkt relevanten Ordner sind
1. avro
2. mdm/metadata
3. mdm/test
4. processing/development
5. scripts/functions

Der Ordner testenvironment beinhaltet eine virtuelle Maschine mit einer Minimalinstallation von CVBoost, die zum Testen der vcs-deployment-Skripte verwendet werden kann.
Der Ordner vcs-deployment beinhaltet Skripte zur Automatisierung des Versionierungs- und Deploymentprozesses.
Hier finden sich die folgenden Unterordner:
1. env - Dieser Ordner beinhaltet Variablen für die Serverkonfiguration, im Sinne von Zielservern des Deployments. Wenn weitere Systeme gewünscht sind, kann sich an einer der Dateien orientiert und die entsprechend anderen Konfigurationen ersetzt werden.


### Benutzung der Versionierungstools
Das Projekt wird aus dem Bitbucket ausgecheckt.
Danach wird 
```bash
bash vcs-deployment/fetch_developments_from_server.sh -e vcs-deployment/<server>-env.sh
```
aufgerufen. Dieses Skript überschreibt alle Ordner im lokalen Repo mit dem Entwicklungsstand vom Server. ```git status``` zeigt die erfolgten Änderungen an.

Diese können dann wiederum mit ```git commit``` und ```git push``` in die Versionskontrolle aufgenommen werden.

## Deployment von Entwicklungen
Um einen Entwicklungsstand auf einen Server zu deployen, wird
```bash
# WARNING: This will delete all developments on the target server and replace them!
# WARNING: Make sure they are backed up beforehand!
bash vcs-deployment/deploy_developments_to_server.sh -e <server>-env.sh
```
aufgerufen.

Es empfiehlt sich aus Gründen der Nachvollziehbarkeit und Übersicht, v.a. so lange kein
CI/CD-Tool im Einsatz ist, die auf die INTG deployten Versionen zu taggen.
Dies ist sehr einfach über die git-Integration von IntelliJ möglich, oder via
```bash
git tag -a vX.Y.Z -m "tag version X.Y.Z"
```
wobei natürlich X.Y.Z durch sinnige Versionsnummer ersetzt werden. Danach sollte das Deployment in
die Übersicht auf https://wiki.helvetia.group/x/rmp0Fg aufgenommen werden.

## Deployment von CVBoost-Systemen
Eine CVBoost-Instanz besteht neben den Eigenentwicklungen aus der Kerninstallation des tomcat-Servers inklusive der cvboost-.war-Datei und Konfigurationsdateien, die sich je nach Umgebung in weiten Teilen unterscheiden können (benutzte Datenbank, Cluster-Installationen, etc.).
Dennoch ist es möglich und wünschenswert, auch diese Dateien über ein Versionskontrollsystem zu managen und automatisiert deployen zu können, um nachvollziehbare und automatisierbare Installationen und Deployments im Sinne eines CI/CD-Prozesses zu ermöglichen.

Die hierfür notwendigen Funktionalitäten sollten ebenfalls im vcs-deployment-Ordner entwickelt werden.


## Update der Avro-Schemas der Producer
Die Avro-Schema-Versionen, die die Producer in den processTransaction()-Skripten benutzen, 
sind in der Datei avro/schema.properties hinterlegt und werden beim Initialisieren des Workflows eingelesen.
Beim deployment auf einen Server wird diese Datei automatisch angelegt und zeigt auf die 
neueste Version der in der schema Registry registrierten Schemas für das entsprechende subject, was
in den allermeisten Fällen das gewünschte Verhalten ist.
Sollte ein anderes Verhalten gewünscht sein, muss nach dem Deployment das auf der Instanz angelegte File
avro/schema.properties manuell editiert werden.