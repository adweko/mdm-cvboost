set -e

export ROOTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd | awk -F 'vcs-deployment' '{print $1}')"

usage() {
    [[ $1 ]] && echo "Error: $1"
    echo "Get the latest versions of the schemas ch-mdm.state.all and ch-mdm.change.all"
    echo "usage: $0 -e <env-file>"
    exit 0
}
[[ $# -eq 0 ]] && usage

while getopts ":he:s:" arg; do
  case $arg in
    e) # Specify env file.
      export ENV_FILE=${OPTARG}
      ;;
    s) # Specify schema subject.
      export SUBJECT=${OPTARG}
      ;;
    h | *) # Display usage and exit
      usage
      ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"
source ${ENV_FILE}


bash ${ROOTDIR}/vcs-deployment/get_latest_schema_version.sh -e ${ENV_FILE} -s ch-mdm.state.all
bash ${ROOTDIR}/vcs-deployment/get_latest_schema_version.sh -e ${ENV_FILE} -s ch-mdm.change.all
