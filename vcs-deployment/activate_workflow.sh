

export ROOTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd | awk -F 'vcs-deployment' '{print $1}')"
echo "ROOTDIR is $ROOTDIR"

usage() {
    [[ $1 ]] && echo "Error: $1"
    echo "usage: $0 -e <env-file> -f <path-to-workflow-bpmn-file-on-target-system>"
    exit 0
}

[[ $# -eq 0 ]] && usage

while getopts ":he:" arg; do
  case $arg in
    e) # Specify env file.
      export ENV_FILE=${OPTARG}
      ;;
    h | *) # Display usage and exit
      usage
      ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"

source ${ENV_FILE}

#curl -k -v \
# -X POST \
# --user Administrator:administrator \
# -H "Content-Type: multipart/form-data" \
# -H "workorder.name: isThisNecessary?" \
# -H "workorder.param.bpmnFilePath: /var/opt/cvboost/home/processing/development/kafkaProducerTest/kafkaProducerTest.bpmn" \
# -F "workorder.file= nothing" \
# https://mdm-cvboost-devl.helvetia.ch:8443/services/rest/processing/workorder/v1.0/start/ActivateWorkflow

# curl -k -v \
# -X POST \
# --user Administrator:administrator \
# -H "Content-Type: multipart/form-data" \
# -H "workorder.name: ActivateWorkflow" \
# -H "workorder.process.param.bpmnFilePath: /var/opt/cvboost/home/processing/development/kafkaProducerTest/kafkaProducerTest.bpmn" \
# -F "workorder.file= nothing" \
# https://mdm-cvboost-devl.helvetia.ch:8443/services/rest/processing/workorder/v1.0/start/ActivateWorkflow


curl -k -v \
 --user Administrator:administrator \
 -H "workorder.param.bpmnFilePath: /var/opt/cvboost/home/processing/development/kafkaProducerTest/kafkaProducerTest.bpmn" \
 https://mdm-cvboost-devl.helvetia.ch:8443/services/rest/processing/workorder/v1.0/start/ActivateWorkflow


