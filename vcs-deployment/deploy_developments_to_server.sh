# along the lines of
set -e
export ROOTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd | awk -F 'vcs-deployment' '{print $1}')"
echo "ROOTDIR is $ROOTDIR"

usage() {
    [[ $1 ]] && echo "Error: $1"
    echo "usage: $0 -e <env-file>"
    exit 0
}
[[ $# -eq 0 ]] && usage

while getopts ":he:" arg; do
  case $arg in
    e) # Specify env file.
      export ENV_FILE=${OPTARG}
      ;;
    h | *) # Display usage and exit
      usage
      ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"

source ${ENV_FILE}

# recreate the file avro/schema.properties and point to the latest version of the schemata registered in that
# environment as a default config for the Kafka producers in the processTransaction scripts
bash ${ROOTDIR}/vcs-deployment/get_latest_schema_versions_default.sh -e ${ENV_FILE} > ${ROOTDIR}/avro/schema.properties

# should we delete folders on server? yes
# but take care not to destroy the logs
ssh -p ${PORT} ${USER}@${SERVER} "systemctl stop cvboost; cd ${CVBHOME} && rm -rf mdm/metadata && rm -rf mdm/test && rm -rf processing/development && rm -rf vcs-deployment && rm -rf avro && rm -rf scripts/functions"
ssh -p ${PORT} ${USER}@${SERVER} "cd ${CVBHOME} && mkdir -p mdm mdm/metadata mdm/test processing/development vcs-deployment avro scripts scripts/functions"
scp -r -P ${PORT} ${ROOTDIR}/avro/* ${USER}@${SERVER}:${CVBHOME}/avro/
scp -r -P ${PORT} ${ROOTDIR}/mdm/* ${USER}@${SERVER}:${CVBHOME}/mdm/
scp -r -P ${PORT} ${ROOTDIR}/processing/* ${USER}@${SERVER}:${CVBHOME}/processing/
scp -r -P ${PORT} ${ROOTDIR}/scripts/functions/* ${USER}@${SERVER}:${CVBHOME}/scripts/functions/
scp -r -P ${PORT} ${ROOTDIR}/vcs-deployment/* ${USER}@${SERVER}:${CVBHOME}/vcs-deployment/

# we copied as root. give permissions back to cvboost user
ssh -p ${PORT} ${USER}@${SERVER} "chown -R ${SERVER_USER} ${CVBHOME}"

# changes on application.properties should be ingested. TODO
#ssh -p ${PORT} ${USER}@${SERVER} "cd ${CVBHOME}/vcs-deployment/ && bash setup.sh"

#ssh -p ${PORT} ${USER}@${SERVER} 'systemctl start cvboost'
