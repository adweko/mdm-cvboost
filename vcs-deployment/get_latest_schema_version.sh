set -e

export ROOTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd | awk -F 'vcs-deployment' '{print $1}')"

usage() {
    [[ $1 ]] && echo "Error: $1"
    echo "Get the latest version of the schema"
    echo "usage: $0 -e <env-file> -s <subject-name>"
    exit 0
}
[[ $# -eq 0 ]] && usage

while getopts ":he:s:" arg; do
  case $arg in
    e) # Specify env file.
      export ENV_FILE=${OPTARG}
      ;;
    s) # Specify schema subject.
      export SUBJECT=${OPTARG}
      ;;
    h | *) # Display usage and exit
      usage
      ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"
source ${ENV_FILE}
[[ -z ${SUBJECT} ]] && usage "subject not set"

version=$(curl -k -X GET ${SCHEMAREG_URL}/subjects/${SUBJECT}/versions/latest | awk -F ',"id' '{print $1}' | awk -F 'version":' '{print $2}')

echo "${SUBJECT}=${version}"
