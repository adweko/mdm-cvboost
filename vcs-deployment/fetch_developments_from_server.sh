# script fetches all development folders from an existing installation of CVBoost and updates the local folders
# with them. After update, new developments can be checked into version control
set -e

export ROOTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd | awk -F 'vcs-deployment' '{print $1}')"
echo "ROOTDIR is $ROOTDIR"

usage() {
    [[ $1 ]] && echo "Error: $1"
    echo "usage: $0 -e <env-file>"
    exit 0
}
[[ $# -eq 0 ]] && usage

while getopts ":he:" arg; do
  case $arg in
    e) # Specify env file.
      export ENV_FILE=${OPTARG}
      ;;
    h | *) # Display usage and exit
      usage
      ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"

source ${ENV_FILE}

TMPDIR=${ROOTDIR}/vcs-deployment/tmp
mkdir -p ${TMPDIR}

scp -r -P ${PORT} ${USER}@${SERVER}:"${CVBHOME}/avro ${CVBHOME}/mdm/metadata ${CVBHOME}/mdm/test ${CVBHOME}/processing/development ${CVBHOME}/scripts/functions" ${TMPDIR}

cp -r ${TMPDIR}/avro/* "$ROOTDIR"/avro/
cp -r ${TMPDIR}/metadata/* "$ROOTDIR"/mdm/metadata/
cp -r ${TMPDIR}/test/* "$ROOTDIR"/mdm/test/
cp -r ${TMPDIR}/development/* "$ROOTDIR"/processing/development/
cp -r ${TMPDIR}/functions/* "$ROOTDIR"/scripts/functions/

rm -r ${TMPDIR}