# connection setting for access to server

export SERVER=dc00-smdm002i@psmp
export PORT=22
export USER=che0lim@root

# cvboost configuration on server

export CVBHOME="/var/opt/cvboostshare/home"
export SERVER_USER=icvboost

export SCHEMAREG_URL="https://cp-schema-registry-kafka-intg.apps.helvetia.io"

### CURRENTLY NOT USED
#export SERVER_ADDRESS="$(hostname).helvetia.ch"
#export JDBC_URL="jdbc:oracle:thin:@//oradev1.helvetia.ch:1521/MDMD001.helvetia.ch"
#export JDBS_USERNAME=CAMUNDA
#export JDBC_PASSWORD=camunda2019
#export MDM_HAZELCASTURL="mdm-spectrum-devl.helvetia.ch:5701"
#export MDM_HAZELCASTNAME="SpectrumCluster"
#
#export CLUSTER_NETWORK_NODEIPADDRESS=$(hostname).helvetia.ch
#export CLUSTER_NETWORK_LOCAL_NODEIPADDRESS=$(hostname).helvetia.ch
