# connection setting for access to server
export SERVER="dc00-smdm002d@psmp"
export PORT=22
export USER="che0wea@root"
# cvboost configuration on server

export CVBHOME="/var/opt/cvboost/home"
export SERVER_USER=dcvboost

export SCHEMAREG_URL="https://cp-schema-registry-kafka-devl.apps.helvetia.io"
### CURRENTLY NOT USED
#export SERVER_ADDRESS="$(hostname).helvetia.ch"
#export JDBC_URL="jdbc:oracle:thin:@//oradev1.helvetia.ch:1521/MDMD001.helvetia.ch"
#export JDBS_USERNAME=CAMUNDA
#export JDBC_PASSWORD=camunda2019
#export MDM_HAZELCASTURL="mdm-spectrum-devl.helvetia.ch:5701"
#export MDM_HAZELCASTNAME="SpectrumCluster"
#
#export CLUSTER_NETWORK_NODEIPADDRESS=$(hostname).helvetia.ch
#export CLUSTER_NETWORK_LOCAL_NODEIPADDRESS=$(hostname).helvetia.ch
