#!/usr/bin/env bash
# if dcvboost does not already exist, create it
sudo id -u dcvboost ||
  { sudo useradd dcvboost &&
  echo "dcvboost" | sudo passwd --stdin dcvboost &&
  sudo usermod -aG wheel dcvboost &&
  sudo usermod -aG root dcvboost ; }

# if tomcat group does not already exist, create it
sudo getent group tomcat ||
  { sudo groupadd tomcat &&
  sudo useradd -M -s /bin/nologin -g tomcat -d /opt/tomcat tomcat ; }
