export CVBOOST_VERSION=4.0.0-BETA.9

sudo mkdir -p /opt/cvboost/agents
sudo mkdir -p /var/opt/cvboost_agent_messaging_kafka
sudo mkdir -p /var/opt/cvboost_agent_messaging_kafka/home

sudo cp /vagrant/${CVBOOST_VERSION}-cvboost/cvboost-${CVBOOST_VERSION}/agents/compoverso-agent-node-messaging-kafka-${CVBOOST_VERSION}.zip /opt/cvboost/agents/
cd /opt/cvboost/agents && sudo unzip compoverso-agent-node-messaging-kafka-${CVBOOST_VERSION}.zip
sudo mv /opt/cvboost/agents/compoverso-agent-node-messaging-kafka-${CVBOOST_VERSION} /opt/cvboost/agents/compoverso-agent-node-messaging-kafka

sudo tee /etc/systemd/system/cvboost_agent_kafka.service <<EOT
[Unit]
Description=CVBoost Messaging Kafka agent for connecting to Kafka messaging system
After=syslog.target

[Service]
Environment=JAVA_HOME=/usr/lib/jvm/jre
Environment=DMP_HOME=/var/opt/cvboost_agent_messaging_kafka/home
Environment=DMP_NODE_ID=AGENT_MESSAGING_KAFKA
Environment=DMP_LOG_DIR=/opt/cvboost/agents/compoverso-agent-node-messaging-kafka/logs
ExecStart=/opt/cvboost/agents/compoverso-agent-node-messaging-kafka/start.sh
WorkingDirectory=/opt/cvboost/agents/compoverso-agent-node-messaging-kafka
SuccessExitStatus=0

User=dcvboost
Group=dcvboost

[Install]
WantedBy=multi-user.target
EOT

sudo systemctl enable cvboost_agent_kafka

sudo cp -r /vagrant/provisioning/conf/messaging/cert /opt/cvboost/agents/compoverso-agent-node-messaging-kafka/
sudo cp /vagrant/provisioning/conf/messaging/application.properties /opt/cvboost/agents/compoverso-agent-node-messaging-kafka/

sudo cp /vagrant/license.json /var/opt/cvboost_agent_messaging_kafka/home/

sudo chown -R dcvboost /opt/cvboost/agents
sudo chown -R dcvboost /var/opt/cvboost_agent_messaging_kafka
sudo chgrp -R dcvboost /var/opt/cvboost_agent_messaging_kafka
sudo chgrp -R dcvboost /opt/cvboost/agents

sudo chmod +x /opt/cvboost/agents/compoverso-agent-node-messaging-kafka/start.sh


