#!/usr/bin/env bash

export CVBOOST_VERSION=4.0.0-BETA.9

sudo mkdir -p /opt/cvboost
sudo mkdir -p /var/opt/cvboost
sudo mkdir -p /var/opt/cvboost/home

sudo cp -r /vagrant/${CVBOOST_VERSION}-cvboost/cvboost-${CVBOOST_VERSION}/tomcat /opt/cvboost/
sudo cp /vagrant/${CVBOOST_VERSION}-cvboost/cvboost-${CVBOOST_VERSION}/cvboost-${CVBOOST_VERSION}.war /opt/cvboost/tomcat/webapps/ROOT.war
sudo cp -r /vagrant/${CVBOOST_VERSION}-cvboost/cvboost-${CVBOOST_VERSION}/docker/home/* /var/opt/cvboost/home/
sudo cp -r /vagrant/license.json /var/opt/cvboost/home/

sudo tee /var/opt/cvboost/home/profiles.conf > /dev/null <<EOT
DEV,!METRIC,!LDAP,!LDAP_AUTHENTICATION,PROCESSING_FILE_INTERFACE,UPDATE,PB-SPECTRUM,MESSAGING-CONNECTOR,AVRO
EOT

sudo tee /var/opt/cvboost/home/application.properties > /dev/null <<EOT
app.temp=\${app.home}/_reset/temp
backupRestore.storage.directory=\${app.home}/_reset/backupRestore
fileStorage.root=\${app.home}/_reset/filestorage
mvc.uploaddir=\${app.home}/_reset/upload
cache.directory=\${app.home}/_reset/cache
process.workDirectory.root=\${app.home}/_reset/process/workDirectory

# Database configuration
jdbc.url=jdbc:h2:file:\${app.home}/_reset/h2/db
h2.server.args=-tcpPort,9099

#server.port=8090
#server.address=127.0.0.1

archive.targetFolder=\${app.home}/_reset/archive
EOT

sudo chown -R dcvboost /opt/cvboost/
sudo chown -R dcvboost /var/opt/cvboost/

sudo tee /etc/systemd/system/cvboost.service > /dev/null <<EOT
#Systemd unit file for tomcat
[Unit]
Description=Apache Tomcat Web Application Container
After=syslog.target network.target

[Service]
Type=forking

Environment=JAVA_HOME=/usr/lib/jvm/jre
Environment=DMP_HOME=/var/opt/cvboost/home
Environment=CATALINA_PID=/opt/cvboost/tomcat/temp/tomcat.pid
Environment=CATALINA_HOME=/opt/cvboost/tomcat/
Environment=CATALINA_BASE=/opt/cvboost/tomcat/
Environment='CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC'
Environment='JAVA_OPTS=-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom'

ExecStart=/opt/cvboost/tomcat/bin/startup.sh
ExecStop=/bin/kill -15 $MAINPID

User=dcvboost
Group=dcvboost

[Install]
WantedBy=multi-user.target
EOT

sudo chown dcvboost /etc/systemd/system/cvboost.service

sudo systemctl enable cvboost

