# Vagrant Setup

## Prerequisites
- You have a valid cvboost "license.json" in this folder
- You have a .zip containing a cvboost distribution, e.g 3.1.0 or 4.0.0-BETA.9
- Open provisioning/install_cvboost.sh and set at the top:
 ```bash
export ${CVBOOST_VERSION}=<your-version>
```
- You have unpacked the .zip distribution of cvboost into this folder, such that
    there is a folder ${CVBOOST_VERSION}-cvboost present (if not, Vagrantfile will fail.
     Go check it then)
- You have to make sure the Kafka-related keystore and truststore in provisioning/conf/messaging/cert are valid and up-to-date. See https://wiki.helvetia.group/x/iBktEw
- You have to give the corresponding and correct passwords in /provisioning/conf/messaging/application.properties for Kafka and jdbc
     

##
```bash
vagrant up
```
should do the whole magic.

After initial installation, you should be able to access cvboost at localhost:8080
in your browser.
The default admin account user is "Administrator" and the default password can either be
found in the server log,
```bash
vagrant ssh
ls -lhrt /var/opt/cvboost/home/logs
cat <last-file-list-by-command-above>
```
or is "admin", "administrator" or "Administrator".